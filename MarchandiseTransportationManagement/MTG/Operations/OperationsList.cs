using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Operations
{
    public partial class OperationsList : UserControl,IIUserControl
    {
        public int OperationID { get; set; }
        public gama_transportationdbDataSet.operationsRow OperationX { get; set; }
        public OperationsList()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
        }

        private void OperationsList_Load(object sender, EventArgs e)
        {
            partnersTableAdapter.Fill(gama_transportationdbDataSet.partners);
            this.packages_orderTableAdapter.Fill(gama_transportationdbDataSet.packages_order);
            this.locationsTableAdapter.Fill(this.gama_transportationdbDataSet.locations);
            this.operationsTableAdapter.Fill(this.gama_transportationdbDataSet.operations);
            mainDataGridViewX.Sort(datDataGridViewTextBoxColumn, ListSortDirection.Descending);
        }

        private void mainDataGridViewX_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (operationsBindingSource.Position < 0) return;
            int id = Convert.ToInt32(((DataRowView)operationsBindingSource.Current)["ID"]);
            bool isLeaving = Convert.ToBoolean(((DataRowView)operationsBindingSource.Current)["IsLeaving"]);
            var xOperation = new Operations.AddEditOperationPackageList();
            xOperation.OperationID = id;

            ((MainForm)this.FindForm()).AddTab(xOperation, isLeaving ? "����� ����: " + id : "����� ����: " + id);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (operationsBindingSource.Position < 0) return;
                if (MessageBox.Show("����� ��� ������� �����Ͽ", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
                int id = Convert.ToInt32(((DataRowView)operationsBindingSource.Current)["ID"]);
                new gama_transportationdbDataSetTableAdapters.packages_orderTableAdapter().DeleteByOperation(id);
                operationsTableAdapter.DeleteQuery(id);
                operationsBindingSource.RemoveCurrent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (operationsBindingSource.Position < 0) return;
            int id = Convert.ToInt32(((DataRowView)operationsBindingSource.Current)["ID"]);
            new Operations.Report.OperationOutReportForm() { operationID = id }.ShowDialog(this);
        }

        private void dataGridViewX1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        public void Refill()
        {
            OperationsList_Load(null, null);
        }
    }
}