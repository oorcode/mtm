using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Microsoft.Reporting.WinForms;

namespace MTM.Operations.Report
{
    public partial class OperationOutReportForm : DevComponents.DotNetBar.Metro.MetroForm
    {
        public OperationOutReportForm()
        {
            InitializeComponent();
            this.FormClosing += (se, ee) =>
            {
                this.reportViewer1.LocalReport.ReleaseSandboxAppDomain();
            };
        }

        private void PrintReportForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'viewDataSet.OperationPackageList' table. You can move, or remove it, as needed.
            this.operationPackageListTableAdapter.FillByOperationID(this.viewDataSet.OperationPackageList,operationID);
            ReportParameter rp1, rp2, rp3, rp4;
            SSUtilities.SSConfigFile cfg = new SSUtilities.SSConfigFile();
            rp1 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName, ""));
            rp2 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity, ""));
            rp3 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1, ""));
            rp4 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, ""));
            reportViewer1.LocalReport.SetParameters(new ReportParameter[] { rp1, rp2, rp3, rp4 });
            
            this.reportViewer1.RefreshReport();
        }

        public int operationID { get; set; }
    }
}