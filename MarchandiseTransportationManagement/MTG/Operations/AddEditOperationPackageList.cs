using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Operations
{
    public partial class AddEditOperationPackageList : UserControl
    {
        public int OperationID { get; set; }
        public gama_transportationdbDataSet.operationsRow OperationX { get; set; }
        public AddEditOperationPackageList()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            
        }

        private void AddEditOperationPackageList_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'viewDataSet.OperationPackageList' table. You can move, or remove it, as needed.
            this.operationPackageListTableAdapter.FillByOperationID(this.viewDataSet.OperationPackageList,OperationID);
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.locations' table. You can move, or remove it, as needed.
            this.locationsTableAdapter.Fill(this.gama_transportationdbDataSet.locations);
            this.operationsTableAdapter.FillByID(this.gama_transportationdbDataSet.operations,OperationID);
            OperationX = gama_transportationdbDataSet.operations.FirstOrDefault();
            LeavingLabel.Text = OperationX.IsLeaving ? "����" : "����";
        }

        private void btnAddOrderFactory_Click(object sender, EventArgs e)
        {
            var select = new Partners.SelectPartner();
            select.IsFactory = true;
            select.IsMerchant = false;
            select.IsWarehouse = false;
            if (select.ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
            
            var packageAdapter = new gama_transportationdbDataSetTableAdapters.packages_orderTableAdapter();
            packageAdapter.InsertQuery(OperationID, select.SelectedPartnerID, 0, 0, 0, 0, OperationX.IsLeaving ? 0 : 1, DateTime.Now);
            int id =packageAdapter.MaxID().GetValueOrDefault(0);
            var packageOrderView = new Packages.AddEditPackagesOrder();
            packageOrderView.PackageOrderID = id;
            packageOrderView.IsFactory = true;
            packageOrderView.PartnerID = select.SelectedPartnerID;
            packageOrderView.IsLeaving = OperationX.IsLeaving;
            packageOrderView.ShowDialog(this);
            this.operationPackageListTableAdapter.FillByOperationID(this.viewDataSet.OperationPackageList, OperationID);
        }

        private void btnAddOrderMerchant_Click(object sender, EventArgs e)
        {
            var select = new Partners.SelectPartner();
            select.IsFactory = true;
            select.IsMerchant = true;
            select.IsWarehouse = false;
            if (select.ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
            
            var packageAdapter = new gama_transportationdbDataSetTableAdapters.packages_orderTableAdapter();
            packageAdapter.InsertQuery(OperationID, select.SelectedPartnerID, 0, 0, 0, 0, 1, DateTime.Now);
            int id = packageAdapter.MaxID().GetValueOrDefault(0);
            var packageOrderView = new Packages.AddEditPackagesOrder();
            packageOrderView.PackageOrderID = id;
            packageOrderView.IsFactory = select.IsFactory;
            packageOrderView.PartnerID = select.SelectedPartnerID;
            packageOrderView.IsLeaving = OperationX.IsLeaving;
            packageOrderView.ShowDialog(this);
            this.operationPackageListTableAdapter.FillByOperationID(this.viewDataSet.OperationPackageList, OperationID);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (operationPackageListBindingSource.Position < 0) return;
            var packageOrder = new Packages.AddEditPackagesOrder();
            packageOrder.PartnerID = Convert.ToInt32(((DataRowView)operationPackageListBindingSource.Current)["PartnerID"]);
            packageOrder.PackageOrderID= Convert.ToInt32(((DataRowView)operationPackageListBindingSource.Current)["PackageOrderID"]);
            packageOrder.IsFactory = Convert.ToBoolean(((DataRowView)operationPackageListBindingSource.Current)["IsFactory"]);
            packageOrder.IsLeaving = OperationX.IsLeaving;
            packageOrder.ShowDialog(this);
            this.operationPackageListTableAdapter.FillByOperationID(this.viewDataSet.OperationPackageList, OperationID);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (operationPackageListBindingSource.Position < 0) return;
            if (MessageBox.Show("����� ��� ������ ������ɿ", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            var PartnerID = Convert.ToInt32(((DataRowView)operationPackageListBindingSource.Current)["PartnerID"]);
            var PackageOrderID = Convert.ToInt32(((DataRowView)operationPackageListBindingSource.Current)["PackageOrderID"]);

            #region Delete
            var expenses = new gama_transportationdbDataSetTableAdapters.PackageOrderExpensesViewTableAdapter().GetData(PackageOrderID);
            if (expenses != null && expenses.Rows.Count > 0)
            {
                var payAdapter = new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter();
                var packagePayAdapter = new gama_transportationdbDataSetTableAdapters.package_order_paymentsTableAdapter();
                var expensesAdapter = new gama_transportationdbDataSetTableAdapters.package_order_expensesTableAdapter();
                foreach (gama_transportationdbDataSet.PackageOrderExpensesViewRow item in expenses.Rows)
                {
                    packagePayAdapter.DeleteQuery(item.order_payments_ID);
                    expensesAdapter.DeleteQuery(item.ID);
                    payAdapter.DeleteQuery(item.partners_payments_ID);
                }
            }
            new gama_transportationdbDataSetTableAdapters.packages_order_linesTableAdapter().DeleteOrderLines(PackageOrderID); 
            new gama_transportationdbDataSetTableAdapters.packages_orderTableAdapter().DeleteQuery(PackageOrderID);
            #endregion
            
            this.operationPackageListTableAdapter.FillByOperationID(this.viewDataSet.OperationPackageList, OperationID);
        }

        private void btnCloseOperation_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("�� �� ����� ���� ����ڿ", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
            operationsTableAdapter.UpdateIsClosed(true,OperationID);
            isClosedSwitchButton.Value = true;                
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

        }

        private void buttonItem2_Click(object sender, EventArgs e)
        {
            if (operationPackageListBindingSource.Position < 0) return;
            int PackageOrderID = Convert.ToInt32(((DataRowView)operationPackageListBindingSource.Current)["PackageOrderID"]);
            new Packages.Reports.PackageOrderReportForm() { OrderID = PackageOrderID }.ShowDialog(this);
        }

        private void buttonItem1_Click(object sender, EventArgs e)
        {
            new Operations.Report.OperationOutReportForm() { operationID = OperationID }.ShowDialog(this);
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                operationPackageListBindingSource.Filter = string.Format("CustomerName LIKE '%{0}%' OR Recipient LIKE '%{0}%' OR Phones LIKE '%{0}%'", SearchBox.Text);
            }
            catch (Exception ex)
            {
            }
        }

        private void SearchBox_ButtonCustom2Click(object sender, EventArgs e)
        {
            SearchBox.Clear();
        }

        private void mainDataGridViewX_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
    }
}