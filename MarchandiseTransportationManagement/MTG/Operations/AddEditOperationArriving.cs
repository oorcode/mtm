using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Operations
{
    public partial class AddEditOperationArriving : DevComponents.DotNetBar.Metro.MetroForm
    {
        public AddEditOperationArriving()
        {
            InitializeComponent();
        }

        private void operationsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.operationsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.gama_transportationdbDataSet);

        }

        private void AddEditOperation_Load(object sender, EventArgs e)
        {
            this.locationsTableAdapter.FillByAllDepots(this.gama_transportationdbDataSet.locations);
            if (OperationID == 0)
            {
                operationsBindingSource.AddNew();
                ((DataRowView)operationsBindingSource.Current)["LocationsTo_ID"] = 1;
                ((DataRowView)operationsBindingSource.Current)["IsLeaving"] = false;
                ((DataRowView)operationsBindingSource.Current)["IsClosed"] = false;
            }
            else
            {
                this.operationsTableAdapter.FillByID(this.gama_transportationdbDataSet.operations,OperationID);
            }

        }

        public int OperationID { get; set; }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (OperationID == 0)
            {
                var id = Convert.ToInt32(((DataRowView)locationsBindingSource.Current)["ID"]);
                var existOpen = operationsTableAdapter.GetIFLocationFromIsNotClosed(id).GetValueOrDefault(0);
                if (existOpen > 0)
                {
                    OperationID = existOpen;
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                    return;
                }
            }
            try
            {
                this.Validate();
                this.operationsBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.gama_transportationdbDataSet);
                DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }
    }
}