using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Operations
{
    public partial class AddEditOperationLeaving : DevComponents.DotNetBar.Metro.MetroForm
    {
        public AddEditOperationLeaving()
        {
            InitializeComponent();
        }

        private void AddEditOperation_Load(object sender, EventArgs e)
        {
            this.locationsTableAdapter.FillByAllDepots(this.gama_transportationdbDataSet.locations);
            if (OperationID == 0)
            {
                operationsBindingSource.AddNew();
               
                ((DataRowView)operationsBindingSource.Current)["LocationsFrom_ID"] = 1;
                ((DataRowView)operationsBindingSource.Current)["IsLeaving"] = true;
                ((DataRowView)operationsBindingSource.Current)["IsClosed"] = false;
                comboBoxEx2.SelectedIndex = 0;
            }
            else
            {
                this.operationsTableAdapter.FillByID(this.gama_transportationdbDataSet.operations,OperationID);
            }
        }

        public int OperationID { get; set; }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (OperationID == 0)
            {
                var id =Convert.ToInt32( ((DataRowView)locationsBindingSource.Current)["ID"]);
                var existOpen = operationsTableAdapter.GetIFLocationToIsNotClosed(id).GetValueOrDefault(0);
                if (existOpen > 0)
                {
                    OperationID = existOpen;
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                    return;
                }
            }
            try
            {
                this.Validate();
                this.operationsBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.gama_transportationdbDataSet);
                DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}