using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Operations
{
    public partial class OperationsListJournal : UserControl,IIUserControl
    {
        public int OperationID { get; set; }
        public gama_transportationdbDataSet.operationsRow OperationX { get; set; }
        public OperationsListJournal()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
        }

        private void OperationsList_Load(object sender, EventArgs e)
        {
            dateTimeInput1.Value.AddDays(-7);
            partnersTableAdapter.Fill(gama_transportationdbDataSet.partners);
            this.locationsTableAdapter.Fill(this.gama_transportationdbDataSet.locations);
            this.operationsTableAdapter.Fill(this.gama_transportationdbDataSet.operations);
            mainDataGridViewX.Sort(datDataGridViewTextBoxColumn, ListSortDirection.Descending);
        }

        private void mainDataGridViewX_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (operationsBindingSource.Position < 0) return;
            int id = Convert.ToInt32(((DataRowView)operationsBindingSource.Current)["ID"]);
            bool isLeaving = Convert.ToBoolean(((DataRowView)operationsBindingSource.Current)["IsLeaving"]);
            var xOperation = new Operations.AddEditOperationPackageList();
            xOperation.OperationID = id;

            ((MainForm)this.FindForm()).AddTab(xOperation, isLeaving ? "����� ����: " + id : "����� ����: " + id);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (operationsBindingSource.Position < 0) return;
                if (MessageBox.Show("����� ��� ������� �����Ͽ", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
                int id = Convert.ToInt32(((DataRowView)operationsBindingSource.Current)["ID"]);
                new gama_transportationdbDataSetTableAdapters.packages_orderTableAdapter().DeleteByOperation(id);
                operationsTableAdapter.DeleteQuery(id);
                operationsBindingSource.RemoveCurrent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (operationsBindingSource.Position < 0) return;
            int id = Convert.ToInt32(((DataRowView)operationsBindingSource.Current)["ID"]);
            new Operations.Report.OperationOutReportForm() { operationID = id }.ShowDialog(this);
        }

        private void dataGridViewX1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void textBoxX1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                operationPackageListBindingSource.Filter = string.Format("CustomerName LIKE '%{0}%' OR Phones LIKE '%{0}%'  OR Recipient LIKE '%{0}%'  OR PhoneRecip LIKE '%{0}%'  OR Note LIKE '%{0}%'  OR Name LIKE '%{0}%' ", textBoxX1.Text);
            }
            catch (Exception ex)
            {
            }
        }

        private void btnFillByDate_Click(object sender, EventArgs e)
        {
            try
            {
                this.operationPackageListTableAdapter.FillByDate(viewDataSet.OperationPackageList, dateTimeInput1.Value.Date, dateTimeInput2.Value.AddDays(1).Date);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void Refill()
        {
            OperationsList_Load(null, null);
            btnFillByDate.PerformClick();
        }

        private void mainDataGridViewX_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(((DataRowView)operationsBindingSource.Current)["ID"]);
                operationPackageListTableAdapter.FillByOperationID(viewDataSet.OperationPackageList, id);
            }
            catch (Exception )
            {
            }
        }
    }
}