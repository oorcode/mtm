﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace MTM
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.CurrentCulture = Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fr-FR")
            {
                NumberFormat = new System.Globalization.NumberFormatInfo() { NumberDecimalSeparator = ".", NumberGroupSeparator = ",", DigitSubstitution = System.Globalization.DigitShapes.None },
               
                DateTimeFormat = new System.Globalization.DateTimeFormatInfo() { ShortDatePattern = "yyyy/MM/dd", DayNames = new string[] { "الأحد", "الإثنين", "الثلاثاء", "الإربعاء", "الخميس", "الجمعة", "السبت" }, MonthNames = new string[] { "جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر",""} }
            };
            Application.CurrentCulture = Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("fr-FR")
            {
                NumberFormat = new System.Globalization.NumberFormatInfo() { NumberDecimalSeparator = ".", NumberGroupSeparator = ",", DigitSubstitution = System.Globalization.DigitShapes.None },
                DateTimeFormat = new System.Globalization.DateTimeFormatInfo() { ShortDatePattern = "yyyy/MM/dd", DayNames = new string[] { "الأحد", "الإثنين", "الثلاثاء", "الإربعاء", "الخميس", "الجمعة", "السبت" }, MonthNames = new string[] { "جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر",""} }
            };
            Application.Run(new MainForm());
        }
    }
}
