using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Partners
{
    public partial class SelectPartner : DevComponents.DotNetBar.Metro.MetroForm
    {
        public bool IsMerchant { get; set; }
        public bool IsFactory { get; set; }
        public bool IsWarehouse { get; set; }
        public int SelectedPartnerID { get; set; }
        public string NameDepot { get; set; }
        public SelectPartner()
        {
            InitializeComponent();
        }

        private void SelectPartner_Load(object sender, EventArgs e)
        {
            if (IsMerchant && IsFactory)
            {
                this.partnersTableAdapter.FillBy(gama_transportationdbDataSet.partners, IsFactory, IsMerchant);
            }
            else if (IsMerchant)
            {
                this.partnersTableAdapter.FillByMerchant(this.gama_transportationdbDataSet.partners);
            }
            else if (IsFactory)
            {
                this.partnersTableAdapter.FillByFactory(this.gama_transportationdbDataSet.partners);
            }
            else if (IsWarehouse)
            {
                this.partnersTableAdapter.FillByDepot(this.gama_transportationdbDataSet.partners);
            }
            else
            {
                this.partnersTableAdapter.Fill(this.gama_transportationdbDataSet.partners);
            }
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Down)
            {
                partnersBindingSource.MoveNext();
            }
            else if (e.KeyData == Keys.Up)
            {
                partnersBindingSource.MovePrevious();
            }
            else if (e.KeyData == Keys.Enter)
            {
                Ok();
            }
        }


        private void mainDataGridViewX_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Ok();
        }

        private void Ok()
        {
            if (partnersBindingSource.Position < 0) return;
            IsFactory = Convert.ToBoolean(((DataRowView)partnersBindingSource.Current)["IsFactory"]);
            IsMerchant = Convert.ToBoolean(((DataRowView)partnersBindingSource.Current)["IsMerchant"]);
            IsWarehouse = Convert.ToBoolean(((DataRowView)partnersBindingSource.Current)["IsDepot"]);
            SelectedPartnerID = Convert.ToInt32(((DataRowView)partnersBindingSource.Current)["ID"]);
            NameDepot = ((DataRowView)partnersBindingSource.Current)["CustomerName"].ToString();
            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            SelectedPartnerID = 0;
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            Ok();
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                partnersBindingSource.Filter = string.Format("CustomerName LIKE '%{0}%' ", SearchBox.Text);
            }
            catch (Exception ex)
            {
            }
        }
    }
}