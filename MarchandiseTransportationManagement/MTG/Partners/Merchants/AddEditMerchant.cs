using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Partners.Merchant
{
    public partial class AddEditMerchant : DevComponents.DotNetBar.Metro.MetroForm
    {
        int ID = 0;
        public AddEditMerchant(int Id_)
        {
            InitializeComponent();
            ID = Id_;
        }

        private void AddEditFactory_Load(object sender, EventArgs e)
        {
            if (ID == 0)
            {
                partnersBindingSource.AddNew();
                ((DataRowView)partnersBindingSource.Current)["IsMerchant"] = true;

            }
            else
            {
                this.partnersTableAdapter.FillByID(this.gama_transportationdbDataSet.partners, ID);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.partnersBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.gama_transportationdbDataSet);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }
    }
}