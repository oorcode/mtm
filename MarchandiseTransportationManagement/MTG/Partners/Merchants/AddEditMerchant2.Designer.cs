﻿namespace MTM.Partners.Merchant
{
    partial class AddEditMerchant2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gama_transportationdbDataSet = new MTM.gama_transportationdbDataSet();
            this.partnersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.partnersTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.partnersTableAdapter();
            this.tableAdapterManager = new MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager();
            this.customerNameTextBoxX = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.phonesTextBoxX = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.mainDataGridViewX = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.productNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partnersIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productsTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.productsTableAdapter();
            this.btnDeleteProduct = new DevComponents.DotNetBar.ButtonX();
            this.btnAddProduct = new DevComponents.DotNetBar.ButtonX();
            this.btnEdit = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.partnersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGridViewX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gama_transportationdbDataSet
            // 
            this.gama_transportationdbDataSet.DataSetName = "gama_transportationdbDataSet";
            this.gama_transportationdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // partnersBindingSource
            // 
            this.partnersBindingSource.DataMember = "partners";
            this.partnersBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // partnersTableAdapter
            // 
            this.partnersTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.deliverersTableAdapter = null;
            this.tableAdapterManager.discountlinesTableAdapter = null;
            this.tableAdapterManager.discountsTableAdapter = null;
            this.tableAdapterManager.locationsTableAdapter = null;
            this.tableAdapterManager.operationsTableAdapter = null;
            this.tableAdapterManager.package_delivrer_orderTableAdapter = null;
            this.tableAdapterManager.package_order_expensesTableAdapter = null;
            this.tableAdapterManager.package_order_paymentsTableAdapter = null;
            this.tableAdapterManager.packages_order_line_delivererTableAdapter = null;
            this.tableAdapterManager.packages_order_linesTableAdapter = null;
            this.tableAdapterManager.packages_orderTableAdapter = null;
            this.tableAdapterManager.partners_paymentsTableAdapter = null;
            this.tableAdapterManager.partnersTableAdapter = this.partnersTableAdapter;
            this.tableAdapterManager.productsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.workers_accountTableAdapter = null;
            this.tableAdapterManager.workers_paymentsTableAdapter = null;
            this.tableAdapterManager.workersTableAdapter = null;
            // 
            // customerNameTextBoxX
            // 
            this.customerNameTextBoxX.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.customerNameTextBoxX.Border.Class = "TextBoxBorder";
            this.customerNameTextBoxX.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.customerNameTextBoxX.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.partnersBindingSource, "CustomerName", true));
            this.customerNameTextBoxX.DisabledBackColor = System.Drawing.Color.White;
            this.customerNameTextBoxX.ForeColor = System.Drawing.Color.Black;
            this.customerNameTextBoxX.Location = new System.Drawing.Point(12, 12);
            this.customerNameTextBoxX.Name = "customerNameTextBoxX";
            this.customerNameTextBoxX.PreventEnterBeep = true;
            this.customerNameTextBoxX.Size = new System.Drawing.Size(244, 27);
            this.customerNameTextBoxX.TabIndex = 0;
            // 
            // phonesTextBoxX
            // 
            this.phonesTextBoxX.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.phonesTextBoxX.Border.Class = "TextBoxBorder";
            this.phonesTextBoxX.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.phonesTextBoxX.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.partnersBindingSource, "Phones", true));
            this.phonesTextBoxX.DisabledBackColor = System.Drawing.Color.White;
            this.phonesTextBoxX.ForeColor = System.Drawing.Color.Black;
            this.phonesTextBoxX.Location = new System.Drawing.Point(12, 45);
            this.phonesTextBoxX.Name = "phonesTextBoxX";
            this.phonesTextBoxX.PreventEnterBeep = true;
            this.phonesTextBoxX.Size = new System.Drawing.Size(244, 27);
            this.phonesTextBoxX.TabIndex = 1;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(262, 45);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(91, 27);
            this.labelX1.TabIndex = 4;
            this.labelX1.Text = "الهاتف:";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(262, 12);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(91, 27);
            this.labelX2.TabIndex = 4;
            this.labelX2.Text = "إسم المصنع:";
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(12, 427);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(111, 32);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.Symbol = "";
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "خروج";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(129, 427);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(127, 32);
            this.btnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSave.Symbol = "";
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "حفظ";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // mainDataGridViewX
            // 
            this.mainDataGridViewX.AllowUserToAddRows = false;
            this.mainDataGridViewX.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            this.mainDataGridViewX.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.mainDataGridViewX.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mainDataGridViewX.AutoGenerateColumns = false;
            this.mainDataGridViewX.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mainDataGridViewX.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.mainDataGridViewX.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mainDataGridViewX.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.productNameDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.iDDataGridViewTextBoxColumn,
            this.partnersIDDataGridViewTextBoxColumn});
            this.mainDataGridViewX.DataSource = this.productsBindingSource;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.mainDataGridViewX.DefaultCellStyle = dataGridViewCellStyle10;
            this.mainDataGridViewX.EnableHeadersVisualStyles = false;
            this.mainDataGridViewX.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.mainDataGridViewX.Location = new System.Drawing.Point(12, 117);
            this.mainDataGridViewX.Name = "mainDataGridViewX";
            this.mainDataGridViewX.ReadOnly = true;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mainDataGridViewX.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.mainDataGridViewX.RowHeadersWidth = 32;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.mainDataGridViewX.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.mainDataGridViewX.RowTemplate.Height = 32;
            this.mainDataGridViewX.Size = new System.Drawing.Size(346, 304);
            this.mainDataGridViewX.TabIndex = 5;
            // 
            // productNameDataGridViewTextBoxColumn
            // 
            this.productNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.productNameDataGridViewTextBoxColumn.DataPropertyName = "ProductName";
            this.productNameDataGridViewTextBoxColumn.HeaderText = "إسم المنتج";
            this.productNameDataGridViewTextBoxColumn.Name = "productNameDataGridViewTextBoxColumn";
            this.productNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N2";
            this.priceDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this.priceDataGridViewTextBoxColumn.HeaderText = "سعر النقل";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // partnersIDDataGridViewTextBoxColumn
            // 
            this.partnersIDDataGridViewTextBoxColumn.DataPropertyName = "partners_ID";
            this.partnersIDDataGridViewTextBoxColumn.HeaderText = "partners_ID";
            this.partnersIDDataGridViewTextBoxColumn.Name = "partnersIDDataGridViewTextBoxColumn";
            this.partnersIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.partnersIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // productsBindingSource
            // 
            this.productsBindingSource.DataMember = "fk_products_partners1";
            this.productsBindingSource.DataSource = this.partnersBindingSource;
            // 
            // productsTableAdapter
            // 
            this.productsTableAdapter.ClearBeforeFill = true;
            // 
            // btnDeleteProduct
            // 
            this.btnDeleteProduct.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDeleteProduct.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDeleteProduct.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteProduct.Location = new System.Drawing.Point(12, 78);
            this.btnDeleteProduct.Name = "btnDeleteProduct";
            this.btnDeleteProduct.Size = new System.Drawing.Size(106, 33);
            this.btnDeleteProduct.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnDeleteProduct.Symbol = "";
            this.btnDeleteProduct.TabIndex = 4;
            this.btnDeleteProduct.Text = "إلغاء منتج";
            this.btnDeleteProduct.Click += new System.EventHandler(this.btnDeleteProduct_Click);
            // 
            // btnAddProduct
            // 
            this.btnAddProduct.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddProduct.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddProduct.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProduct.Location = new System.Drawing.Point(236, 78);
            this.btnAddProduct.Name = "btnAddProduct";
            this.btnAddProduct.Size = new System.Drawing.Size(106, 33);
            this.btnAddProduct.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddProduct.Symbol = "";
            this.btnAddProduct.TabIndex = 2;
            this.btnAddProduct.Text = "إضافة منتج";
            this.btnAddProduct.Click += new System.EventHandler(this.btnAddProduct_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnEdit.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Location = new System.Drawing.Point(124, 78);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(106, 33);
            this.btnEdit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnEdit.Symbol = "";
            this.btnEdit.TabIndex = 3;
            this.btnEdit.Text = "فتح";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // AddEditFactory
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(370, 471);
            this.ControlBox = false;
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnDeleteProduct);
            this.Controls.Add(this.btnAddProduct);
            this.Controls.Add(this.mainDataGridViewX);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.phonesTextBoxX);
            this.Controls.Add(this.customerNameTextBoxX);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "AddEditFactory";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تسجيل مصنع";
            this.Load += new System.EventHandler(this.AddEditFactory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.partnersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGridViewX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private gama_transportationdbDataSet gama_transportationdbDataSet;
        private System.Windows.Forms.BindingSource partnersBindingSource;
        private gama_transportationdbDataSetTableAdapters.partnersTableAdapter partnersTableAdapter;
        private gama_transportationdbDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private DevComponents.DotNetBar.Controls.TextBoxX customerNameTextBoxX;
        private DevComponents.DotNetBar.Controls.TextBoxX phonesTextBoxX;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private DevComponents.DotNetBar.Controls.DataGridViewX mainDataGridViewX;
        private System.Windows.Forms.BindingSource productsBindingSource;
        private gama_transportationdbDataSetTableAdapters.productsTableAdapter productsTableAdapter;
        private DevComponents.DotNetBar.ButtonX btnDeleteProduct;
        private DevComponents.DotNetBar.ButtonX btnAddProduct;
        private System.Windows.Forms.DataGridViewTextBoxColumn productNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn partnersIDDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.ButtonX btnEdit;
    }
}