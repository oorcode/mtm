using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Microsoft.Reporting.WinForms;

namespace MTM.Partners.Reports
{
    public partial class ReleverReportForm : DevComponents.DotNetBar.Metro.MetroForm
    {
        public int PartnerID { get; set; }
        public bool IsWarehouse { get; set; }
        public ReleverReportForm()
        {
            InitializeComponent();
            this.FormClosing += (se, ee) =>
            {
                this.reportViewer1.LocalReport.ReleaseSandboxAppDomain();
            };
        }

        private void PrintReportForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.partners' table. You can move, or remove it, as needed.
            this.partnersTableAdapter.FillByID(this.gama_transportationdbDataSet.partners,PartnerID);
            if (IsWarehouse)
            {
                this.releverTableAdapter.FillByDepot(viewDataSet.Relever, PartnerID);
            }
            else
            {
                this.releverTableAdapter.Fill(viewDataSet.Relever, PartnerID);
            }

            ReportParameter rp1, rp2, rp3, rp4;
            SSUtilities.SSConfigFile cfg = new SSUtilities.SSConfigFile();
            rp1 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName, ""));
            rp2 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity, ""));
            rp3 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1, ""));
            rp4 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, ""));
            var rp5 = new ReportParameter("OldSolde", (0.00M).ToString()); 
            reportViewer1.LocalReport.SetParameters(new ReportParameter[] { rp1, rp2, rp3, rp4,rp5 });
            
            this.reportViewer1.RefreshReport();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            decimal balance = 0;
            if (IsWarehouse)
            {
                this.releverTableAdapter.FillByDepotDate(viewDataSet.Relever, dateTimeInput1.Value.Date, dateTimeInput2.Value.Date.AddDays(1), PartnerID);
                balance = releverTableAdapter.GetLastBalanceDepot(dateTimeInput1.Value.Date, PartnerID).GetValueOrDefault(0);
            }
            else
            {
                this.releverTableAdapter.FillByDate(viewDataSet.Relever, dateTimeInput1.Value.Date, dateTimeInput2.Value.Date.AddDays(1), PartnerID);
                balance = releverTableAdapter.GetLastBalance(dateTimeInput1.Value.Date, PartnerID).GetValueOrDefault(0);
            }
                //viewDataSet.partners.FirstOrDefault().Balance;
            var rp5 = new ReportParameter("OldSolde", balance.ToString("N2"));
            var rp6 = new ReportParameter("dat1",string.Format("{0} ��� {1}",dateTimeInput1.Value.ToString("d"),dateTimeInput2.Value.ToString("d")));
            reportViewer1.LocalReport.SetParameters(new ReportParameter[] { rp5, rp6 });

            this.reportViewer1.RefreshReport();

        }
    }
}