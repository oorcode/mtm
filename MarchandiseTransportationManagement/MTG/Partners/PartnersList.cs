using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Partners
{
    public partial class PartnersList : UserControl,IIUserControl
    {
        public PartnersList()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (MerchantChBox.Checked)
            {
                new Merchant.AddEditMerchant2(0).ShowDialog();
            }
            else if (WarehouseChBox.Checked)
            {
                new Warehouse.AddEditWarehouse(0).ShowDialog();
            }
            else if (FactoryChBox.Checked)
            {
                new Factory.AddEditFactory(0).ShowDialog();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (partnersBindingSource.Position < 0) return;
            int id = Convert.ToInt32(((DataRowView)partnersBindingSource.Current)["ID"]);
            if (MerchantChBox.Checked)
            {
                new Merchant.AddEditMerchant2(id).ShowDialog();
            }
            else if (WarehouseChBox.Checked)
            {
                new Warehouse.AddEditWarehouse(id).ShowDialog();
            }
            else if (FactoryChBox.Checked)
            {
                new Factory.AddEditFactory(id).ShowDialog();
            }
        }

        private void PartnersList_Load(object sender, EventArgs e)
        {
            partnersTableAdapter1.Fill(gama_transportationdbDataSet.partners);
            // TODO: This line of code loads data into the 'viewDataSet.partners' table. You can move, or remove it, as needed.
            dateTimeInput1.Value = dateTimeInput1.Value.Date.AddDays(-7);
            WarehouseChBox_CheckedChanged(null, null);

            partnersBindingSource.PositionChanged+=new EventHandler(partnersBindingSource_PositionChanged);
            dataGridViewX1.Sort(datDataGridViewTextBoxColumn, ListSortDirection.Descending);
        }

        private void WarehouseChBox_CheckedChanged(object sender, EventArgs e)
        {
            if (WarehouseChBox.Checked)
            {
                this.partnersTableAdapter.Fill(this.viewDataSet.partners);
            }
            else if (FactoryChBox.Checked)
            {
                this.partnersTableAdapter.FillByNonDepot(this.viewDataSet.partners, true, false);
            }
            else
            {
                this.partnersTableAdapter.FillByNonDepot(this.viewDataSet.partners, false, true);

            }
                     
            foreach (DataGridViewRow item in mainDataGridViewX.Rows)
            {
                try
                {
                    var balance = Convert.ToDecimal(item.Cells[balanceDataGridViewTextBoxColumn.Name].Value);
                    if (balance > 0)
                    {
                        item.DefaultCellStyle.BackColor = Color.OrangeRed;
                    }
                    else if (balance < 0)
                    {
                        item.DefaultCellStyle.BackColor = Color.Gold;
                    }
                    else
                    {
                        item.DefaultCellStyle.BackColor = Color.ForestGreen;
                    }
                }
                catch (Exception)
                {
                }
            }

        }

        private void textBoxX1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                partnersBindingSource.Filter = string.Format("CustomerName LIKE '%{0}%' OR Phones LIKE '%{0}%' ", textBoxX1.Text);
            }
            catch (Exception ex)
            {
            }
        }

        private void partnersBindingSource_PositionChanged(object sender, EventArgs e)
        {
            if (partnersBindingSource.Position < 0) return;           
            int id = Convert.ToInt32(((DataRowView)partnersBindingSource.Current)["ID"]);
            if (WarehouseChBox.Checked)
            {
                this.releverTableAdapter.FillByDepotDate(this.viewDataSet.Relever, dateTimeInput1.Value.Date, dateTimeInput2.Value.Date.AddDays(1), id);
            }
            else
            {
                this.releverTableAdapter.FillByDate(this.viewDataSet.Relever, dateTimeInput1.Value.Date, dateTimeInput2.Value.Date.AddDays(1), id);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (partnersBindingSource.Position < 0) return;
            int id = Convert.ToInt32(((DataRowView)partnersBindingSource.Current)["ID"]);
            new Reports.ReleverReportForm() { PartnerID = id, IsWarehouse = WarehouseChBox.Checked }.ShowDialog(this);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (partnersBindingSource.Position < 0) { return; }
                if (MessageBox.Show("����� ����� ����ݿ", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
                if (WarehouseChBox.Checked)
                {
                    new gama_transportationdbDataSetTableAdapters.locationsTableAdapter().DeleteByPartner(Convert.ToInt32(((DataRowView)partnersBindingSource.Current)["ID"]));
                }
                new gama_transportationdbDataSetTableAdapters.partnersTableAdapter().DeleteQuery(Convert.ToInt32(((DataRowView)partnersBindingSource.Current)["ID"]));
                partnersBindingSource.RemoveCurrent();
            }
            catch (Exception ex )
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnFillByDate_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(((DataRowView)partnersBindingSource.Current)["ID"]);
            if (WarehouseChBox.Checked)
            {
                this.releverTableAdapter.FillByDepotDate(this.viewDataSet.Relever,dateTimeInput1.Value.Date,dateTimeInput2.Value.Date.AddDays(1),id);
            }
            else if (FactoryChBox.Checked)
            {
                this.releverTableAdapter.FillByDate(this.viewDataSet.Relever, dateTimeInput1.Value.Date, dateTimeInput2.Value.Date.AddDays(1), id);
            }
            else
            {
                this.releverTableAdapter.FillByDate(this.viewDataSet.Relever, dateTimeInput1.Value.Date, dateTimeInput2.Value.Date.AddDays(1), id);
            }
        }

        private void dataGridViewX1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dataGridViewX1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (releverBindingSource.Position < 0) return;
            if (((DataRowView)releverBindingSource.Current)["DelivererName"].ToString().Contains("����") || ((DataRowView)releverBindingSource.Current)["DelivererName"].ToString().Contains("�����")
                 || ((DataRowView)releverBindingSource.Current)["DelivererName"].ToString().Contains("�����"))
            {
                var packageOrder = new Packages.AddEditPackagesOrder();
                packageOrder.PartnerID = Convert.ToInt32(((DataRowView)releverBindingSource.Current)["partners_ID"]);
                packageOrder.PackageOrderID = Convert.ToInt32(((DataRowView)releverBindingSource.Current)["ID"]);
                packageOrder.IsFactory = WarehouseChBox.Checked;
                packageOrder.IsLeaving = true;
                packageOrder.ShowDialog(this);
            }
            else
                if (((DataRowView)releverBindingSource.Current)["DelivererName"].ToString().Contains("�����")|| ((DataRowView)releverBindingSource.Current)["DelivererName"].ToString().Contains("��� ����� ����"))
                {
                    if (releverBindingSource.Position < 0) return;
                    int idLine = Convert.ToInt32(((DataRowView)releverBindingSource.Current)["ID"]);
                    int idyear = Convert.ToInt32(((DataRowView)releverBindingSource.Current)["SerialYear"]);
                    int partnerID = Convert.ToInt32(((DataRowView)releverBindingSource.Current)["partners_ID"]);
                new Deliverer.AddEditDeliveryOrder() { IdYear = idyear, OrderID = idLine, PartnerID = partnerID }.ShowDialog(this);
            }
        }

        private void partnersBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            try
            {
                var result = viewDataSet.partners.Sum(x => x.Balance);
                doubleInput1.Value = Convert.ToDouble(result);
            }
            catch (Exception)
            {
            }
        }

        public void Refill()
        {
            try
            {
                WarehouseChBox_CheckedChanged(null, null);
                btnFillByDate_Click(null, null);
                //partnersBindingSource_ListChanged(null, null);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}