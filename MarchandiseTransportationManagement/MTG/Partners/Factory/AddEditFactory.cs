using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Partners.Factory
{
    public partial class AddEditFactory : DevComponents.DotNetBar.Metro.MetroForm
    {
        int ID = 0;
        public AddEditFactory(int Id_)
        {
            InitializeComponent();
            ID = Id_;
        }

        private void AddEditFactory_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.products' table. You can move, or remove it, as needed.
            if (ID == 0)
            {
                partnersBindingSource.AddNew();
                ((DataRowView)partnersBindingSource.Current)["IsFactory"]=true;
            }
            else
            {
                this.partnersTableAdapter.FillByID(this.gama_transportationdbDataSet.partners, ID);
            }
            this.productsTableAdapter.FillByPartnerID(this.gama_transportationdbDataSet.products, ID);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }

        private void Save()
        {
            this.Validate();
            this.partnersBindingSource.EndEdit();
            this.productsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.gama_transportationdbDataSet);
        }

        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            Save();
            if (ID == 0)
            {
                ID = partnersTableAdapter.MaxID().GetValueOrDefault(0);
                this.partnersTableAdapter.FillByID(this.gama_transportationdbDataSet.partners, ID);
            }
            new Products.AddEditProduct(0,ID).ShowDialog();
            this.productsTableAdapter.FillByPartnerID(this.gama_transportationdbDataSet.products, ID);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (productsBindingSource.Position < 0) return;
            if (new Products.AddEditProduct(Convert.ToInt32(((DataRowView)productsBindingSource.Current)["ID"])).ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
        }

        private void btnDeleteProduct_Click(object sender, EventArgs e)
        {
            if (productsBindingSource.Position < 0) return;
            if (MessageBox.Show("����� ��� ������ �����Ͽ", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            try
            {
                productsTableAdapter.DeleteQuery(Convert.ToInt32(((DataRowView)productsBindingSource.Current)["ID"]));
                productsBindingSource.RemoveCurrent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}