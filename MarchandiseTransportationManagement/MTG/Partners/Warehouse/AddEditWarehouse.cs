using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Partners.Warehouse
{
    public partial class AddEditWarehouse : DevComponents.DotNetBar.Metro.MetroForm
    {
        int ID = 0;
        public AddEditWarehouse(int Id_)
        {
            InitializeComponent();
            ID = Id_;
        }

        private void AddEditFactory_Load(object sender, EventArgs e)
        {
            if (ID == 0)
            {
                partnersBindingSource.AddNew();
                ((DataRowView)partnersBindingSource.Current)["IsDepot"]=true;
            }
            else
            {
                this.partnersTableAdapter.FillByID(this.gama_transportationdbDataSet.partners, ID);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                
                this.Validate();
                this.partnersBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.gama_transportationdbDataSet);
                if (ID == 0)
                {
                    new gama_transportationdbDataSetTableAdapters.locationsTableAdapter().Insert(customerNameTextBoxX.Text, 0, partnersTableAdapter.MaxID().GetValueOrDefault(0));
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }
    }
}