using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Deliverer
{
    public partial class DeliverersList : UserControl,IIUserControl
    {
        public DeliverersList()
        {
            InitializeComponent();
        }

        private void DeliverersList_Load(object sender, EventArgs e)
        {
            this.deliverersTableAdapter.Fill(gama_transportationdbDataSet.deliverers);
            this.deliverersBindingSource.PositionChanged += new EventHandler(deliverersBindingSource_PositionChanged);
        }

        private void deliverersBindingSource_PositionChanged(object sender, EventArgs e)
        {
            if (deliverersBindingSource.Position < 0) return;
            try
            {
                var id = Convert.ToInt32(((DataRowView)deliverersBindingSource.Current)["ID"]);
                deliveryOrderListTableAdapter.FillByDelivererID(viewDataSet.DeliveryOrderList, id);
            }
            catch (Exception)
            {
            }
        }

        private void textBoxX1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                deliverersBindingSource.Filter = string.Format("DelivererName LIKE '%{0}%' OR Phones LIKE '%{0}%' OR TruckModel LIKE '%{0}%'", textBoxX1.Text);
            }
            catch (Exception ex)
            {
            }
        }

        private void Refresh1_Click(object sender, EventArgs e)
        {
            if (deliverersBindingSource.Position < 0) return;
            try
            {
                var id = GetSelectedId();
                deliveryOrderListTableAdapter.FillByDelivererID(viewDataSet.DeliveryOrderList, id);
            }
            catch (Exception)
            {
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            new AddEditDeliverer(0).ShowDialog(this);
            Refresh1();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (deliverersBindingSource.Position < 0) return;
            try
            {
                var id = GetSelectedId();
                new AddEditDeliverer(id).ShowDialog(this);
                this.deliverersTableAdapter.Fill(gama_transportationdbDataSet.deliverers);
            }
            catch (Exception)
            {
            }
        }

        private void Refresh1() 
        {

            this.deliverersTableAdapter.Fill(gama_transportationdbDataSet.deliverers);
        }

        private int GetSelectedId() 
        {
            if (deliverersBindingSource.Position < 0)
            { 
                return 0; 
            }
            return Convert.ToInt32(((DataRowView)deliverersBindingSource.Current)["ID"]);        
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (deliverersBindingSource.Position < 0) return;
            if (MessageBox.Show("����� ����� ��������", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
            try
            {
                var id = GetSelectedId();            
                this.deliverersTableAdapter.DeleteById(id);
                Refresh1();
            }
            catch (Exception)
            {
            }

        }

        private void dataGridViewX1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        public void Refill()
        {
            Refresh1_Click(null, null);
        }
    }
}