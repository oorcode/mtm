using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Deliverer
{
    public partial class SelectDeliverer : DevComponents.DotNetBar.Metro.MetroForm
    {
        public SelectDeliverer()
        {
            InitializeComponent();
        }

        private void SelectPartner_Load(object sender, EventArgs e)
        {
            this.deliverersTableAdapter.Fill(this.gama_transportationdbDataSet.deliverers);
            SearchBox.Focus();
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Down)
            {
                deliverersBindingSource.MoveNext();
            }
            else if (e.KeyData == Keys.Up)
            {
                deliverersBindingSource.MovePrevious();
            }
            else if (e.KeyData == Keys.Enter)
            {
                Ok();
            }
        }

        public int SelectedDelivererID { get; set; }

        private void mainDataGridViewX_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Ok();
        }

        private void Ok()
        {
            if (deliverersBindingSource.Position < 0) return;
            SelectedDelivererID = Convert.ToInt32(((DataRowView)deliverersBindingSource.Current)["ID"]);
            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            SelectedDelivererID = 0;
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            Ok();
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                deliverersBindingSource.Filter = string.Format("DelivererName LIKE '%{0}%' OR Phones LIKE '%{0}%' OR Truckmodel LIKE '%{0}%'", SearchBox.Text);
            }
            catch (Exception ex)
            {
            }
        }
    }
}