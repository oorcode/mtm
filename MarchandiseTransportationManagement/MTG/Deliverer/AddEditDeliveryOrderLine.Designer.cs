﻿namespace MTM.Deliverer
{
    partial class AddEditDeliveryOrderLine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.packages_order_linesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gama_transportationdbDataSet = new MTM.gama_transportationdbDataSet();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.packages_order_linesTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.packages_order_linesTableAdapter();
            this.tableAdapterManager = new MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager();
            this.priceDoubleInput = new DevComponents.Editors.DoubleInput();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.doubleInput1 = new DevComponents.Editors.DoubleInput();
            ((System.ComponentModel.ISupportInitialize)(this.packages_order_linesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceDoubleInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.doubleInput1)).BeginInit();
            this.SuspendLayout();
            // 
            // packages_order_linesBindingSource
            // 
            this.packages_order_linesBindingSource.DataMember = "packages_order_lines";
            this.packages_order_linesBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // gama_transportationdbDataSet
            // 
            this.gama_transportationdbDataSet.DataSetName = "gama_transportationdbDataSet";
            this.gama_transportationdbDataSet.EnforceConstraints = false;
            this.gama_transportationdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(191, 33);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(111, 27);
            this.labelX3.TabIndex = 53;
            this.labelX3.Text = "الكمية:";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(12, 120);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(111, 32);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.Symbol = "";
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "خروج";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSave.Location = new System.Drawing.Point(129, 120);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(111, 32);
            this.btnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSave.Symbol = "";
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "حفظ";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // packages_order_linesTableAdapter
            // 
            this.packages_order_linesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.deliverersTableAdapter = null;
            this.tableAdapterManager.discountlinesTableAdapter = null;
            this.tableAdapterManager.discountsTableAdapter = null;
            this.tableAdapterManager.expensesTableAdapter = null;
            this.tableAdapterManager.locationsTableAdapter = null;
            this.tableAdapterManager.operationsTableAdapter = null;
            this.tableAdapterManager.package_delivrer_orderTableAdapter = null;
            this.tableAdapterManager.package_order_expensesTableAdapter = null;
            this.tableAdapterManager.package_order_payments1TableAdapter = null;
            this.tableAdapterManager.package_order_paymentsTableAdapter = null;
            this.tableAdapterManager.packages_order_line_delivererTableAdapter = null;
            this.tableAdapterManager.packages_order_linesTableAdapter = this.packages_order_linesTableAdapter;
            this.tableAdapterManager.packages_orderTableAdapter = null;
            this.tableAdapterManager.partners_paymentsTableAdapter = null;
            this.tableAdapterManager.partners_rcTableAdapter = null;
            this.tableAdapterManager.partnersTableAdapter = null;
            this.tableAdapterManager.payment_methodsTableAdapter = null;
            this.tableAdapterManager.productsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.workers_absenceTableAdapter = null;
            this.tableAdapterManager.workers_accountTableAdapter = null;
            this.tableAdapterManager.workers_paymentsTableAdapter = null;
            this.tableAdapterManager.workersTableAdapter = null;
            // 
            // priceDoubleInput
            // 
            this.priceDoubleInput.AllowEmptyState = false;
            this.priceDoubleInput.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.priceDoubleInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.priceDoubleInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.priceDoubleInput.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.priceDoubleInput.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.packages_order_linesBindingSource, "Price", true));
            this.priceDoubleInput.FocusHighlightEnabled = true;
            this.priceDoubleInput.ForeColor = System.Drawing.Color.Black;
            this.priceDoubleInput.Increment = 1D;
            this.priceDoubleInput.Location = new System.Drawing.Point(74, 66);
            this.priceDoubleInput.MaxValue = 9999999D;
            this.priceDoubleInput.MinValue = 0D;
            this.priceDoubleInput.Name = "priceDoubleInput";
            this.priceDoubleInput.Size = new System.Drawing.Size(111, 27);
            this.priceDoubleInput.TabIndex = 1;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(191, 66);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(111, 27);
            this.labelX1.TabIndex = 53;
            this.labelX1.Text = "سعر الوحدة:";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // doubleInput1
            // 
            this.doubleInput1.AllowEmptyState = false;
            this.doubleInput1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.doubleInput1.BackgroundStyle.Class = "DateTimeInputBackground";
            this.doubleInput1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.doubleInput1.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.doubleInput1.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.packages_order_linesBindingSource, "Quantity", true));
            this.doubleInput1.FocusHighlightEnabled = true;
            this.doubleInput1.ForeColor = System.Drawing.Color.Black;
            this.doubleInput1.Increment = 1D;
            this.doubleInput1.Location = new System.Drawing.Point(74, 33);
            this.doubleInput1.MaxValue = 9999999D;
            this.doubleInput1.MinValue = 0D;
            this.doubleInput1.Name = "doubleInput1";
            this.doubleInput1.Size = new System.Drawing.Size(111, 27);
            this.doubleInput1.TabIndex = 1;
            // 
            // AddEditDeliveryOrderLine
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(376, 164);
            this.ControlBox = false;
            this.Controls.Add(this.doubleInput1);
            this.Controls.Add(this.priceDoubleInput);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.labelX3);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.Name = "AddEditDeliveryOrderLine";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "إضافة سلعة";
            this.Load += new System.EventHandler(this.AddEditDeliveryOrderLine_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AddEditDeliveryOrderLine_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.packages_order_linesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceDoubleInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.doubleInput1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private gama_transportationdbDataSet gama_transportationdbDataSet;
        private System.Windows.Forms.BindingSource packages_order_linesBindingSource;
        private gama_transportationdbDataSetTableAdapters.packages_order_linesTableAdapter packages_order_linesTableAdapter;
        private gama_transportationdbDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private DevComponents.Editors.DoubleInput priceDoubleInput;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.Editors.DoubleInput doubleInput1;
    }
}