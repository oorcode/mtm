﻿namespace MTM.Deliverer
{
    partial class AddEditDeliverer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gama_transportationdbDataSet = new MTM.gama_transportationdbDataSet();
            this.tableAdapterManager = new MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager();
            this.deliverersTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.deliverersTableAdapter();
            this.customerNameTextBoxX = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.deliverersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.phonesTextBoxX = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.textBoxX1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deliverersBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gama_transportationdbDataSet
            // 
            this.gama_transportationdbDataSet.DataSetName = "gama_transportationdbDataSet";
            this.gama_transportationdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.deliverersTableAdapter = this.deliverersTableAdapter;
            this.tableAdapterManager.discountlinesTableAdapter = null;
            this.tableAdapterManager.discountsTableAdapter = null;
            this.tableAdapterManager.locationsTableAdapter = null;
            this.tableAdapterManager.operationsTableAdapter = null;
            this.tableAdapterManager.package_delivrer_orderTableAdapter = null;
            this.tableAdapterManager.package_order_expensesTableAdapter = null;
            this.tableAdapterManager.package_order_paymentsTableAdapter = null;
            this.tableAdapterManager.packages_order_line_delivererTableAdapter = null;
            this.tableAdapterManager.packages_order_linesTableAdapter = null;
            this.tableAdapterManager.packages_orderTableAdapter = null;
            this.tableAdapterManager.partners_paymentsTableAdapter = null;
            this.tableAdapterManager.partnersTableAdapter = null;
            this.tableAdapterManager.productsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.workers_accountTableAdapter = null;
            this.tableAdapterManager.workers_paymentsTableAdapter = null;
            this.tableAdapterManager.workersTableAdapter = null;
            // 
            // deliverersTableAdapter
            // 
            this.deliverersTableAdapter.ClearBeforeFill = true;
            // 
            // customerNameTextBoxX
            // 
            this.customerNameTextBoxX.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.customerNameTextBoxX.Border.Class = "TextBoxBorder";
            this.customerNameTextBoxX.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.customerNameTextBoxX.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.deliverersBindingSource, "DelivererName", true));
            this.customerNameTextBoxX.DisabledBackColor = System.Drawing.Color.White;
            this.customerNameTextBoxX.ForeColor = System.Drawing.Color.Black;
            this.customerNameTextBoxX.Location = new System.Drawing.Point(12, 12);
            this.customerNameTextBoxX.Name = "customerNameTextBoxX";
            this.customerNameTextBoxX.PreventEnterBeep = true;
            this.customerNameTextBoxX.Size = new System.Drawing.Size(244, 27);
            this.customerNameTextBoxX.TabIndex = 0;
            // 
            // deliverersBindingSource
            // 
            this.deliverersBindingSource.DataMember = "deliverers";
            this.deliverersBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // phonesTextBoxX
            // 
            this.phonesTextBoxX.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.phonesTextBoxX.Border.Class = "TextBoxBorder";
            this.phonesTextBoxX.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.phonesTextBoxX.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.deliverersBindingSource, "TruckModel", true));
            this.phonesTextBoxX.DisabledBackColor = System.Drawing.Color.White;
            this.phonesTextBoxX.ForeColor = System.Drawing.Color.Black;
            this.phonesTextBoxX.Location = new System.Drawing.Point(12, 45);
            this.phonesTextBoxX.Name = "phonesTextBoxX";
            this.phonesTextBoxX.PreventEnterBeep = true;
            this.phonesTextBoxX.Size = new System.Drawing.Size(244, 27);
            this.phonesTextBoxX.TabIndex = 1;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(262, 45);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(91, 27);
            this.labelX1.TabIndex = 4;
            this.labelX1.Text = "نوع الشاحنة:";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(262, 12);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(91, 27);
            this.labelX2.TabIndex = 4;
            this.labelX2.Text = "إسم الحامل:";
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(12, 111);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(111, 32);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.Symbol = "";
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "خروج";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(129, 111);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(127, 32);
            this.btnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSave.Symbol = "";
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "حفظ";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // textBoxX1
            // 
            this.textBoxX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.textBoxX1.Border.Class = "TextBoxBorder";
            this.textBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.deliverersBindingSource, "Phones", true));
            this.textBoxX1.DisabledBackColor = System.Drawing.Color.White;
            this.textBoxX1.ForeColor = System.Drawing.Color.Black;
            this.textBoxX1.Location = new System.Drawing.Point(12, 78);
            this.textBoxX1.Name = "textBoxX1";
            this.textBoxX1.PreventEnterBeep = true;
            this.textBoxX1.Size = new System.Drawing.Size(244, 27);
            this.textBoxX1.TabIndex = 1;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(262, 78);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(91, 27);
            this.labelX3.TabIndex = 4;
            this.labelX3.Text = "الهاتف:";
            // 
            // AddEditDeliverer
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(370, 154);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.textBoxX1);
            this.Controls.Add(this.phonesTextBoxX);
            this.Controls.Add(this.customerNameTextBoxX);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "AddEditDeliverer";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تسجيل الحامل";
            this.Load += new System.EventHandler(this.AddEditFactory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deliverersBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private gama_transportationdbDataSet gama_transportationdbDataSet;
        private gama_transportationdbDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private DevComponents.DotNetBar.Controls.TextBoxX customerNameTextBoxX;
        private DevComponents.DotNetBar.Controls.TextBoxX phonesTextBoxX;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX1;
        private DevComponents.DotNetBar.LabelX labelX3;
        private System.Windows.Forms.BindingSource deliverersBindingSource;
        private gama_transportationdbDataSetTableAdapters.deliverersTableAdapter deliverersTableAdapter;
    }
}