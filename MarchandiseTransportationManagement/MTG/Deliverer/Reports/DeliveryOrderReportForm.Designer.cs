namespace MTM.Deliverer.Reports
{
    partial class DeliveryOrderReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.deliveryOrderListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.viewDataSet = new MTM.ViewDataSet();
            this.deliveryOrderLinesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.deliveryOrderListTableAdapter = new MTM.ViewDataSetTableAdapters.DeliveryOrderListTableAdapter();
            this.deliveryOrderLinesTableAdapter = new MTM.ViewDataSetTableAdapters.DeliveryOrderLinesTableAdapter();
            this.gama_transportationdbDataSet = new MTM.gama_transportationdbDataSet();
            this.packageOrderExpensesViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.packageOrderExpensesViewTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.PackageOrderExpensesViewTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryOrderListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryOrderLinesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packageOrderExpensesViewBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // deliveryOrderListBindingSource
            // 
            this.deliveryOrderListBindingSource.DataMember = "DeliveryOrderList";
            this.deliveryOrderListBindingSource.DataSource = this.viewDataSet;
            // 
            // viewDataSet
            // 
            this.viewDataSet.DataSetName = "ViewDataSet";
            this.viewDataSet.EnforceConstraints = false;
            this.viewDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // deliveryOrderLinesBindingSource
            // 
            this.deliveryOrderLinesBindingSource.DataMember = "DeliveryOrderLines";
            this.deliveryOrderLinesBindingSource.DataSource = this.viewDataSet;
            // 
            // reportViewer1
            // 
            this.reportViewer1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewer1.ForeColor = System.Drawing.Color.Black;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.deliveryOrderListBindingSource;
            reportDataSource2.Name = "DataSet2";
            reportDataSource2.Value = this.deliveryOrderLinesBindingSource;
            reportDataSource3.Name = "DataSet3";
            reportDataSource3.Value = this.packageOrderExpensesViewBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource3);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "MTM.Deliverer.Reports.DeliveryOrderReport.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(794, 641);
            this.reportViewer1.TabIndex = 0;
            // 
            // deliveryOrderListTableAdapter
            // 
            this.deliveryOrderListTableAdapter.ClearBeforeFill = true;
            // 
            // deliveryOrderLinesTableAdapter
            // 
            this.deliveryOrderLinesTableAdapter.ClearBeforeFill = true;
            // 
            // gama_transportationdbDataSet
            // 
            this.gama_transportationdbDataSet.DataSetName = "gama_transportationdbDataSet";
            this.gama_transportationdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // packageOrderExpensesViewBindingSource
            // 
            this.packageOrderExpensesViewBindingSource.DataMember = "PackageOrderExpensesView";
            this.packageOrderExpensesViewBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // packageOrderExpensesViewTableAdapter
            // 
            this.packageOrderExpensesViewTableAdapter.ClearBeforeFill = true;
            // 
            // DeliveryOrderReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 641);
            this.Controls.Add(this.reportViewer1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "DeliveryOrderReportForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Aper�u";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PrintReportForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.deliveryOrderListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryOrderLinesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packageOrderExpensesViewBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private ViewDataSet viewDataSet;
        private System.Windows.Forms.BindingSource deliveryOrderListBindingSource;
        private ViewDataSetTableAdapters.DeliveryOrderListTableAdapter deliveryOrderListTableAdapter;
        private System.Windows.Forms.BindingSource deliveryOrderLinesBindingSource;
        private ViewDataSetTableAdapters.DeliveryOrderLinesTableAdapter deliveryOrderLinesTableAdapter;
        private System.Windows.Forms.BindingSource packageOrderExpensesViewBindingSource;
        private gama_transportationdbDataSet gama_transportationdbDataSet;
        private gama_transportationdbDataSetTableAdapters.PackageOrderExpensesViewTableAdapter packageOrderExpensesViewTableAdapter;
    }
}