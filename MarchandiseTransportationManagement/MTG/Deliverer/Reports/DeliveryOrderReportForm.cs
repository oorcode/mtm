using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Microsoft.Reporting.WinForms;

namespace MTM.Deliverer.Reports
{
    public partial class DeliveryOrderReportForm : DevComponents.DotNetBar.Metro.MetroForm
    {
        public int OrderID { get; set; }
        public int YearID { get; set; }
        public decimal totalpayed { get; set; }
        public decimal totalreste{ get; set; }
        public DeliveryOrderReportForm()
        {
            InitializeComponent();
            this.FormClosing += (se, ee) =>
            {
                this.reportViewer1.LocalReport.ReleaseSandboxAppDomain();
            };
        }

        private void PrintReportForm_Load(object sender, EventArgs e)
        {
            packageOrderExpensesViewTableAdapter.FillByOrder(gama_transportationdbDataSet.PackageOrderExpensesView, OrderID, YearID);
            this.deliveryOrderLinesTableAdapter.FillByID(this.viewDataSet.DeliveryOrderLines,OrderID,YearID);
            this.deliveryOrderListTableAdapter.FillByID(this.viewDataSet.DeliveryOrderList,OrderID,YearID);
            ReportParameter rp1,rp2,rp3,rp4,rp5,rp6;
            SSUtilities.SSConfigFile cfg = new SSUtilities.SSConfigFile();
            rp1 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName,""));
            rp2 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity,""));
            rp3 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1,""));
            rp4 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, ""));
            rp5 = new ReportParameter("totalpayed",totalpayed.ToString());
            rp6 = new ReportParameter("totalreste", totalreste.ToString());
            reportViewer1.LocalReport.SetParameters(new ReportParameter[]{rp1,rp2,rp3,rp4,rp5,rp6});
            this.reportViewer1.RefreshReport();
        }
    }
}