using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Microsoft.Reporting.WinForms;

namespace MTM.Deliverer
{
    public partial class AddEditDeliveryOrder : DevComponents.DotNetBar.Metro.MetroForm
    {
        public int OrderID { get; set; }
        public int IdYear { get; set; }
        public bool IsFactory { get; set; }
        public int PartnerID { get; set; }
        public bool IsPayedInDepot { get; set; }
        public gama_transportationdbDataSet.package_delivrer_orderRow OrderLiv { get; set; }
        gama_transportationdbDataSetTableAdapters.packages_order_line_delivererTableAdapter deliveryOrderLineAdapter;
        public AddEditDeliveryOrder()
        {
            InitializeComponent();
        }

        private void AddEditDeliveryOrder_Load(object sender, EventArgs e)
        {
            this.partnersTableAdapter.FillByID(this.gama_transportationdbDataSet.partners, PartnerID);
           
            var partner = gama_transportationdbDataSet.partners.FirstOrDefault();
            IsFactory = partner.IsFactory;
            if (IsFactory)
            {
                this.productsTableAdapter.FillByPartnerID(this.gama_transportationdbDataSet.products, PartnerID);
            }
            else
            {
                this.productsTableAdapter.FillByNullPartner(this.gama_transportationdbDataSet.products);
            }        
            
            this.packages_order_line_delivererTableAdapter.FillByOrder(this.gama_transportationdbDataSet.packages_order_line_deliverer,OrderID, IdYear);
            
            this.deliverersTableAdapter.Fill(this.gama_transportationdbDataSet.deliverers);
            this.package_delivrer_orderTableAdapter.FillByID(gama_transportationdbDataSet.package_delivrer_order, OrderID, IdYear);
            packageOrderExpensesViewTableAdapter.FillByOrder(gama_transportationdbDataSet.PackageOrderExpensesView, OrderID, IdYear);
            
            #region DEPOT
            OrderLiv = gama_transportationdbDataSet.package_delivrer_order.FirstOrDefault();
            var depot = partnersTableAdapter.GetDataByID(OrderLiv.depotID).FirstOrDefault();
            if(depot!=null){
            DepotLbl.Text = depot.CustomerName;
            DepotPanel.Visible = OrderLiv.IsPayedInDepot;
            }
            #endregion
            
            deliveryOrderLineAdapter = new gama_transportationdbDataSetTableAdapters.packages_order_line_delivererTableAdapter();
            CalculateTotal();
            labelX1.BackColor = Color.OrangeRed;
            labelX8.BackColor = Color.ForestGreen;
        }

        private void btnAddLine_Click(object sender, EventArgs e)
        {
            if (productsBindingSource.Position < 0) return;
            decimal Price = Convert.ToDecimal(((DataRowView)productsBindingSource.Current)["Price"]);
            string productName = ((DataRowView)productsBindingSource.Current)["ProductName"].ToString();
            var qteQ = new AddEditDeliveryOrderLine() { Price = Convert.ToDouble(Price)};
            if (qteQ.ShowDialog(this) == DialogResult.Cancel) return;

            packages_order_line_delivererTableAdapter.Insert(Convert.ToDecimal(qteQ.maxQte), 0, OrderID, IdYear,Convert.ToDecimal( qteQ.Price), 0, productName);
            
            this.packages_order_line_delivererTableAdapter.FillByOrder(this.gama_transportationdbDataSet.packages_order_line_deliverer, OrderID, IdYear);
            CalculateTotal();
        }

        private void btnDeleteLine_Click(object sender, EventArgs e)
        {
            if (packages_order_line_delivererBindingSource.Position < 0) return;
            if (MessageBox.Show("����� ��� ����� �����Ͽ", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            int idLine = Convert.ToInt32(((DataRowView)packages_order_line_delivererBindingSource.Current)["ID"]);
            deliveryOrderLineAdapter.DeleteQuery(idLine);
             
            this.packages_order_line_delivererTableAdapter.FillByOrder(this.gama_transportationdbDataSet.packages_order_line_deliverer, OrderID, IdYear);
            CalculateTotal();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (packages_order_line_delivererBindingSource.Count == 0)
            {
                try
                {
                    package_delivrer_orderTableAdapter.DeleteQuery(OrderID, IdYear);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }

            try
            {
                
                this.Validate();
                this.package_delivrer_orderBindingSource.EndEdit();
                var or = gama_transportationdbDataSet.package_delivrer_order.FirstOrDefault();
                package_delivrer_orderTableAdapter.UpdateQuery(
                    or.deliverers_ID,
                    or.dat,
                    or.Note,
                    or.Delivered,
                    or.Total,
                    Convert.ToDecimal(DiscountBox.Value),
                    or.ID,
                    or.SerialYear);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnCloseOperation_Click(object sender, EventArgs e)
        {
            if (deliveredSwitchButton.Value)
            {
                this.package_delivrer_orderTableAdapter.UpdateDelivered(!deliveredSwitchButton.Value, OrderID, IdYear);
                new gama_transportationdbDataSetTableAdapters.packages_order_line_delivererTableAdapter().UpdateDelivered(!deliveredSwitchButton.Value, OrderID, IdYear);
                var or = gama_transportationdbDataSet.package_delivrer_order.FirstOrDefault();
                package_delivrer_orderTableAdapter.UpdateQuery(
                 or.deliverers_ID,
                 or.dat,
                 or.Note,
                 or.Delivered,
                 or.Total,
                 Convert.ToDecimal(DiscountBox.Value),
                 or.ID,
                 or.SerialYear);
            }
            else
            {
                this.package_delivrer_orderTableAdapter.UpdateDelivered(!deliveredSwitchButton.Value, OrderID, IdYear);
                new gama_transportationdbDataSetTableAdapters.packages_order_line_delivererTableAdapter().UpdateDelivered(!deliveredSwitchButton.Value, OrderID, IdYear);
                var or = gama_transportationdbDataSet.package_delivrer_order.FirstOrDefault();
                package_delivrer_orderTableAdapter.UpdateQuery(
                 or.deliverers_ID,
                 or.dat,
                 or.Note,
                 or.Delivered,
                 or.Total,
                    Convert.ToDecimal(DiscountBox.Value),
                 or.ID,
                 or.SerialYear);
            }
            this.package_delivrer_orderTableAdapter.FillByID(gama_transportationdbDataSet.package_delivrer_order, OrderID, IdYear);            
        }

        private void CalculateTotal()
        {
            if (packages_order_line_delivererBindingSource.Position < 0)
            {
                TotalBox.Value = 0;
                return;
            }
            OrderLiv = gama_transportationdbDataSet.package_delivrer_order.FirstOrDefault();
            if (OrderLiv == null) return;
            TotalPackAmountDepot.Value = !OrderLiv.IsPayedInDepot ? 0 : Convert.ToDouble(gama_transportationdbDataSet.packages_order_line_deliverer.Sum(x => x.Quantity * x.Price));
            doubleInput1.Value = Convert.ToDouble(gama_transportationdbDataSet.packages_order_line_deliverer.Sum(x => x.Quantity * x.Price)) - TotalPackAmountDepot.Value;
            TotalBox.Value = Convert.ToDouble(gama_transportationdbDataSet.packages_order_line_deliverer.Sum(x => x.Quantity * x.Price)) + Convert.ToDouble(gama_transportationdbDataSet.PackageOrderExpensesView.Sum(x => x.Amount)) - TotalPackAmountDepot.Value - DiscountBox.Value;
            PaymentBox.Value = Convert.ToDouble(new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter().SumOfPaymentsByDeliveryOrder(OrderID,IdYear).GetValueOrDefault(0));
            ResteBox.Value = TotalBox.Value - PaymentBox.Value ;
        }

        private void btnAddPayment_Click(object sender, EventArgs e)
        {
            if (TotalBox.Value>0)
            {

                new Payments.AddEditPayment(0, PartnerID)
                    { StartUpAmount = TotalBox.Value,DelivererOrderID = OrderID, DelivererOrderYear = IdYear }.ShowDialog();
                CalculateTotal();
            }
        }

        private void btnPrintDirect_Click(object sender, EventArgs e)
        {
            try
            {
                List<ReportDataSource> dataSources = new List<ReportDataSource>();
                dataSources.Add(new ReportDataSource("DataSet1", (object)new ViewDataSetTableAdapters.DeliveryOrderListTableAdapter().GetDataByID(OrderID, IdYear)));
                dataSources.Add(new ReportDataSource("DataSet2", (object)new ViewDataSetTableAdapters.DeliveryOrderLinesTableAdapter().GetDataByID(OrderID, IdYear)));
                dataSources.Add(new ReportDataSource("DataSet3", (object)new gama_transportationdbDataSetTableAdapters.PackageOrderExpensesViewTableAdapter().GetDataByOrder(OrderID, IdYear)));
                var totalpayed = Convert.ToDecimal(PaymentBox.Value);
                var totalreste = (gama_transportationdbDataSet.packages_order_line_deliverer.Sum(x =>  x.Quantity * x.Price ));
                
                ReportParameter rp1, rp2, rp3, rp4,rp5,rp6;
                SSUtilities.SSConfigFile cfg = new SSUtilities.SSConfigFile();
                rp1 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName, ""));
                rp2 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity, ""));
                rp3 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1, ""));
                rp4 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, ""));
                rp5 = new ReportParameter("totalpayed", totalpayed.ToString());
                rp6 = new ReportParameter("totalreste", totalreste.ToString());
                var printer = cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.MTM.DefaultPrinter, "");
                new Report.Direct().DirectPrinting(printer, "MTM.Deliverer.Reports.DeliveryOrderReport.rdlc", false, dataSources, null, "None", new ReportParameter[] { rp1, rp2, rp3, rp4 ,rp5,rp6});
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddEditDeliveryOrder_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F3) ProductSearchBox.Focus();
        }

        private void btnPrintO_Click(object sender, EventArgs e)
        {
            var tp1 = (gama_transportationdbDataSet.packages_order_line_deliverer.Sum(x => x.Quantity * x.Price )) ;
            new Deliverer.Reports.DeliveryOrderReportForm() { OrderID = OrderID, 
                YearID = IdYear, 
                totalreste = tp1, 
                totalpayed =Convert.ToDecimal( PaymentBox.Value )}.ShowDialog(this);
        }

        private void btnAddExpences_Click(object sender, EventArgs e)
        {
             var selectPartner = new Partners.SelectPartner();
            selectPartner.IsFactory = false;
            selectPartner.IsMerchant = false;
            selectPartner.IsWarehouse = true;
            if (selectPartner.ShowDialog(this) == DialogResult.Cancel) return;
                
            var expenseView = new Expenses.AddEditPackageOrderExpense();
            expenseView.ExpenseID = 0;
            expenseView.PackageOrderID = OrderID;
            expenseView.PackageOrderYearID = IdYear;
            expenseView.DepotID = selectPartner.SelectedPartnerID;

            if (expenseView.ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
            this.packageOrderExpensesViewTableAdapter.FillByOrder(this.gama_transportationdbDataSet.PackageOrderExpensesView, OrderID,IdYear);
            CalculateTotal();
        }

        private void btnEditExpenses_Click(object sender, EventArgs e)
        {
            if (packageOrderExpensesViewBindingSource.Position < 0) return;
            var expenseView = new Expenses.AddEditPackageOrderExpense();
            expenseView.ExpenseID = Convert.ToInt32(((DataRowView)packageOrderExpensesViewBindingSource.Current)["ID"]);
            expenseView.PackageOrderID = OrderID;
            expenseView.PackageOrderYearID = IdYear;
            if (expenseView.ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
            this.packageOrderExpensesViewTableAdapter.FillByOrder(this.gama_transportationdbDataSet.PackageOrderExpensesView, OrderID, IdYear);
            CalculateTotal();
        }

        private void btnDeleteExpenses_Click(object sender, EventArgs e)
        {
            if (packageOrderExpensesViewBindingSource.Position < 0) return;
            if (MessageBox.Show("����� ����� ��������", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
       
            new gama_transportationdbDataSetTableAdapters.package_order_paymentsTableAdapter().DeleteQuery(
                Convert.ToInt32(((DataRowView)packageOrderExpensesViewBindingSource.Current)["order_payments_ID"]));
            
            new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter().DeleteQuery(
                Convert.ToInt32(((DataRowView)packageOrderExpensesViewBindingSource.Current)["partners_payments_ID"]));
          
            new gama_transportationdbDataSetTableAdapters.package_order_expensesTableAdapter().DeleteQuery(
                Convert.ToInt32(((DataRowView)packageOrderExpensesViewBindingSource.Current)["ID"]));
            this.packageOrderExpensesViewTableAdapter.FillByOrder(this.gama_transportationdbDataSet.PackageOrderExpensesView, OrderID, IdYear);            
            CalculateTotal();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.packages_order_line_delivererBindingSource.EndEdit();
                this.package_delivrer_orderBindingSource.EndEdit();
                this.tableAdapterManager1.UpdateAll(gama_transportationdbDataSet);
              
                this.packages_order_line_delivererTableAdapter.FillByOrder(this.gama_transportationdbDataSet.packages_order_line_deliverer, OrderID, IdYear);
                this.package_delivrer_orderTableAdapter.FillByID(gama_transportationdbDataSet.package_delivrer_order, OrderID, IdYear);
              
                CalculateTotal();
                var or = gama_transportationdbDataSet.package_delivrer_order.FirstOrDefault();
                package_delivrer_orderTableAdapter.UpdateQuery(
                 or.deliverers_ID,
                 or.dat,
                 or.Note,
                 or.Delivered,
                 or.Total,
                    Convert.ToDecimal(DiscountBox.Value),
                 or.ID,
                 or.SerialYear);
               
            }
            catch (Exception)
            {
            }
        }

        private void ProductSearchBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                productsBindingSource.Filter = "ProductName LIKE '%"+ProductSearchBox.Text+"%'";
            }
            catch (Exception)
            {
            }
        }

        private void packages_order_line_delivererBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            labelX14.Text= packages_order_line_delivererBindingSource.Count.ToString();
            labelX13.Text = gama_transportationdbDataSet.packages_order_line_deliverer.Sum(x => x.Quantity ).ToString();
        }

        private void btnDepotPayed_Click(object sender, EventArgs e)
        {
            var order = gama_transportationdbDataSet.package_delivrer_order.FirstOrDefault();

            if (!order.IsPayedInDepot)
            {
                var selectPartner = new Partners.SelectPartner();
                selectPartner.IsFactory = false;
                selectPartner.IsMerchant = false;
                selectPartner.IsWarehouse = true;
                if (selectPartner.ShowDialog(this) == DialogResult.Cancel) return;
                DepotLbl.Text = selectPartner.NameDepot;
                TotalPackAmountDepot.Value = Convert.ToDouble(gama_transportationdbDataSet.packages_order_line_deliverer.Sum(x => x.Quantity * x.Price));
                DepotPanel.Visible = true;
                package_delivrer_orderTableAdapter.UpdateOnDepot(selectPartner.SelectedPartnerID, true, gama_transportationdbDataSet.packages_order_line_deliverer.Sum(x => x.Quantity * x.Price), order.ID, order.SerialYear);
            }
            else
            {
                if (MessageBox.Show("���� ����� ���� ������", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
                DepotPanel.Visible = false;
                package_delivrer_orderTableAdapter.UpdateOnDepot(0, false, 0.00M, order.ID, order.SerialYear);
            }
            this.package_delivrer_orderTableAdapter.FillByID(gama_transportationdbDataSet.package_delivrer_order, OrderID, IdYear);

            btnSave.PerformClick();
            btnSave.PerformClick();
        }

        private void DiscountBox_ValueChanged(object sender, EventArgs e)
        {
            if(OrderLiv!=null)
            CalculateTotal();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            var selectPartner = new Partners.SelectPartner();
            selectPartner.IsFactory = true;
            selectPartner.IsMerchant = true;
            selectPartner.IsWarehouse = false;
            if (selectPartner.ShowDialog(this) == DialogResult.Cancel) return;

            // Update Payment's partners_ID
            new gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter().UpdatePartnerID(selectPartner.SelectedPartnerID, OrderID, IdYear);
            new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter().UpdatePartnerID(selectPartner.SelectedPartnerID, PartnerID, OrderID, IdYear);

            PartnerID = selectPartner.SelectedPartnerID;

            this.partnersTableAdapter.FillByID(this.gama_transportationdbDataSet.partners, selectPartner.SelectedPartnerID);
        }
    }
}