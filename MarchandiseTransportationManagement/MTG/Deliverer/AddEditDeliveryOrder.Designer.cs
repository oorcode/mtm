﻿namespace MTM.Deliverer
{
    partial class AddEditDeliveryOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.collapsibleSplitContainer1 = new DevComponents.DotNetBar.Controls.CollapsibleSplitContainer();
            this.DeliveryDataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.DesignationColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deliveredDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.packagedelivrerorderIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packagedelivrerorderSerialYearDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packages_order_line_delivererBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gama_transportationdbDataSet = new MTM.gama_transportationdbDataSet();
            this.DepotPanel = new System.Windows.Forms.TableLayoutPanel();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.TotalPackAmountDepot = new DevComponents.Editors.DoubleInput();
            this.package_delivrer_orderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DepotLbl = new DevComponents.DotNetBar.LabelX();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDeleteLine = new DevComponents.DotNetBar.ButtonX();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.deliveredSwitchButton = new DevComponents.DotNetBar.Controls.SwitchButton();
            this.comboBoxEx1 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.deliverersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.noteTextBoxX = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.datDateTimeInput = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.ExpensesCommandesTable = new System.Windows.Forms.TableLayoutPanel();
            this.btnDeleteExpenses = new DevComponents.DotNetBar.ButtonX();
            this.btnEditExpenses = new DevComponents.DotNetBar.ButtonX();
            this.btnAddExpences = new DevComponents.DotNetBar.ButtonX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.ExpensesDataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.designationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partnerspaymentsIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderpaymentsIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packageOrderExpensesViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.TotalBox = new DevComponents.Editors.DoubleInput();
            this.ResteBox = new DevComponents.Editors.DoubleInput();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.doubleInput1 = new DevComponents.Editors.DoubleInput();
            this.labelX21 = new DevComponents.DotNetBar.LabelX();
            this.labelX17 = new DevComponents.DotNetBar.LabelX();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.PaymentBox = new DevComponents.Editors.DoubleInput();
            this.DiscountBox = new DevComponents.Editors.DoubleInput();
            this.mainDataGridViewX = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.productNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partnersIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.panelEx3 = new DevComponents.DotNetBar.PanelEx();
            this.btnAddLine = new DevComponents.DotNetBar.ButtonX();
            this.ProductSearchBox = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnExit = new DevComponents.DotNetBar.ButtonX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.partnersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnCloseOperation = new DevComponents.DotNetBar.ButtonX();
            this.btnAddPayment = new DevComponents.DotNetBar.ButtonX();
            this.btnPrint = new DevComponents.DotNetBar.ButtonX();
            this.btnPrintO = new DevComponents.DotNetBar.ButtonItem();
            this.btnPrintDirect = new DevComponents.DotNetBar.ButtonItem();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.btnDepotPayed = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.tableAdapterManager = new MTM.ViewDataSetTableAdapters.TableAdapterManager();
            this.package_delivrer_orderTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter();
            this.tableAdapterManager1 = new MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager();
            this.packages_order_line_delivererTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.packages_order_line_delivererTableAdapter();
            this.deliverersTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.deliverersTableAdapter();
            this.productsTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.productsTableAdapter();
            this.packageOrderExpensesViewTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.PackageOrderExpensesViewTableAdapter();
            this.partnersTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.partnersTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.collapsibleSplitContainer1)).BeginInit();
            this.collapsibleSplitContainer1.Panel1.SuspendLayout();
            this.collapsibleSplitContainer1.Panel2.SuspendLayout();
            this.collapsibleSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DeliveryDataGridViewX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packages_order_line_delivererBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).BeginInit();
            this.DepotPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalPackAmountDepot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.package_delivrer_orderBindingSource)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.panelEx2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deliverersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datDateTimeInput)).BeginInit();
            this.ExpensesCommandesTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExpensesDataGridViewX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packageOrderExpensesViewBindingSource)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TotalBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResteBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.doubleInput1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGridViewX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).BeginInit();
            this.panelEx1.SuspendLayout();
            this.panelEx3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.partnersBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // collapsibleSplitContainer1
            // 
            this.collapsibleSplitContainer1.BackColor = System.Drawing.Color.White;
            this.collapsibleSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.collapsibleSplitContainer1.ForeColor = System.Drawing.Color.Black;
            this.collapsibleSplitContainer1.Location = new System.Drawing.Point(0, 39);
            this.collapsibleSplitContainer1.Name = "collapsibleSplitContainer1";
            // 
            // collapsibleSplitContainer1.Panel1
            // 
            this.collapsibleSplitContainer1.Panel1.AutoScroll = true;
            this.collapsibleSplitContainer1.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.DeliveryDataGridViewX1);
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.DepotPanel);
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.tableLayoutPanel4);
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.tableLayoutPanel6);
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.panelEx2);
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.labelX8);
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.ExpensesCommandesTable);
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.ExpensesDataGridViewX1);
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.tableLayoutPanel2);
            this.collapsibleSplitContainer1.Panel1.ForeColor = System.Drawing.Color.Black;
            this.collapsibleSplitContainer1.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            // 
            // collapsibleSplitContainer1.Panel2
            // 
            this.collapsibleSplitContainer1.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.collapsibleSplitContainer1.Panel2.Controls.Add(this.mainDataGridViewX);
            this.collapsibleSplitContainer1.Panel2.Controls.Add(this.panelEx1);
            this.collapsibleSplitContainer1.Panel2.ForeColor = System.Drawing.Color.Black;
            this.collapsibleSplitContainer1.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.collapsibleSplitContainer1.Size = new System.Drawing.Size(1020, 603);
            this.collapsibleSplitContainer1.SplitterDistance = 649;
            this.collapsibleSplitContainer1.SplitterWidth = 18;
            this.collapsibleSplitContainer1.TabIndex = 0;
            // 
            // DeliveryDataGridViewX1
            // 
            this.DeliveryDataGridViewX1.AllowUserToAddRows = false;
            this.DeliveryDataGridViewX1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.DeliveryDataGridViewX1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DeliveryDataGridViewX1.AutoGenerateColumns = false;
            this.DeliveryDataGridViewX1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DeliveryDataGridViewX1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DeliveryDataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DeliveryDataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DesignationColumn,
            this.quantityDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.deliveredDataGridViewCheckBoxColumn,
            this.packagedelivrerorderIDDataGridViewTextBoxColumn,
            this.packagedelivrerorderSerialYearDataGridViewTextBoxColumn,
            this.iDDataGridViewTextBoxColumn});
            this.DeliveryDataGridViewX1.DataSource = this.packages_order_line_delivererBindingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DeliveryDataGridViewX1.DefaultCellStyle = dataGridViewCellStyle5;
            this.DeliveryDataGridViewX1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DeliveryDataGridViewX1.EnableHeadersVisualStyles = false;
            this.DeliveryDataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.DeliveryDataGridViewX1.Location = new System.Drawing.Point(0, 173);
            this.DeliveryDataGridViewX1.Name = "DeliveryDataGridViewX1";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DeliveryDataGridViewX1.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DeliveryDataGridViewX1.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.DeliveryDataGridViewX1.RowTemplate.Height = 32;
            this.DeliveryDataGridViewX1.Size = new System.Drawing.Size(649, 95);
            this.DeliveryDataGridViewX1.TabIndex = 4;
            // 
            // DesignationColumn
            // 
            this.DesignationColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DesignationColumn.DataPropertyName = "Designation";
            this.DesignationColumn.HeaderText = "التعيين";
            this.DesignationColumn.Name = "DesignationColumn";
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "Quantity";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Format = "N2";
            this.quantityDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.quantityDataGridViewTextBoxColumn.HeaderText = "الكمية";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            this.priceDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.priceDataGridViewTextBoxColumn.HeaderText = "سعر الوحدة";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.Width = 110;
            // 
            // deliveredDataGridViewCheckBoxColumn
            // 
            this.deliveredDataGridViewCheckBoxColumn.DataPropertyName = "Delivered";
            this.deliveredDataGridViewCheckBoxColumn.HeaderText = "مسلمة";
            this.deliveredDataGridViewCheckBoxColumn.Name = "deliveredDataGridViewCheckBoxColumn";
            this.deliveredDataGridViewCheckBoxColumn.Visible = false;
            // 
            // packagedelivrerorderIDDataGridViewTextBoxColumn
            // 
            this.packagedelivrerorderIDDataGridViewTextBoxColumn.DataPropertyName = "package_delivrer_order_ID";
            this.packagedelivrerorderIDDataGridViewTextBoxColumn.HeaderText = "package_delivrer_order_ID";
            this.packagedelivrerorderIDDataGridViewTextBoxColumn.Name = "packagedelivrerorderIDDataGridViewTextBoxColumn";
            this.packagedelivrerorderIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // packagedelivrerorderSerialYearDataGridViewTextBoxColumn
            // 
            this.packagedelivrerorderSerialYearDataGridViewTextBoxColumn.DataPropertyName = "package_delivrer_order_SerialYear";
            this.packagedelivrerorderSerialYearDataGridViewTextBoxColumn.HeaderText = "package_delivrer_order_SerialYear";
            this.packagedelivrerorderSerialYearDataGridViewTextBoxColumn.Name = "packagedelivrerorderSerialYearDataGridViewTextBoxColumn";
            this.packagedelivrerorderSerialYearDataGridViewTextBoxColumn.Visible = false;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // packages_order_line_delivererBindingSource
            // 
            this.packages_order_line_delivererBindingSource.DataMember = "packages_order_line_deliverer";
            this.packages_order_line_delivererBindingSource.DataSource = this.gama_transportationdbDataSet;
            this.packages_order_line_delivererBindingSource.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.packages_order_line_delivererBindingSource_ListChanged);
            // 
            // gama_transportationdbDataSet
            // 
            this.gama_transportationdbDataSet.DataSetName = "gama_transportationdbDataSet";
            this.gama_transportationdbDataSet.EnforceConstraints = false;
            this.gama_transportationdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DepotPanel
            // 
            this.DepotPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.DepotPanel.ColumnCount = 3;
            this.DepotPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.DepotPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.DepotPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.DepotPanel.Controls.Add(this.labelX19, 0, 0);
            this.DepotPanel.Controls.Add(this.TotalPackAmountDepot, 2, 0);
            this.DepotPanel.Controls.Add(this.DepotLbl, 1, 0);
            this.DepotPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DepotPanel.ForeColor = System.Drawing.Color.Black;
            this.DepotPanel.Location = new System.Drawing.Point(0, 268);
            this.DepotPanel.Name = "DepotPanel";
            this.DepotPanel.RowCount = 1;
            this.DepotPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.DepotPanel.Size = new System.Drawing.Size(649, 38);
            this.DepotPanel.TabIndex = 85;
            this.DepotPanel.Visible = false;
            // 
            // labelX19
            // 
            this.labelX19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX19.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(240, 3);
            this.labelX19.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(409, 32);
            this.labelX19.TabIndex = 5;
            this.labelX19.Text = "هذه السلع تم تخليصها عند المرسل و هو المخزن ";
            this.labelX19.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // TotalPackAmountDepot
            // 
            this.TotalPackAmountDepot.AllowEmptyState = false;
            this.TotalPackAmountDepot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TotalPackAmountDepot.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TotalPackAmountDepot.BackgroundStyle.Class = "DateTimeInputBackground";
            this.TotalPackAmountDepot.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TotalPackAmountDepot.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.TotalPackAmountDepot.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.package_delivrer_orderBindingSource, "OnDepotAmount", true));
            this.TotalPackAmountDepot.FocusHighlightEnabled = true;
            this.TotalPackAmountDepot.ForeColor = System.Drawing.Color.Black;
            this.TotalPackAmountDepot.Increment = 1D;
            this.TotalPackAmountDepot.IsInputReadOnly = true;
            this.TotalPackAmountDepot.Location = new System.Drawing.Point(3, 5);
            this.TotalPackAmountDepot.MaxValue = 999999999D;
            this.TotalPackAmountDepot.MinValue = 0D;
            this.TotalPackAmountDepot.Name = "TotalPackAmountDepot";
            this.TotalPackAmountDepot.Size = new System.Drawing.Size(114, 27);
            this.TotalPackAmountDepot.TabIndex = 1;
            // 
            // package_delivrer_orderBindingSource
            // 
            this.package_delivrer_orderBindingSource.DataMember = "package_delivrer_order";
            this.package_delivrer_orderBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // DepotLbl
            // 
            this.DepotLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.DepotLbl.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.DepotLbl.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DepotLbl.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DepotLbl.ForeColor = System.Drawing.Color.Black;
            this.DepotLbl.Location = new System.Drawing.Point(123, 3);
            this.DepotLbl.Name = "DepotLbl";
            this.DepotLbl.Size = new System.Drawing.Size(114, 32);
            this.DepotLbl.TabIndex = 5;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.tableLayoutPanel4.ColumnCount = 7;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 168F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 117F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 101F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel4.Controls.Add(this.labelX13, 6, 0);
            this.tableLayoutPanel4.Controls.Add(this.labelX12, 5, 0);
            this.tableLayoutPanel4.Controls.Add(this.labelX14, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.labelX15, 3, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel4.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 306);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(649, 38);
            this.tableLayoutPanel4.TabIndex = 81;
            // 
            // labelX13
            // 
            this.labelX13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX13.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(0, 3);
            this.labelX13.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(50, 32);
            this.labelX13.TabIndex = 5;
            this.labelX13.Text = "0";
            this.labelX13.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX12
            // 
            this.labelX12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX12.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(50, 3);
            this.labelX12.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(115, 32);
            this.labelX12.TabIndex = 5;
            this.labelX12.Text = "عدد الطرود:";
            // 
            // labelX14
            // 
            this.labelX14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX14.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(165, 3);
            this.labelX14.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(50, 32);
            this.labelX14.TabIndex = 5;
            this.labelX14.Text = "0";
            this.labelX14.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX15
            // 
            this.labelX15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX15.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX15.ForeColor = System.Drawing.Color.Black;
            this.labelX15.Location = new System.Drawing.Point(215, 3);
            this.labelX15.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(101, 32);
            this.labelX15.TabIndex = 5;
            this.labelX15.Text = "عدد الأسطر:";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Controls.Add(this.btnDeleteLine, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel6.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 138);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(649, 35);
            this.tableLayoutPanel6.TabIndex = 74;
            // 
            // btnDeleteLine
            // 
            this.btnDeleteLine.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDeleteLine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteLine.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDeleteLine.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteLine.Location = new System.Drawing.Point(3, 3);
            this.btnDeleteLine.Name = "btnDeleteLine";
            this.btnDeleteLine.Size = new System.Drawing.Size(109, 29);
            this.btnDeleteLine.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnDeleteLine.Symbol = "";
            this.btnDeleteLine.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDeleteLine.TabIndex = 1;
            this.btnDeleteLine.Text = "إلغاء سلعة";
            this.btnDeleteLine.Click += new System.EventHandler(this.btnDeleteLine_Click);
            // 
            // panelEx2
            // 
            this.panelEx2.AutoScroll = true;
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx2.Controls.Add(this.deliveredSwitchButton);
            this.panelEx2.Controls.Add(this.comboBoxEx1);
            this.panelEx2.Controls.Add(this.labelX9);
            this.panelEx2.Controls.Add(this.labelX2);
            this.panelEx2.Controls.Add(this.labelX6);
            this.panelEx2.Controls.Add(this.noteTextBoxX);
            this.panelEx2.Controls.Add(this.labelX7);
            this.panelEx2.Controls.Add(this.datDateTimeInput);
            this.panelEx2.Controls.Add(this.labelX3);
            this.panelEx2.Controls.Add(this.labelX5);
            this.panelEx2.Controls.Add(this.labelX4);
            this.panelEx2.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEx2.Location = new System.Drawing.Point(0, 33);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(649, 105);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 10;
            // 
            // deliveredSwitchButton
            // 
            // 
            // 
            // 
            this.deliveredSwitchButton.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.deliveredSwitchButton.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.package_delivrer_orderBindingSource, "Delivered", true));
            this.deliveredSwitchButton.ForeColor = System.Drawing.Color.Black;
            this.deliveredSwitchButton.Location = new System.Drawing.Point(3, 43);
            this.deliveredSwitchButton.Name = "deliveredSwitchButton";
            this.deliveredSwitchButton.OffText = "غير تامة";
            this.deliveredSwitchButton.OnText = "تمت";
            this.deliveredSwitchButton.Size = new System.Drawing.Size(197, 25);
            this.deliveredSwitchButton.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.deliveredSwitchButton.TabIndex = 11;
            // 
            // comboBoxEx1
            // 
            this.comboBoxEx1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.package_delivrer_orderBindingSource, "deliverers_ID", true));
            this.comboBoxEx1.DataSource = this.deliverersBindingSource;
            this.comboBoxEx1.DisplayMember = "DelivererName";
            this.comboBoxEx1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxEx1.FocusHighlightEnabled = true;
            this.comboBoxEx1.ForeColor = System.Drawing.Color.Black;
            this.comboBoxEx1.FormattingEnabled = true;
            this.comboBoxEx1.ItemHeight = 22;
            this.comboBoxEx1.Location = new System.Drawing.Point(3, 9);
            this.comboBoxEx1.Name = "comboBoxEx1";
            this.comboBoxEx1.Size = new System.Drawing.Size(197, 28);
            this.comboBoxEx1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.comboBoxEx1.TabIndex = 9;
            this.comboBoxEx1.ValueMember = "ID";
            // 
            // deliverersBindingSource
            // 
            this.deliverersBindingSource.DataMember = "deliverers";
            this.deliverersBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // labelX9
            // 
            this.labelX9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(206, 9);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(56, 28);
            this.labelX9.TabIndex = 5;
            this.labelX9.Text = "الحامل:";
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(555, 9);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(91, 23);
            this.labelX2.TabIndex = 5;
            this.labelX2.Text = "رقم الوصل:";
            // 
            // labelX6
            // 
            this.labelX6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(555, 41);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(91, 23);
            this.labelX6.TabIndex = 5;
            this.labelX6.Text = "تاريخ الوصل:";
            // 
            // noteTextBoxX
            // 
            this.noteTextBoxX.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.noteTextBoxX.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.noteTextBoxX.Border.Class = "TextBoxBorder";
            this.noteTextBoxX.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.noteTextBoxX.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.package_delivrer_orderBindingSource, "Note", true));
            this.noteTextBoxX.DisabledBackColor = System.Drawing.Color.White;
            this.noteTextBoxX.ForeColor = System.Drawing.Color.Black;
            this.noteTextBoxX.Location = new System.Drawing.Point(3, 74);
            this.noteTextBoxX.Name = "noteTextBoxX";
            this.noteTextBoxX.PreventEnterBeep = true;
            this.noteTextBoxX.Size = new System.Drawing.Size(546, 27);
            this.noteTextBoxX.TabIndex = 8;
            // 
            // labelX7
            // 
            this.labelX7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(555, 74);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(91, 23);
            this.labelX7.TabIndex = 5;
            this.labelX7.Text = "ملاحظة:";
            // 
            // datDateTimeInput
            // 
            this.datDateTimeInput.AllowEmptyState = false;
            this.datDateTimeInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.datDateTimeInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.datDateTimeInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.datDateTimeInput.ButtonDropDown.Visible = true;
            this.datDateTimeInput.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.package_delivrer_orderBindingSource, "dat", true));
            this.datDateTimeInput.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.package_delivrer_orderBindingSource, "dat", true));
            this.datDateTimeInput.FocusHighlightEnabled = true;
            this.datDateTimeInput.ForeColor = System.Drawing.Color.Black;
            this.datDateTimeInput.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center;
            this.datDateTimeInput.IsPopupCalendarOpen = false;
            this.datDateTimeInput.Location = new System.Drawing.Point(419, 41);
            // 
            // 
            // 
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.datDateTimeInput.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.DisplayMonth = new System.DateTime(2018, 12, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.TodayButtonVisible = true;
            this.datDateTimeInput.Name = "datDateTimeInput";
            this.datDateTimeInput.Size = new System.Drawing.Size(130, 27);
            this.datDateTimeInput.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.datDateTimeInput.TabIndex = 6;
            this.datDateTimeInput.Value = new System.DateTime(2018, 12, 17, 22, 47, 37, 329);
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.package_delivrer_orderBindingSource, "ID", true));
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(487, 9);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(62, 23);
            this.labelX3.TabIndex = 5;
            this.labelX3.Text = "---------";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX5
            // 
            this.labelX5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.package_delivrer_orderBindingSource, "SerialYear", true));
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(419, 9);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(55, 23);
            this.labelX5.TabIndex = 5;
            this.labelX5.Text = "---------";
            // 
            // labelX4
            // 
            this.labelX4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(474, 8);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(13, 23);
            this.labelX4.TabIndex = 5;
            this.labelX4.Text = "/";
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(0, 0);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(649, 33);
            this.labelX8.TabIndex = 9;
            this.labelX8.Text = "محتويات الوصل";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // ExpensesCommandesTable
            // 
            this.ExpensesCommandesTable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.ExpensesCommandesTable.ColumnCount = 5;
            this.ExpensesCommandesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ExpensesCommandesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.ExpensesCommandesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.ExpensesCommandesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.ExpensesCommandesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.ExpensesCommandesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.ExpensesCommandesTable.Controls.Add(this.btnDeleteExpenses, 4, 0);
            this.ExpensesCommandesTable.Controls.Add(this.btnEditExpenses, 3, 0);
            this.ExpensesCommandesTable.Controls.Add(this.btnAddExpences, 2, 0);
            this.ExpensesCommandesTable.Controls.Add(this.labelX11, 0, 0);
            this.ExpensesCommandesTable.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ExpensesCommandesTable.ForeColor = System.Drawing.Color.Black;
            this.ExpensesCommandesTable.Location = new System.Drawing.Point(0, 344);
            this.ExpensesCommandesTable.Name = "ExpensesCommandesTable";
            this.ExpensesCommandesTable.RowCount = 1;
            this.ExpensesCommandesTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ExpensesCommandesTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.ExpensesCommandesTable.Size = new System.Drawing.Size(649, 38);
            this.ExpensesCommandesTable.TabIndex = 80;
            // 
            // btnDeleteExpenses
            // 
            this.btnDeleteExpenses.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDeleteExpenses.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteExpenses.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDeleteExpenses.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteExpenses.Location = new System.Drawing.Point(3, 3);
            this.btnDeleteExpenses.Name = "btnDeleteExpenses";
            this.btnDeleteExpenses.Size = new System.Drawing.Size(109, 32);
            this.btnDeleteExpenses.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnDeleteExpenses.Symbol = "";
            this.btnDeleteExpenses.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDeleteExpenses.TabIndex = 2;
            this.btnDeleteExpenses.Text = "إلغاء";
            this.btnDeleteExpenses.Click += new System.EventHandler(this.btnDeleteExpenses_Click);
            // 
            // btnEditExpenses
            // 
            this.btnEditExpenses.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEditExpenses.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditExpenses.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnEditExpenses.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditExpenses.Location = new System.Drawing.Point(118, 3);
            this.btnEditExpenses.Name = "btnEditExpenses";
            this.btnEditExpenses.Size = new System.Drawing.Size(109, 32);
            this.btnEditExpenses.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnEditExpenses.Symbol = "";
            this.btnEditExpenses.SymbolColor = System.Drawing.Color.OrangeRed;
            this.btnEditExpenses.TabIndex = 1;
            this.btnEditExpenses.Text = "تعديل";
            this.btnEditExpenses.Click += new System.EventHandler(this.btnEditExpenses_Click);
            // 
            // btnAddExpences
            // 
            this.btnAddExpences.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddExpences.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddExpences.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddExpences.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddExpences.Location = new System.Drawing.Point(233, 3);
            this.btnAddExpences.Name = "btnAddExpences";
            this.btnAddExpences.Size = new System.Drawing.Size(109, 32);
            this.btnAddExpences.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddExpences.Symbol = "";
            this.btnAddExpences.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnAddExpences.TabIndex = 0;
            this.btnAddExpences.Text = "مصاريف";
            this.btnAddExpences.Click += new System.EventHandler(this.btnAddExpences_Click);
            // 
            // labelX11
            // 
            this.labelX11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX11.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ExpensesCommandesTable.SetColumnSpan(this.labelX11, 2);
            this.labelX11.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(348, 3);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(298, 32);
            this.labelX11.TabIndex = 67;
            this.labelX11.Text = "مصاريف الوصل";
            // 
            // ExpensesDataGridViewX1
            // 
            this.ExpensesDataGridViewX1.AllowUserToAddRows = false;
            this.ExpensesDataGridViewX1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            this.ExpensesDataGridViewX1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.ExpensesDataGridViewX1.AutoGenerateColumns = false;
            this.ExpensesDataGridViewX1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ExpensesDataGridViewX1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.ExpensesDataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ExpensesDataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.designationDataGridViewTextBoxColumn,
            this.datDataGridViewTextBoxColumn,
            this.amountDataGridViewTextBoxColumn,
            this.iDDataGridViewTextBoxColumn2,
            this.partnerspaymentsIDDataGridViewTextBoxColumn,
            this.orderpaymentsIDDataGridViewTextBoxColumn});
            this.ExpensesDataGridViewX1.DataSource = this.packageOrderExpensesViewBindingSource;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ExpensesDataGridViewX1.DefaultCellStyle = dataGridViewCellStyle10;
            this.ExpensesDataGridViewX1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ExpensesDataGridViewX1.EnableHeadersVisualStyles = false;
            this.ExpensesDataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.ExpensesDataGridViewX1.Location = new System.Drawing.Point(0, 382);
            this.ExpensesDataGridViewX1.Name = "ExpensesDataGridViewX1";
            this.ExpensesDataGridViewX1.ReadOnly = true;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ExpensesDataGridViewX1.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ExpensesDataGridViewX1.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.ExpensesDataGridViewX1.RowTemplate.Height = 32;
            this.ExpensesDataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ExpensesDataGridViewX1.Size = new System.Drawing.Size(649, 122);
            this.ExpensesDataGridViewX1.TabIndex = 79;
            // 
            // designationDataGridViewTextBoxColumn
            // 
            this.designationDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.designationDataGridViewTextBoxColumn.DataPropertyName = "Designation";
            this.designationDataGridViewTextBoxColumn.HeaderText = "التعيين";
            this.designationDataGridViewTextBoxColumn.Name = "designationDataGridViewTextBoxColumn";
            this.designationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // datDataGridViewTextBoxColumn
            // 
            this.datDataGridViewTextBoxColumn.DataPropertyName = "dat";
            this.datDataGridViewTextBoxColumn.HeaderText = "التاريخ";
            this.datDataGridViewTextBoxColumn.Name = "datDataGridViewTextBoxColumn";
            this.datDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // amountDataGridViewTextBoxColumn
            // 
            this.amountDataGridViewTextBoxColumn.DataPropertyName = "Amount";
            this.amountDataGridViewTextBoxColumn.HeaderText = "المبلغ";
            this.amountDataGridViewTextBoxColumn.Name = "amountDataGridViewTextBoxColumn";
            this.amountDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iDDataGridViewTextBoxColumn2
            // 
            this.iDDataGridViewTextBoxColumn2.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn2.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn2.Name = "iDDataGridViewTextBoxColumn2";
            this.iDDataGridViewTextBoxColumn2.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn2.Visible = false;
            // 
            // partnerspaymentsIDDataGridViewTextBoxColumn
            // 
            this.partnerspaymentsIDDataGridViewTextBoxColumn.DataPropertyName = "partners_payments_ID";
            this.partnerspaymentsIDDataGridViewTextBoxColumn.HeaderText = "partners_payments_ID";
            this.partnerspaymentsIDDataGridViewTextBoxColumn.Name = "partnerspaymentsIDDataGridViewTextBoxColumn";
            this.partnerspaymentsIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.partnerspaymentsIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // orderpaymentsIDDataGridViewTextBoxColumn
            // 
            this.orderpaymentsIDDataGridViewTextBoxColumn.DataPropertyName = "order_payments_ID";
            this.orderpaymentsIDDataGridViewTextBoxColumn.HeaderText = "order_payments_ID";
            this.orderpaymentsIDDataGridViewTextBoxColumn.Name = "orderpaymentsIDDataGridViewTextBoxColumn";
            this.orderpaymentsIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.orderpaymentsIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // packageOrderExpensesViewBindingSource
            // 
            this.packageOrderExpensesViewBindingSource.DataMember = "PackageOrderExpensesView";
            this.packageOrderExpensesViewBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoScroll = true;
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 131F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 148F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 95F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 148F));
            this.tableLayoutPanel2.Controls.Add(this.labelX10, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.TotalBox, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.ResteBox, 4, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelX18, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.doubleInput1, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelX21, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelX17, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelX20, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.PaymentBox, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.DiscountBox, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel2.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 504);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(649, 99);
            this.tableLayoutPanel2.TabIndex = 78;
            // 
            // labelX10
            // 
            this.labelX10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX10.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(151, 35);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(89, 26);
            this.labelX10.TabIndex = 0;
            this.labelX10.Text = "تخ-مج:";
            this.labelX10.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // TotalBox
            // 
            this.TotalBox.AllowEmptyState = false;
            this.TotalBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TotalBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TotalBox.BackgroundStyle.Class = "DateTimeInputBackground";
            this.TotalBox.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TotalBox.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.TotalBox.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.package_delivrer_orderBindingSource, "Total", true));
            this.TotalBox.FocusHighlightEnabled = true;
            this.TotalBox.ForeColor = System.Drawing.Color.Black;
            this.TotalBox.Increment = 1D;
            this.TotalBox.IsInputReadOnly = true;
            this.TotalBox.Location = new System.Drawing.Point(3, 35);
            this.TotalBox.MaxValue = 999999999D;
            this.TotalBox.MinValue = 0D;
            this.TotalBox.Name = "TotalBox";
            this.TotalBox.Size = new System.Drawing.Size(142, 27);
            this.TotalBox.TabIndex = 1;
            // 
            // ResteBox
            // 
            this.ResteBox.AllowEmptyState = false;
            this.ResteBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ResteBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ResteBox.BackgroundStyle.Class = "DateTimeInputBackground";
            this.ResteBox.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ResteBox.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.ResteBox.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.package_delivrer_orderBindingSource, "ResteToPay", true));
            this.ResteBox.FocusHighlightEnabled = true;
            this.ResteBox.ForeColor = System.Drawing.Color.Black;
            this.ResteBox.Increment = 1D;
            this.ResteBox.IsInputReadOnly = true;
            this.ResteBox.Location = new System.Drawing.Point(3, 68);
            this.ResteBox.MaxValue = 999999999D;
            this.ResteBox.MinValue = 0D;
            this.ResteBox.Name = "ResteBox";
            this.ResteBox.Size = new System.Drawing.Size(142, 27);
            this.ResteBox.TabIndex = 1;
            // 
            // labelX18
            // 
            this.labelX18.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX18.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.ForeColor = System.Drawing.Color.Black;
            this.labelX18.Location = new System.Drawing.Point(151, 67);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(89, 29);
            this.labelX18.TabIndex = 0;
            this.labelX18.Text = "الحساب:";
            this.labelX18.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // doubleInput1
            // 
            this.doubleInput1.AllowEmptyState = false;
            this.doubleInput1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.doubleInput1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.doubleInput1.BackgroundStyle.Class = "DateTimeInputBackground";
            this.doubleInput1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.doubleInput1.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.doubleInput1.FocusHighlightEnabled = true;
            this.doubleInput1.ForeColor = System.Drawing.Color.Black;
            this.doubleInput1.Increment = 1D;
            this.doubleInput1.IsInputReadOnly = true;
            this.doubleInput1.Location = new System.Drawing.Point(3, 3);
            this.doubleInput1.MaxValue = 999999999D;
            this.doubleInput1.MinValue = 0D;
            this.doubleInput1.Name = "doubleInput1";
            this.doubleInput1.Size = new System.Drawing.Size(142, 27);
            this.doubleInput1.TabIndex = 1;
            // 
            // labelX21
            // 
            this.labelX21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX21.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX21.ForeColor = System.Drawing.Color.Black;
            this.labelX21.Location = new System.Drawing.Point(151, 3);
            this.labelX21.Name = "labelX21";
            this.labelX21.Size = new System.Drawing.Size(89, 26);
            this.labelX21.TabIndex = 0;
            this.labelX21.Text = "المجموع:";
            this.labelX21.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX17
            // 
            this.labelX17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX17.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX17.ForeColor = System.Drawing.Color.Black;
            this.labelX17.Location = new System.Drawing.Point(394, 35);
            this.labelX17.Name = "labelX17";
            this.labelX17.Size = new System.Drawing.Size(125, 26);
            this.labelX17.TabIndex = 0;
            this.labelX17.Text = "المبلغ المدفوع:";
            this.labelX17.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX20
            // 
            this.labelX20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX20.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.ForeColor = System.Drawing.Color.Black;
            this.labelX20.Location = new System.Drawing.Point(394, 3);
            this.labelX20.Name = "labelX20";
            this.labelX20.Size = new System.Drawing.Size(125, 26);
            this.labelX20.TabIndex = 0;
            this.labelX20.Text = "التخفيض:";
            this.labelX20.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // PaymentBox
            // 
            this.PaymentBox.AllowEmptyState = false;
            this.PaymentBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.PaymentBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.PaymentBox.BackgroundStyle.Class = "DateTimeInputBackground";
            this.PaymentBox.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.PaymentBox.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.PaymentBox.FocusHighlightEnabled = true;
            this.PaymentBox.ForeColor = System.Drawing.Color.Black;
            this.PaymentBox.Increment = 1D;
            this.PaymentBox.IsInputReadOnly = true;
            this.PaymentBox.Location = new System.Drawing.Point(246, 35);
            this.PaymentBox.MaxValue = 999999999D;
            this.PaymentBox.MinValue = 0D;
            this.PaymentBox.Name = "PaymentBox";
            this.PaymentBox.Size = new System.Drawing.Size(142, 27);
            this.PaymentBox.TabIndex = 1;
            // 
            // DiscountBox
            // 
            this.DiscountBox.AllowEmptyState = false;
            this.DiscountBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.DiscountBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.DiscountBox.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DiscountBox.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DiscountBox.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.DiscountBox.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.package_delivrer_orderBindingSource, "Discount", true));
            this.DiscountBox.FocusHighlightEnabled = true;
            this.DiscountBox.ForeColor = System.Drawing.Color.Black;
            this.DiscountBox.Increment = 1D;
            this.DiscountBox.Location = new System.Drawing.Point(246, 3);
            this.DiscountBox.MaxValue = 999999999D;
            this.DiscountBox.MinValue = 0D;
            this.DiscountBox.Name = "DiscountBox";
            this.DiscountBox.Size = new System.Drawing.Size(142, 27);
            this.DiscountBox.TabIndex = 1;
            this.DiscountBox.ValueChanged += new System.EventHandler(this.DiscountBox_ValueChanged);
            // 
            // mainDataGridViewX
            // 
            this.mainDataGridViewX.AllowUserToAddRows = false;
            this.mainDataGridViewX.AllowUserToDeleteRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            this.mainDataGridViewX.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.mainDataGridViewX.AutoGenerateColumns = false;
            this.mainDataGridViewX.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mainDataGridViewX.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.mainDataGridViewX.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mainDataGridViewX.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.productNameDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn1,
            this.iDDataGridViewTextBoxColumn1,
            this.partnersIDDataGridViewTextBoxColumn});
            this.mainDataGridViewX.DataSource = this.productsBindingSource;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.mainDataGridViewX.DefaultCellStyle = dataGridViewCellStyle16;
            this.mainDataGridViewX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDataGridViewX.EnableHeadersVisualStyles = false;
            this.mainDataGridViewX.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.mainDataGridViewX.Location = new System.Drawing.Point(0, 77);
            this.mainDataGridViewX.Name = "mainDataGridViewX";
            this.mainDataGridViewX.ReadOnly = true;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mainDataGridViewX.RowHeadersDefaultCellStyle = dataGridViewCellStyle17;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.mainDataGridViewX.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this.mainDataGridViewX.RowTemplate.Height = 32;
            this.mainDataGridViewX.Size = new System.Drawing.Size(353, 526);
            this.mainDataGridViewX.TabIndex = 3;
            // 
            // productNameDataGridViewTextBoxColumn
            // 
            this.productNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.productNameDataGridViewTextBoxColumn.DataPropertyName = "ProductName";
            this.productNameDataGridViewTextBoxColumn.HeaderText = "المنتج";
            this.productNameDataGridViewTextBoxColumn.Name = "productNameDataGridViewTextBoxColumn";
            this.productNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // priceDataGridViewTextBoxColumn1
            // 
            this.priceDataGridViewTextBoxColumn1.DataPropertyName = "Price";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.Format = "N2";
            this.priceDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle15;
            this.priceDataGridViewTextBoxColumn1.HeaderText = "سعر الوحدة";
            this.priceDataGridViewTextBoxColumn1.Name = "priceDataGridViewTextBoxColumn1";
            this.priceDataGridViewTextBoxColumn1.ReadOnly = true;
            this.priceDataGridViewTextBoxColumn1.Width = 110;
            // 
            // iDDataGridViewTextBoxColumn1
            // 
            this.iDDataGridViewTextBoxColumn1.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn1.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn1.Name = "iDDataGridViewTextBoxColumn1";
            this.iDDataGridViewTextBoxColumn1.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn1.Visible = false;
            // 
            // partnersIDDataGridViewTextBoxColumn
            // 
            this.partnersIDDataGridViewTextBoxColumn.DataPropertyName = "partners_ID";
            this.partnersIDDataGridViewTextBoxColumn.HeaderText = "partners_ID";
            this.partnersIDDataGridViewTextBoxColumn.Name = "partnersIDDataGridViewTextBoxColumn";
            this.partnersIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.partnersIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // productsBindingSource
            // 
            this.productsBindingSource.DataMember = "products";
            this.productsBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.panelEx3);
            this.panelEx1.Controls.Add(this.labelX1);
            this.panelEx1.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEx1.Location = new System.Drawing.Point(0, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(353, 77);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 4;
            // 
            // panelEx3
            // 
            this.panelEx3.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx3.Controls.Add(this.btnAddLine);
            this.panelEx3.Controls.Add(this.ProductSearchBox);
            this.panelEx3.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEx3.Location = new System.Drawing.Point(0, 33);
            this.panelEx3.Name = "panelEx3";
            this.panelEx3.Size = new System.Drawing.Size(353, 39);
            this.panelEx3.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx3.Style.GradientAngle = 90;
            this.panelEx3.TabIndex = 77;
            this.panelEx3.Text = " ";
            // 
            // btnAddLine
            // 
            this.btnAddLine.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddLine.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddLine.Location = new System.Drawing.Point(3, 5);
            this.btnAddLine.Name = "btnAddLine";
            this.btnAddLine.Size = new System.Drawing.Size(120, 27);
            this.btnAddLine.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddLine.Symbol = "";
            this.btnAddLine.TabIndex = 1;
            this.btnAddLine.Text = "إضافة للوصل";
            this.btnAddLine.Click += new System.EventHandler(this.btnAddLine_Click);
            // 
            // ProductSearchBox
            // 
            this.ProductSearchBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ProductSearchBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ProductSearchBox.Border.Class = "TextBoxBorder";
            this.ProductSearchBox.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ProductSearchBox.DisabledBackColor = System.Drawing.Color.White;
            this.ProductSearchBox.ForeColor = System.Drawing.Color.Black;
            this.ProductSearchBox.Location = new System.Drawing.Point(128, 5);
            this.ProductSearchBox.Name = "ProductSearchBox";
            this.ProductSearchBox.PreventEnterBeep = true;
            this.ProductSearchBox.Size = new System.Drawing.Size(219, 27);
            this.ProductSearchBox.TabIndex = 0;
            this.ProductSearchBox.WatermarkText = "بحــــــــــث";
            this.ProductSearchBox.TextChanged += new System.EventHandler(this.ProductSearchBox_TextChanged);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(0, 0);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(353, 33);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "السلع الغير مسلمة";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.Controls.Add(this.btnExit, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelX16, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnCloseOperation, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnAddPayment, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnPrint, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSave, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnDepotPayed, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonX1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1020, 39);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // btnExit
            // 
            this.btnExit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnExit.Location = new System.Drawing.Point(3, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F4);
            this.btnExit.Size = new System.Drawing.Size(106, 33);
            this.btnExit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnExit.Symbol = "";
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "خروج F4";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // labelX16
            // 
            this.labelX16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX16.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.partnersBindingSource, "CustomerName", true));
            this.labelX16.ForeColor = System.Drawing.Color.Black;
            this.labelX16.Location = new System.Drawing.Point(784, 3);
            this.labelX16.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(236, 33);
            this.labelX16.TabIndex = 5;
            this.labelX16.Text = "-----";
            this.labelX16.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // partnersBindingSource
            // 
            this.partnersBindingSource.DataMember = "partners";
            this.partnersBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // btnCloseOperation
            // 
            this.btnCloseOperation.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCloseOperation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseOperation.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCloseOperation.Location = new System.Drawing.Point(227, 3);
            this.btnCloseOperation.Name = "btnCloseOperation";
            this.btnCloseOperation.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F4);
            this.btnCloseOperation.Size = new System.Drawing.Size(106, 33);
            this.btnCloseOperation.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCloseOperation.Symbol = "";
            this.btnCloseOperation.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnCloseOperation.TabIndex = 5;
            this.btnCloseOperation.Text = "عملية منتهية";
            this.btnCloseOperation.Tooltip = "عملية منتهية";
            this.btnCloseOperation.Click += new System.EventHandler(this.btnCloseOperation_Click);
            // 
            // btnAddPayment
            // 
            this.btnAddPayment.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddPayment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddPayment.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddPayment.Image = global::MTM.Properties.Resources.NewPayment;
            this.btnAddPayment.ImageFixedSize = new System.Drawing.Size(24, 24);
            this.btnAddPayment.Location = new System.Drawing.Point(115, 3);
            this.btnAddPayment.Name = "btnAddPayment";
            this.btnAddPayment.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F12);
            this.btnAddPayment.Size = new System.Drawing.Size(106, 33);
            this.btnAddPayment.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddPayment.TabIndex = 5;
            this.btnAddPayment.Text = "تسجيل دفع";
            this.btnAddPayment.Tooltip = "تسجيل دفع";
            this.btnAddPayment.Click += new System.EventHandler(this.btnAddPayment_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.AutoExpandOnClick = true;
            this.btnPrint.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnPrint.Location = new System.Drawing.Point(339, 3);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.ShowSubItems = false;
            this.btnPrint.Size = new System.Drawing.Size(106, 33);
            this.btnPrint.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPrint.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnPrintO,
            this.btnPrintDirect});
            this.btnPrint.Symbol = "";
            this.btnPrint.SymbolColor = System.Drawing.Color.DodgerBlue;
            this.btnPrint.TabIndex = 4;
            this.btnPrint.Text = "طباعة";
            this.btnPrint.Tooltip = "طباعة";
            // 
            // btnPrintO
            // 
            this.btnPrintO.GlobalItem = false;
            this.btnPrintO.Name = "btnPrintO";
            this.btnPrintO.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F9);
            this.btnPrintO.Text = "معاينة";
            this.btnPrintO.Click += new System.EventHandler(this.btnPrintO_Click);
            // 
            // btnPrintDirect
            // 
            this.btnPrintDirect.GlobalItem = false;
            this.btnPrintDirect.Name = "btnPrintDirect";
            this.btnPrintDirect.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F8);
            this.btnPrintDirect.Text = "مباشرة";
            this.btnPrintDirect.Tooltip = "مباشرة";
            this.btnPrintDirect.Click += new System.EventHandler(this.btnPrintDirect_Click);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSave.Location = new System.Drawing.Point(451, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F4);
            this.btnSave.Size = new System.Drawing.Size(106, 33);
            this.btnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSave.Symbol = "";
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "تسجيل";
            this.btnSave.Tooltip = "تسجيل";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDepotPayed
            // 
            this.btnDepotPayed.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDepotPayed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDepotPayed.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDepotPayed.Location = new System.Drawing.Point(563, 3);
            this.btnDepotPayed.Name = "btnDepotPayed";
            this.btnDepotPayed.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F4);
            this.btnDepotPayed.Size = new System.Drawing.Size(106, 33);
            this.btnDepotPayed.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnDepotPayed.TabIndex = 5;
            this.btnDepotPayed.Text = "مخلص فالديبو";
            this.btnDepotPayed.Tooltip = "مخلص فالديبو تع سطيف أو الجزائر";
            this.btnDepotPayed.Click += new System.EventHandler(this.btnDepotPayed_Click);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(675, 3);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F4);
            this.buttonX1.Size = new System.Drawing.Size(106, 33);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.TabIndex = 5;
            this.buttonX1.Text = "تغيير الزبون";
            this.buttonX1.Tooltip = "مخلص فالديبو تع سطيف أو الجزائر";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.DeliveryOrderLinesTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MTM.ViewDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // package_delivrer_orderTableAdapter
            // 
            this.package_delivrer_orderTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.deliverersTableAdapter = null;
            this.tableAdapterManager1.discountlinesTableAdapter = null;
            this.tableAdapterManager1.discountsTableAdapter = null;
            this.tableAdapterManager1.expensesTableAdapter = null;
            this.tableAdapterManager1.locationsTableAdapter = null;
            this.tableAdapterManager1.operationsTableAdapter = null;
            this.tableAdapterManager1.package_delivrer_orderTableAdapter = this.package_delivrer_orderTableAdapter;
            this.tableAdapterManager1.package_order_expensesTableAdapter = null;
            this.tableAdapterManager1.package_order_payments1TableAdapter = null;
            this.tableAdapterManager1.package_order_paymentsTableAdapter = null;
            this.tableAdapterManager1.packages_order_line_delivererTableAdapter = this.packages_order_line_delivererTableAdapter;
            this.tableAdapterManager1.packages_order_linesTableAdapter = null;
            this.tableAdapterManager1.packages_orderTableAdapter = null;
            this.tableAdapterManager1.partners_paymentsTableAdapter = null;
            this.tableAdapterManager1.partners_rcTableAdapter = null;
            this.tableAdapterManager1.partnersTableAdapter = null;
            this.tableAdapterManager1.payment_methodsTableAdapter = null;
            this.tableAdapterManager1.productsTableAdapter = null;
            this.tableAdapterManager1.UpdateOrder = MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager1.workers_absenceTableAdapter = null;
            this.tableAdapterManager1.workers_accountTableAdapter = null;
            this.tableAdapterManager1.workers_paymentsTableAdapter = null;
            this.tableAdapterManager1.workersTableAdapter = null;
            // 
            // packages_order_line_delivererTableAdapter
            // 
            this.packages_order_line_delivererTableAdapter.ClearBeforeFill = true;
            // 
            // deliverersTableAdapter
            // 
            this.deliverersTableAdapter.ClearBeforeFill = true;
            // 
            // productsTableAdapter
            // 
            this.productsTableAdapter.ClearBeforeFill = true;
            // 
            // packageOrderExpensesViewTableAdapter
            // 
            this.packageOrderExpensesViewTableAdapter.ClearBeforeFill = true;
            // 
            // partnersTableAdapter
            // 
            this.partnersTableAdapter.ClearBeforeFill = true;
            // 
            // AddEditDeliveryOrder
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1020, 642);
            this.ControlBox = false;
            this.Controls.Add(this.collapsibleSplitContainer1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.KeyPreview = true;
            this.Name = "AddEditDeliveryOrder";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "وصل تسليم";
            this.Load += new System.EventHandler(this.AddEditDeliveryOrder_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AddEditDeliveryOrder_KeyDown);
            this.collapsibleSplitContainer1.Panel1.ResumeLayout(false);
            this.collapsibleSplitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.collapsibleSplitContainer1)).EndInit();
            this.collapsibleSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DeliveryDataGridViewX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packages_order_line_delivererBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).EndInit();
            this.DepotPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TotalPackAmountDepot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.package_delivrer_orderBindingSource)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.panelEx2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deliverersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datDateTimeInput)).EndInit();
            this.ExpensesCommandesTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExpensesDataGridViewX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packageOrderExpensesViewBindingSource)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TotalBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResteBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.doubleInput1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGridViewX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).EndInit();
            this.panelEx1.ResumeLayout(false);
            this.panelEx3.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.partnersBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.CollapsibleSplitContainer collapsibleSplitContainer1;
        private DevComponents.DotNetBar.Controls.DataGridViewX mainDataGridViewX;
        private DevComponents.DotNetBar.Controls.DataGridViewX DeliveryDataGridViewX1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevComponents.DotNetBar.ButtonX btnExit;
        private DevComponents.DotNetBar.ButtonX btnPrint;
        private DevComponents.DotNetBar.ButtonX btnCloseOperation;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.PanelEx panelEx3;
        private DevComponents.DotNetBar.ButtonX btnAddLine;
        private DevComponents.DotNetBar.Controls.TextBoxX ProductSearchBox;
        private ViewDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput datDateTimeInput;
        private System.Windows.Forms.BindingSource package_delivrer_orderBindingSource;
        private gama_transportationdbDataSet gama_transportationdbDataSet;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX2;
        private gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter package_delivrer_orderTableAdapter;
        private gama_transportationdbDataSetTableAdapters.TableAdapterManager tableAdapterManager1;
        private DevComponents.DotNetBar.PanelEx panelEx2;
        private DevComponents.DotNetBar.Controls.TextBoxX noteTextBoxX;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private DevComponents.DotNetBar.ButtonX btnDeleteLine;
        private DevComponents.DotNetBar.Controls.ComboBoxEx comboBoxEx1;
        private DevComponents.DotNetBar.LabelX labelX9;
        private System.Windows.Forms.BindingSource deliverersBindingSource;
        private gama_transportationdbDataSetTableAdapters.deliverersTableAdapter deliverersTableAdapter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.Editors.DoubleInput TotalBox;
        private DevComponents.DotNetBar.Controls.SwitchButton deliveredSwitchButton;
        private DevComponents.DotNetBar.ButtonX btnAddPayment;
        private DevComponents.DotNetBar.ButtonItem btnPrintDirect;
        private DevComponents.DotNetBar.ButtonItem btnPrintO;
        private System.Windows.Forms.TableLayoutPanel ExpensesCommandesTable;
        private DevComponents.DotNetBar.ButtonX btnDeleteExpenses;
        private DevComponents.DotNetBar.ButtonX btnEditExpenses;
        private DevComponents.DotNetBar.ButtonX btnAddExpences;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.Controls.DataGridViewX ExpensesDataGridViewX1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.LabelX labelX15;
        private System.Windows.Forms.BindingSource productsBindingSource;
        private gama_transportationdbDataSetTableAdapters.productsTableAdapter productsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn productNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn partnersIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource packageOrderExpensesViewBindingSource;
        private gama_transportationdbDataSetTableAdapters.PackageOrderExpensesViewTableAdapter packageOrderExpensesViewTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn designationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn partnerspaymentsIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderpaymentsIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource packages_order_line_delivererBindingSource;
        private gama_transportationdbDataSetTableAdapters.packages_order_line_delivererTableAdapter packages_order_line_delivererTableAdapter;
        private DevComponents.DotNetBar.LabelX labelX16;
        private System.Windows.Forms.BindingSource partnersBindingSource;
        private gama_transportationdbDataSetTableAdapters.partnersTableAdapter partnersTableAdapter;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private DevComponents.Editors.DoubleInput PaymentBox;
        private DevComponents.DotNetBar.LabelX labelX17;
        private DevComponents.Editors.DoubleInput ResteBox;
        private DevComponents.DotNetBar.LabelX labelX18;
        private System.Windows.Forms.TableLayoutPanel DepotPanel;
        private DevComponents.DotNetBar.ButtonX btnDepotPayed;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.LabelX DepotLbl;
        private DevComponents.Editors.DoubleInput TotalPackAmountDepot;
        private DevComponents.DotNetBar.LabelX labelX20;
        private DevComponents.Editors.DoubleInput DiscountBox;
        private DevComponents.Editors.DoubleInput doubleInput1;
        private DevComponents.DotNetBar.LabelX labelX21;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private System.Windows.Forms.DataGridViewTextBoxColumn DesignationColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn deliveredDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn packagedelivrerorderIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn packagedelivrerorderSerialYearDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
    }
}