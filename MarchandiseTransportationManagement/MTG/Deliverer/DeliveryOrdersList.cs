using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Microsoft.Reporting.WinForms;

namespace MTM.Deliverer
{
    public partial class DeliveryOrdersList : UserControl,IIUserControl
    {
        public DeliveryOrdersList()
        {
            InitializeComponent();
            btnSetDeliveredColumn.Click += (ee, rr) =>
            {
                CloseTransportOperation();
                return;
            };
        }

        private void DeliveryOrdersList_Load(object sender, EventArgs e)
        {
            dateTimeInput1.Value = DateTime.Now.AddDays(-7);
            deliveryOrderListTableAdapter.FillByDate(viewDataSet.DeliveryOrderList, dateTimeInput1.Value.Date, dateTimeInput2.Value.Date.AddDays(1));
        }

        private void CloseTransportOperation()
        {
            if (deliveryOrderListBindingSource.Position < 0) return;
            bool IsDelivered = Convert.ToBoolean(((DataRowView)deliveryOrderListBindingSource.Current)["Delivered"]);
            int OrderID = Convert.ToInt32(((DataRowView)deliveryOrderListBindingSource.Current)["ID"]);
            int IdYear = Convert.ToInt32(((DataRowView)deliveryOrderListBindingSource.Current)["SerialYear"]);
            if (IsDelivered)
            {
                if (MessageBox.Show("��� ������ �� ������� �� ��� �� ���� ����� �������", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    new gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter().UpdateDelivered(!IsDelivered, OrderID, IdYear);
                    new gama_transportationdbDataSetTableAdapters.packages_order_line_delivererTableAdapter().UpdateDelivered(!IsDelivered, OrderID, IdYear);
                }
            }
            else
            {
                new gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter().UpdateDelivered(!IsDelivered, OrderID, IdYear);
                new gama_transportationdbDataSetTableAdapters.packages_order_line_delivererTableAdapter().UpdateDelivered(!IsDelivered, OrderID, IdYear); ToastNotification.Show(this, "�� ����� ������ ����� �������");
                if (MessageBox.Show("�� ���� ����� ���� ����ɿ", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    var partnerID = Convert.ToInt32(((DataRowView)deliveryOrderListBindingSource.Current)["partners_ID"]);
                    var totalOrder = Convert.ToDecimal(((DataRowView)deliveryOrderListBindingSource.Current)["Total"]) -
                        new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter().SumOfPaymentsByDeliveryOrder(OrderID, IdYear).GetValueOrDefault(0);
                    new Payments.AddEditPayment(0, partnerID) { StartUpAmount = Convert.ToDouble(totalOrder), DelivererOrderID = OrderID, DelivererOrderYear = IdYear }.ShowDialog(this);
                }
            }

            if (FilterIsNotDelivered)
            {
                deliveryOrderListTableAdapter.FillByDateNotDelivered(viewDataSet.DeliveryOrderList, dateTimeInput1.Value.Date, dateTimeInput2.Value.Date.AddDays(1));
            }
            else
            {
                deliveryOrderListTableAdapter.FillByDate(viewDataSet.DeliveryOrderList, dateTimeInput1.Value.Date, dateTimeInput2.Value.Date.AddDays(1));
            }
        }

        private void btnCloseOperation_Click(object sender, EventArgs e)
        {
            CloseTransportOperation();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var Adapter = new gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter();
            var id = Adapter.MaxID(DateTime.Now.Year).GetValueOrDefault(0)+1;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (deliveryOrderListBindingSource.Position < 0) return;
            int idLine = Convert.ToInt32(((DataRowView)deliveryOrderListBindingSource.Current)["ID"]);
            int idyear = Convert.ToInt32(((DataRowView)deliveryOrderListBindingSource.Current)["SerialYear"]);
            int partnerID = Convert.ToInt32(((DataRowView)deliveryOrderListBindingSource.Current)["partners_ID"]);
            new AddEditDeliveryOrder() { IdYear = idyear, OrderID = idLine, PartnerID = partnerID }.ShowDialog(this);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("����� ��� ����� �����Ͽ", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
                int idLine = Convert.ToInt32(((DataRowView)deliveryOrderListBindingSource.Current)["ID"]);
                int idyear = Convert.ToInt32(((DataRowView)deliveryOrderListBindingSource.Current)["SerialYear"]);
                new gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter().DeleteQuery(idLine, idyear);
                deliveryOrderListBindingSource.RemoveCurrent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public bool FilterIsNotDelivered { get; set; }
        private void buttonX2_Click(object sender, EventArgs e)
        {
            deliveryOrderListTableAdapter.FillByDateNotDelivered(viewDataSet.DeliveryOrderList, dateTimeInput1.Value.Date, dateTimeInput2.Value.Date.AddDays(1));
            FilterIsNotDelivered = true;
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            deliveryOrderListTableAdapter.FillByDate(viewDataSet.DeliveryOrderList, dateTimeInput1.Value.Date, dateTimeInput2.Value.Date.AddDays(1));
            FilterIsNotDelivered = false;
        }

        private void btnPrintO_Click(object sender, EventArgs e)
        {
            if (deliveryOrderListBindingSource.Position < 0) return;
            int OrderID = Convert.ToInt32(((DataRowView)deliveryOrderListBindingSource.Current)["ID"]);
            int IdYear = Convert.ToInt32(((DataRowView)deliveryOrderListBindingSource.Current)["SerialYear"]);

            var adapter = new gama_transportationdbDataSetTableAdapters.packages_order_line_delivererTableAdapter();

            new Deliverer.Reports.DeliveryOrderReportForm()
            {
                OrderID = OrderID,
                YearID = IdYear,
                totalreste = Convert.ToDecimal(((DataRowView)deliveryOrderListBindingSource.Current)["Total"]),
                totalpayed = Convert.ToDecimal(((DataRowView)deliveryOrderListBindingSource.Current)["PaymentsAmount"])
            }.ShowDialog(this);
        }

        private void btnPrintDirect_Click(object sender, EventArgs e)
        {
            var adapter = new gama_transportationdbDataSetTableAdapters.packages_order_line_delivererTableAdapter();
            foreach (DataGridViewRow row in DeliveryDataGridViewX1.SelectedRows)
            {
                int OrderID = Convert.ToInt32(row.Cells[iDDataGridViewTextBoxColumn.Name].Value);
                int IdYear = Convert.ToInt32(row.Cells[serialYearDataGridViewTextBoxColumn.Name].Value);
                try
                {
                    List<ReportDataSource> dataSources = new List<ReportDataSource>();
                    dataSources.Add(new ReportDataSource("DataSet1", (object)new ViewDataSetTableAdapters.DeliveryOrderListTableAdapter().GetDataByID(OrderID, IdYear)));
                    dataSources.Add(new ReportDataSource("DataSet2", (object)new ViewDataSetTableAdapters.DeliveryOrderLinesTableAdapter().GetDataByID(OrderID, IdYear)));
                    dataSources.Add(new ReportDataSource("DataSet3", (object)new gama_transportationdbDataSetTableAdapters.PackageOrderExpensesViewTableAdapter().GetDataByOrder(OrderID, IdYear)));
                    var totalreste = Convert.ToDecimal(row.Cells[TotalColumn.Name].Value);
                    var totalpayed = Convert.ToDecimal(row.Cells[PaymentsAmountColumn.Name].Value);

                    ReportParameter rp1, rp2, rp3, rp4, rp5, rp6;
                    SSUtilities.SSConfigFile cfg = new SSUtilities.SSConfigFile();
                    rp1 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName, ""));
                    rp2 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity, ""));
                    rp3 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1, ""));
                    rp4 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, ""));
                    rp5 = new ReportParameter("totalpayed", totalpayed.ToString());
                    rp6 = new ReportParameter("totalreste", totalreste.ToString());
                    var printer = cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.MTM.DefaultPrinter, "");
                    new Report.Direct().DirectPrinting(printer, "MTM.Deliverer.Reports.DeliveryOrderReport.rdlc", false, dataSources, null, "None", new ReportParameter[] { rp1, rp2, rp3, rp4, rp5, rp6 });
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
                //if (deliveryOrderListBindingSource.Position < 0) return;
                //int OrderID = Convert.ToInt32(((DataRowView)deliveryOrderListBindingSource.Current)["ID"]);
                //int IdYear = Convert.ToInt32(((DataRowView)deliveryOrderListBindingSource.Current)["SerialYear"]);
                //var adapter = new gama_transportationdbDataSetTableAdapters.packages_order_line_delivererTableAdapter();
               
                //try
                //{
                //    List<ReportDataSource> dataSources = new List<ReportDataSource>();
                //    dataSources.Add(new ReportDataSource("DataSet1", (object)new ViewDataSetTableAdapters.DeliveryOrderListTableAdapter().GetDataByID(OrderID, IdYear)));
                //    dataSources.Add(new ReportDataSource("DataSet2", (object)new ViewDataSetTableAdapters.DeliveryOrderLinesTableAdapter().GetDataByID(OrderID, IdYear)));
                //    dataSources.Add(new ReportDataSource("DataSet3", (object)new gama_transportationdbDataSetTableAdapters.PackageOrderExpensesViewTableAdapter().GetDataByOrder(OrderID, IdYear)));
                //    var totalreste = Convert.ToDecimal(((DataRowView)deliveryOrderListBindingSource.Current)["Total"]);
                //    var totalpayed = Convert.ToDecimal(((DataRowView)deliveryOrderListBindingSource.Current)["PaymentsAmount"]);

                //    ReportParameter rp1, rp2, rp3, rp4, rp5, rp6;
                //    SSUtilities.SSConfigFile cfg = new SSUtilities.SSConfigFile();
                //    rp1 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName, ""));
                //    rp2 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity, ""));
                //    rp3 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1, ""));
                //    rp4 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, ""));
                //    rp5 = new ReportParameter("totalpayed", totalpayed.ToString());
                //    rp6 = new ReportParameter("totalreste", totalreste.ToString());
                //    var printer = cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.MTM.DefaultPrinter, "");
                //    new Report.Direct().DirectPrinting(printer, "MTM.Deliverer.Reports.DeliveryOrderReport.rdlc", false, dataSources, null, "None", new ReportParameter[] { rp1, rp2, rp3, rp4, rp5, rp6 });
                //}
                //catch (Exception ex)
                //{
                //    MessageBox.Show(ex.Message);
                //}
        }

        private void textBoxX1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                deliveryOrderListBindingSource.Filter = string.Format("CustomerName LIKE '%{0}%' OR DelivererName LIKE '%{0}%'  OR deliverersPhone LIKE '%{0}%' OR Phones LIKE '%{0}%'", textBoxX1.Text);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void deliveryOrderListBindingSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                bool IsDelivered = Convert.ToBoolean(((DataRowView)deliveryOrderListBindingSource.Current)["Delivered"]);
                if (IsDelivered)
                {
                    btnSetDelivered.Text = "����� �������";
                }
                else
                {
                    btnSetDelivered.Text = "����� �������";
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void deliveryOrderListBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            this.Validate();
            foreach (DataGridViewRow dr in DeliveryDataGridViewX1.Rows)
            {
                if (Convert.ToDecimal(dr.Cells[ResteToPayColumn.Name].Value) > 0)
                {
                    dr.DefaultCellStyle.BackColor = Color.YellowGreen;
                }
                else if (Convert.ToDecimal(dr.Cells[ResteToPayColumn.Name].Value) == 0)
                {
                    dr.DefaultCellStyle.BackColor = Color.OrangeRed;
                }
                else 
                {
                    dr.DefaultCellStyle.BackColor = Color.LightGreen;
                }
            }
            var tot = viewDataSet.DeliveryOrderList.Where(x => x.DelivererName.Contains(textBoxX1.Text) || x.CustomerName.Contains(textBoxX1.Text) ||(!x.IsdeliverersPhoneNull() && x.deliverersPhone.Contains(textBoxX1.Text))
                 || (!x.IsPhonesNull() && x.Phones.Contains(textBoxX1.Text))).Sum(x => x.Total);
            TotalBox.Value =Convert.ToDouble( tot);
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            var selectPartner = new Partners.SelectPartner();
            selectPartner.IsFactory = true;
            selectPartner.IsMerchant = true;
            if (selectPartner.ShowDialog(this) == DialogResult.Cancel) return;
                
            var select = new SelectDeliverer();
            if (select.ShowDialog(this) == DialogResult.Cancel) return;

            var Adapter = new gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter();
            var id = Adapter.MaxID(DateTime.Now.Year).GetValueOrDefault(0) + 1;

            Adapter.Insert(id, DateTime.Now.Year, select.SelectedDelivererID,  DateTime.Now, "", 0,0.00M, selectPartner.SelectedPartnerID,0,0,0,0,0);
            new AddEditDeliveryOrder() { IdYear = DateTime.Now.Year, OrderID = id, PartnerID = selectPartner.SelectedPartnerID }.ShowDialog(this);
        }

        public void Refill()
        {
            if (!FilterIsNotDelivered)
            {
                buttonX1_Click(null, null);
            }
            else
            {
                buttonX2_Click(null, null);
            }
        }
    }
}