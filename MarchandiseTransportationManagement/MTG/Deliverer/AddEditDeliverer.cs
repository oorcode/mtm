using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Deliverer
{
    public partial class AddEditDeliverer : DevComponents.DotNetBar.Metro.MetroForm
    {
        int ID = 0;
        public AddEditDeliverer(int Id_)
        {
            InitializeComponent();
            ID = Id_;
        }

        private void AddEditFactory_Load(object sender, EventArgs e)
        {
            this.deliverersTableAdapter.Fill(this.gama_transportationdbDataSet.deliverers);
            if (ID == 0)
            {
                deliverersBindingSource.AddNew();
            }
            else
            {
                this.deliverersTableAdapter.FillByID(gama_transportationdbDataSet.deliverers, ID);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }

        private void Save()
        {
            this.Validate();
            this.deliverersBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.gama_transportationdbDataSet);
        }
    }
}