using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Deliverer
{
    public partial class AddEditDeliveryOrderLine : DevComponents.DotNetBar.Metro.MetroForm
    {
        public double maxQte;
        public double Price;
        public AddEditDeliveryOrderLine()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                maxQte = doubleInput1.Value;
                Price = priceDoubleInput.Value;
                DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }

        private void AddEditDeliveryOrderLine_Load(object sender, EventArgs e)
        {
            priceDoubleInput.Value = Price;
        }

        private void AddEditDeliveryOrderLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                this.GetNextControl(this, true).Focus();
            }
        }
    }
}