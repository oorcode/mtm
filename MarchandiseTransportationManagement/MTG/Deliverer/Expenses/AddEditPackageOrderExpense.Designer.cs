﻿namespace MTM.Deliverer.Expenses
{
    partial class AddEditPackageOrderExpense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gama_transportationdbDataSet = new MTM.gama_transportationdbDataSet();
            this.AmountDoubleInput = new DevComponents.Editors.DoubleInput();
            this.packageOrderExpensesViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.packages_order_linesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DesignationTextBoxX = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.packages_order_linesTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.packages_order_linesTableAdapter();
            this.productsTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.productsTableAdapter();
            this.datDateTimeInput = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.tableAdapterManager = new MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager();
            this.productsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.packageOrderExpensesViewTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.PackageOrderExpensesViewTableAdapter();
            this.textBoxX1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountDoubleInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packageOrderExpensesViewBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packages_order_linesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datDateTimeInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gama_transportationdbDataSet
            // 
            this.gama_transportationdbDataSet.DataSetName = "gama_transportationdbDataSet";
            this.gama_transportationdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // AmountDoubleInput
            // 
            this.AmountDoubleInput.AllowEmptyState = false;
            this.AmountDoubleInput.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.AmountDoubleInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.AmountDoubleInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.AmountDoubleInput.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.AmountDoubleInput.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.packageOrderExpensesViewBindingSource, "Amount", true));
            this.AmountDoubleInput.FocusHighlightEnabled = true;
            this.AmountDoubleInput.ForeColor = System.Drawing.Color.Black;
            this.AmountDoubleInput.Increment = 1D;
            this.AmountDoubleInput.Location = new System.Drawing.Point(143, 78);
            this.AmountDoubleInput.Name = "AmountDoubleInput";
            this.AmountDoubleInput.Size = new System.Drawing.Size(149, 27);
            this.AmountDoubleInput.TabIndex = 2;
            // 
            // packageOrderExpensesViewBindingSource
            // 
            this.packageOrderExpensesViewBindingSource.DataMember = "PackageOrderExpensesView";
            this.packageOrderExpensesViewBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // packages_order_linesBindingSource
            // 
            this.packages_order_linesBindingSource.DataMember = "packages_order_lines";
            this.packages_order_linesBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // DesignationTextBoxX
            // 
            this.DesignationTextBoxX.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.DesignationTextBoxX.Border.Class = "TextBoxBorder";
            this.DesignationTextBoxX.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DesignationTextBoxX.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.packageOrderExpensesViewBindingSource, "Designation", true));
            this.DesignationTextBoxX.DisabledBackColor = System.Drawing.Color.White;
            this.DesignationTextBoxX.FocusHighlightEnabled = true;
            this.DesignationTextBoxX.ForeColor = System.Drawing.Color.Black;
            this.DesignationTextBoxX.Location = new System.Drawing.Point(48, 12);
            this.DesignationTextBoxX.Name = "DesignationTextBoxX";
            this.DesignationTextBoxX.PreventEnterBeep = true;
            this.DesignationTextBoxX.Size = new System.Drawing.Size(244, 27);
            this.DesignationTextBoxX.TabIndex = 0;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(298, 78);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(83, 27);
            this.labelX1.TabIndex = 54;
            this.labelX1.Text = "المبلـــــــــغ:";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(298, 45);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(83, 27);
            this.labelX3.TabIndex = 53;
            this.labelX3.Text = "التاريـــــــــخ:";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(298, 12);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(83, 27);
            this.labelX4.TabIndex = 54;
            this.labelX4.Text = "التعييـــــــن:";
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnCancel.Location = new System.Drawing.Point(12, 149);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(111, 32);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.Symbol = "";
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "خروج";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnSave.Location = new System.Drawing.Point(129, 149);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(111, 32);
            this.btnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSave.Symbol = "";
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "حفظ";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // packages_order_linesTableAdapter
            // 
            this.packages_order_linesTableAdapter.ClearBeforeFill = true;
            // 
            // productsTableAdapter
            // 
            this.productsTableAdapter.ClearBeforeFill = true;
            // 
            // datDateTimeInput
            // 
            this.datDateTimeInput.AllowEmptyState = false;
            this.datDateTimeInput.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.datDateTimeInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.datDateTimeInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.datDateTimeInput.ButtonDropDown.Visible = true;
            this.datDateTimeInput.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.packageOrderExpensesViewBindingSource, "dat", true));
            this.datDateTimeInput.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.packageOrderExpensesViewBindingSource, "dat", true));
            this.datDateTimeInput.FocusHighlightEnabled = true;
            this.datDateTimeInput.ForeColor = System.Drawing.Color.Black;
            this.datDateTimeInput.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center;
            this.datDateTimeInput.IsPopupCalendarOpen = false;
            this.datDateTimeInput.Location = new System.Drawing.Point(143, 45);
            // 
            // 
            // 
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.datDateTimeInput.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.DisplayMonth = new System.DateTime(2018, 12, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.TodayButtonVisible = true;
            this.datDateTimeInput.Name = "datDateTimeInput";
            this.datDateTimeInput.Size = new System.Drawing.Size(149, 27);
            this.datDateTimeInput.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.datDateTimeInput.TabIndex = 56;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.deliverersTableAdapter = null;
            this.tableAdapterManager.discountlinesTableAdapter = null;
            this.tableAdapterManager.discountsTableAdapter = null;
            this.tableAdapterManager.locationsTableAdapter = null;
            this.tableAdapterManager.operationsTableAdapter = null;
            this.tableAdapterManager.package_delivrer_orderTableAdapter = null;
            this.tableAdapterManager.package_order_expensesTableAdapter = null;
            this.tableAdapterManager.package_order_paymentsTableAdapter = null;
            this.tableAdapterManager.packages_order_line_delivererTableAdapter = null;
            this.tableAdapterManager.packages_order_linesTableAdapter = null;
            this.tableAdapterManager.packages_orderTableAdapter = null;
            this.tableAdapterManager.partners_paymentsTableAdapter = null;
            this.tableAdapterManager.partnersTableAdapter = null;
            this.tableAdapterManager.productsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.workers_accountTableAdapter = null;
            this.tableAdapterManager.workers_paymentsTableAdapter = null;
            this.tableAdapterManager.workersTableAdapter = null;
            // 
            // productsBindingSource
            // 
            this.productsBindingSource.DataMember = "products";
            this.productsBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // packageOrderExpensesViewTableAdapter
            // 
            this.packageOrderExpensesViewTableAdapter.ClearBeforeFill = true;
            // 
            // textBoxX1
            // 
            this.textBoxX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.textBoxX1.Border.Class = "TextBoxBorder";
            this.textBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.packageOrderExpensesViewBindingSource, "Note", true));
            this.textBoxX1.DisabledBackColor = System.Drawing.Color.White;
            this.textBoxX1.ForeColor = System.Drawing.Color.Black;
            this.textBoxX1.Location = new System.Drawing.Point(12, 111);
            this.textBoxX1.Name = "textBoxX1";
            this.textBoxX1.PreventEnterBeep = true;
            this.textBoxX1.Size = new System.Drawing.Size(280, 27);
            this.textBoxX1.TabIndex = 57;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(298, 111);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(83, 27);
            this.labelX2.TabIndex = 54;
            this.labelX2.Text = "ملاحظة:";
            // 
            // AddEditPackageOrderExpense
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(401, 193);
            this.ControlBox = false;
            this.Controls.Add(this.textBoxX1);
            this.Controls.Add(this.datDateTimeInput);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.DesignationTextBoxX);
            this.Controls.Add(this.AmountDoubleInput);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "AddEditPackageOrderExpense";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "إصافة سلعة";
            this.Load += new System.EventHandler(this.AddEditPackageOrderLine_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountDoubleInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packageOrderExpensesViewBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packages_order_linesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datDateTimeInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private gama_transportationdbDataSet gama_transportationdbDataSet;
        private System.Windows.Forms.BindingSource packages_order_linesBindingSource;
        private gama_transportationdbDataSetTableAdapters.packages_order_linesTableAdapter packages_order_linesTableAdapter;
        private gama_transportationdbDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private DevComponents.Editors.DoubleInput AmountDoubleInput;
        private DevComponents.DotNetBar.Controls.TextBoxX DesignationTextBoxX;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private System.Windows.Forms.BindingSource productsBindingSource;
        private gama_transportationdbDataSetTableAdapters.productsTableAdapter productsTableAdapter;
        private System.Windows.Forms.BindingSource packageOrderExpensesViewBindingSource;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput datDateTimeInput;
        private gama_transportationdbDataSetTableAdapters.PackageOrderExpensesViewTableAdapter packageOrderExpensesViewTableAdapter;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX1;
        private DevComponents.DotNetBar.LabelX labelX2;
    }
}