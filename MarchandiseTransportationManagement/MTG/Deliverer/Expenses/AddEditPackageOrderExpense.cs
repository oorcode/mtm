 using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Deliverer.Expenses
{
    public partial class AddEditPackageOrderExpense : DevComponents.DotNetBar.Metro.MetroForm
    {
        public int ExpenseID { get; set; }
        public int? PackageOrderID { get; set; }
        public int? PackageOrderYearID { get; set; }
        public int DepotID { get; set; }
        public AddEditPackageOrderExpense()
        {
            InitializeComponent();
        }

        private void AddEditPackageOrderLine_Load(object sender, EventArgs e)
        {
            this.packageOrderExpensesViewTableAdapter.Fill(this.gama_transportationdbDataSet.PackageOrderExpensesView, ExpenseID);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ExpenseID == 0)
            {
                try
                {
                    var amount = Convert.ToDecimal(AmountDoubleInput.Value);
                 
                    var ppayAdapter = new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter();
                    ppayAdapter.InsertQuery(datDateTimeInput.Value, amount, DepotID,textBoxX1.Text,PackageOrderID,PackageOrderYearID,null);
                    int PaymentID = ppayAdapter.MaxID().GetValueOrDefault(0);

                    var orderExpenseAdapter = new gama_transportationdbDataSetTableAdapters.package_order_expensesTableAdapter();
                    orderExpenseAdapter.Insert(PackageOrderID.Value, DesignationTextBoxX.Text, 0, PackageOrderYearID.Value);
                    int OrderExpenseID = orderExpenseAdapter.MaxID().GetValueOrDefault(0);

                    new gama_transportationdbDataSetTableAdapters.package_order_paymentsTableAdapter().InsertQuery(datDateTimeInput.Value, amount, PackageOrderID, PaymentID, OrderExpenseID, PackageOrderYearID);
                }
                catch (Exception ex)
                {
                    MessageBoxEx.Show(ex.ToString());
                }
            }
            else
            {
                var expenseLine = gama_transportationdbDataSet.PackageOrderExpensesView.FirstOrDefault();
                var amount = Convert.ToDecimal(AmountDoubleInput.Value);
                var ppayAdapter = new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter();
                var orderExpenseAdapter = new gama_transportationdbDataSetTableAdapters.package_order_expensesTableAdapter();
               
                orderExpenseAdapter.UpdateQuery(DesignationTextBoxX.Text, expenseLine.ID);
                ppayAdapter.UpdateQuery(datDateTimeInput.Value, amount,textBoxX1.Text, expenseLine.partners_payments_ID);
                new gama_transportationdbDataSetTableAdapters.package_order_paymentsTableAdapter().UpdateQuery(datDateTimeInput.Value, amount, textBoxX1.Text, expenseLine.order_payments_ID);
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
    }
}