using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Discount
{
    public partial class AddEditPartnerDiscount : DevComponents.DotNetBar.Metro.MetroForm
    {
        public int PartnerID { get; set; }
        public int Discounts_ID { get; set; }
        public int Id { get; set; }
        public AddEditPartnerDiscount(int Id_)
        {
            InitializeComponent();
            Id = Id_;
        }

        private void AddEditFactory_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.discountlines' table. You can move, or remove it, as needed.
            if (Id == 0)
            {
                discountlinesBindingSource.AddNew();
                ((DataRowView)discountlinesBindingSource.Current)["Amount"] = 0;
                ((DataRowView)discountlinesBindingSource.Current)["Discounts_ID"] = Discounts_ID;
                ((DataRowView)discountlinesBindingSource.Current)["partners_ID"] = PartnerID;
                ((DataRowView)discountlinesBindingSource.Current)["Deleted"] = false;
                ((DataRowView)discountlinesBindingSource.Current)["partners_payments_ID"] = DBNull.Value;
                ((DataRowView)discountlinesBindingSource.Current)["dat"] = DateTime.Now;
            }
            else
            {
                this.discountlinesTableAdapter.FillByID(this.gama_transportationdbDataSet.discountlines, Id);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }

        private void Save()
        {
            this.Validate();
            this.discountlinesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(gama_transportationdbDataSet);
        }
    }
}