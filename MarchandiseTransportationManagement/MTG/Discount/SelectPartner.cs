using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Discount
{
    public partial class SelectPartner : DevComponents.DotNetBar.Metro.MetroForm
    {

        public decimal DiscountPercent { get; set; }
        public decimal MinBalance { get; set; }
        public int YEARid { get; set; }
        public SelectPartner()
        {
            InitializeComponent();
        }

        private void SelectPartner_Load(object sender, EventArgs e)
        {
            partnersForDiscountTableAdapter.FillPartnersForDiscount(viewDataSet.PartnersForDiscount, MinBalance, YEARid);
            SearchBox.Focus();
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Down)
            {
                partnersForDiscountBindingSource.MoveNext();
            }
            else if (e.KeyData == Keys.Up)
            {
                partnersForDiscountBindingSource.MovePrevious();
            }
            else if (e.KeyData == Keys.Enter)
            {
                Ok();
            }
        }

        public int SelectedPartnerID { get; set; }

        private void mainDataGridViewX_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Ok();
        }

        private void Ok()
        {
            if (partnersForDiscountBindingSource.Position < 0) return;
            SelectedPartnerID = Convert.ToInt32(((DataRowView)partnersForDiscountBindingSource.Current)["ID"]);
            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            SelectedPartnerID = 0;
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            Ok();
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                partnersForDiscountBindingSource.Filter = string.Format("CustomerName LIKE '%{0}%' ", SearchBox.Text);
            }
            catch (Exception ex)
            {
            }
        }

        private void partnersForDiscountBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            var dis = DiscountPercent/100;
            foreach (DataGridViewRow item in mainDataGridViewX.Rows)
            {
                try
                {
                    item.Cells[DiscountColumn.Name].Value = Convert.ToDecimal(item.Cells[balanceDataGridViewTextBoxColumn.Name].Value) * dis;
                }
                catch (Exception ex)
                {
                }
            }
        }


    }
}