using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Discount
{
    public partial class AddEditDiscountYear : DevComponents.DotNetBar.Metro.MetroForm
    {
        int Year = 0;
        public AddEditDiscountYear(int Id_)
        {
            InitializeComponent();
            Year = Id_;
        }

        private void AddEditFactory_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.discounts' table. You can move, or remove it, as needed.
            this.discountsTableAdapter.Fill(this.gama_transportationdbDataSet.discounts);
            if (Year == 0)
            {
                discountsBindingSource.AddNew();
                ((DataRowView)discountsBindingSource.Current)["Deleted"] = false;
                ((DataRowView)discountsBindingSource.Current)["DiscountYear"] = 2018;
                ((DataRowView)discountsBindingSource.Current)["MinMoneyAmount"] = 0;
                ((DataRowView)discountsBindingSource.Current)["DiscountPercentage"] = 0;
            }
            else
            {
                this.discountsTableAdapter.FillByYear(gama_transportationdbDataSet.discounts, Year);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }

        private void Save()
        {
            this.Validate();
            this.discountsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.gama_transportationdbDataSet);
        }
    }
}