﻿namespace MTM.Discount
{
    partial class AddEditDiscountYear
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gama_transportationdbDataSet = new MTM.gama_transportationdbDataSet();
            this.tableAdapterManager = new MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager();
            this.discountsTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.discountsTableAdapter();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.discountsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.discountYearIntegerInput = new DevComponents.Editors.IntegerInput();
            this.minMoneyAmountDoubleInput = new DevComponents.Editors.DoubleInput();
            this.discountPercentageDoubleInput = new DevComponents.Editors.DoubleInput();
            this.noteTextBoxX = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountYearIntegerInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minMoneyAmountDoubleInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountPercentageDoubleInput)).BeginInit();
            this.SuspendLayout();
            // 
            // gama_transportationdbDataSet
            // 
            this.gama_transportationdbDataSet.DataSetName = "gama_transportationdbDataSet";
            this.gama_transportationdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.deliverersTableAdapter = null;
            this.tableAdapterManager.discountlinesTableAdapter = null;
            this.tableAdapterManager.discountsTableAdapter = this.discountsTableAdapter;
            this.tableAdapterManager.locationsTableAdapter = null;
            this.tableAdapterManager.operationsTableAdapter = null;
            this.tableAdapterManager.package_delivrer_orderTableAdapter = null;
            this.tableAdapterManager.package_order_expensesTableAdapter = null;
            this.tableAdapterManager.package_order_paymentsTableAdapter = null;
            this.tableAdapterManager.packages_order_line_delivererTableAdapter = null;
            this.tableAdapterManager.packages_order_linesTableAdapter = null;
            this.tableAdapterManager.packages_orderTableAdapter = null;
            this.tableAdapterManager.partners_paymentsTableAdapter = null;
            this.tableAdapterManager.partnersTableAdapter = null;
            this.tableAdapterManager.productsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.workers_accountTableAdapter = null;
            this.tableAdapterManager.workers_paymentsTableAdapter = null;
            this.tableAdapterManager.workersTableAdapter = null;
            // 
            // discountsTableAdapter
            // 
            this.discountsTableAdapter.ClearBeforeFill = true;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(299, 45);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(121, 27);
            this.labelX1.TabIndex = 4;
            this.labelX1.Text = "الحد الأدنى المالي:";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(299, 12);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(121, 27);
            this.labelX2.TabIndex = 4;
            this.labelX2.Text = "السنة:";
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(12, 190);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(111, 32);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.Symbol = "";
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "خروج";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(129, 190);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(127, 32);
            this.btnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSave.Symbol = "";
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "حفظ";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(299, 78);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(121, 27);
            this.labelX3.TabIndex = 4;
            this.labelX3.Text = "نسبة التخفيض:";
            // 
            // discountsBindingSource
            // 
            this.discountsBindingSource.DataMember = "discounts";
            this.discountsBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // discountYearIntegerInput
            // 
            this.discountYearIntegerInput.AllowEmptyState = false;
            this.discountYearIntegerInput.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.discountYearIntegerInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.discountYearIntegerInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.discountYearIntegerInput.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.discountYearIntegerInput.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.discountsBindingSource, "DiscountYear", true));
            this.discountYearIntegerInput.FocusHighlightEnabled = true;
            this.discountYearIntegerInput.ForeColor = System.Drawing.Color.Black;
            this.discountYearIntegerInput.Location = new System.Drawing.Point(197, 12);
            this.discountYearIntegerInput.MaxValue = 8888;
            this.discountYearIntegerInput.MinValue = 2018;
            this.discountYearIntegerInput.Name = "discountYearIntegerInput";
            this.discountYearIntegerInput.Size = new System.Drawing.Size(82, 27);
            this.discountYearIntegerInput.TabIndex = 0;
            this.discountYearIntegerInput.Value = 2018;
            // 
            // minMoneyAmountDoubleInput
            // 
            this.minMoneyAmountDoubleInput.AllowEmptyState = false;
            this.minMoneyAmountDoubleInput.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.minMoneyAmountDoubleInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.minMoneyAmountDoubleInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.minMoneyAmountDoubleInput.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.minMoneyAmountDoubleInput.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.discountsBindingSource, "MinMoneyAmount", true));
            this.minMoneyAmountDoubleInput.FocusHighlightEnabled = true;
            this.minMoneyAmountDoubleInput.ForeColor = System.Drawing.Color.Black;
            this.minMoneyAmountDoubleInput.Increment = 1D;
            this.minMoneyAmountDoubleInput.Location = new System.Drawing.Point(139, 45);
            this.minMoneyAmountDoubleInput.MaxValue = 999999999D;
            this.minMoneyAmountDoubleInput.MinValue = 0D;
            this.minMoneyAmountDoubleInput.Name = "minMoneyAmountDoubleInput";
            this.minMoneyAmountDoubleInput.Size = new System.Drawing.Size(140, 27);
            this.minMoneyAmountDoubleInput.TabIndex = 1;
            // 
            // discountPercentageDoubleInput
            // 
            this.discountPercentageDoubleInput.AllowEmptyState = false;
            this.discountPercentageDoubleInput.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.discountPercentageDoubleInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.discountPercentageDoubleInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.discountPercentageDoubleInput.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.discountPercentageDoubleInput.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.discountsBindingSource, "DiscountPercentage", true));
            this.discountPercentageDoubleInput.FocusHighlightEnabled = true;
            this.discountPercentageDoubleInput.ForeColor = System.Drawing.Color.Black;
            this.discountPercentageDoubleInput.Increment = 1D;
            this.discountPercentageDoubleInput.Location = new System.Drawing.Point(139, 78);
            this.discountPercentageDoubleInput.MaxValue = 80D;
            this.discountPercentageDoubleInput.MinValue = 0D;
            this.discountPercentageDoubleInput.Name = "discountPercentageDoubleInput";
            this.discountPercentageDoubleInput.Size = new System.Drawing.Size(140, 27);
            this.discountPercentageDoubleInput.TabIndex = 2;
            // 
            // noteTextBoxX
            // 
            this.noteTextBoxX.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.noteTextBoxX.Border.Class = "TextBoxBorder";
            this.noteTextBoxX.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.noteTextBoxX.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.discountsBindingSource, "Note", true));
            this.noteTextBoxX.DisabledBackColor = System.Drawing.Color.White;
            this.noteTextBoxX.ForeColor = System.Drawing.Color.Black;
            this.noteTextBoxX.Location = new System.Drawing.Point(12, 111);
            this.noteTextBoxX.MaxLength = 255;
            this.noteTextBoxX.Multiline = true;
            this.noteTextBoxX.Name = "noteTextBoxX";
            this.noteTextBoxX.PreventEnterBeep = true;
            this.noteTextBoxX.Size = new System.Drawing.Size(267, 65);
            this.noteTextBoxX.TabIndex = 3;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(285, 111);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(135, 27);
            this.labelX4.TabIndex = 6;
            this.labelX4.Text = "ملاحظة:";
            // 
            // AddEditDiscountYear
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(441, 234);
            this.ControlBox = false;
            this.Controls.Add(this.noteTextBoxX);
            this.Controls.Add(this.discountPercentageDoubleInput);
            this.Controls.Add(this.minMoneyAmountDoubleInput);
            this.Controls.Add(this.discountYearIntegerInput);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "AddEditDiscountYear";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تسجيل إعدادات التخفيض السنوي";
            this.Load += new System.EventHandler(this.AddEditFactory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountYearIntegerInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minMoneyAmountDoubleInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountPercentageDoubleInput)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private gama_transportationdbDataSet gama_transportationdbDataSet;
        private gama_transportationdbDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private DevComponents.DotNetBar.LabelX labelX3;
        private System.Windows.Forms.BindingSource discountsBindingSource;
        private gama_transportationdbDataSetTableAdapters.discountsTableAdapter discountsTableAdapter;
        private DevComponents.Editors.IntegerInput discountYearIntegerInput;
        private DevComponents.Editors.DoubleInput minMoneyAmountDoubleInput;
        private DevComponents.Editors.DoubleInput discountPercentageDoubleInput;
        private DevComponents.DotNetBar.Controls.TextBoxX noteTextBoxX;
        private DevComponents.DotNetBar.LabelX labelX4;
    }
}