using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Discount
{
    public partial class DiscountsList : UserControl,IIUserControl
    {
        public DiscountsList()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            new AddEditDiscountYear(DateTime.Now.Year).ShowDialog(this);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (discountsBindingSource.Position < 0)
            {
                return;
            }

            new AddEditDiscountYear(Convert.ToInt32(((DataRowView)discountsBindingSource.Current)["DiscountYear"])).ShowDialog(this);
            Fill();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (discountsBindingSource.Position < 0)
            {
                return;
            }
            try
            {
                if (MessageBox.Show("����� ����� ��������", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
                discountsTableAdapter.DeleteQuery(Convert.ToInt32(((DataRowView)discountsBindingSource.Current)["ID"]));
                discountsBindingSource.RemoveCurrent();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }

        private void Fill()
        {
            int pos = discountsBindingSource.Position;
            discountsTableAdapter.Fill(gama_transportationdbDataSet.discounts);
            discountsBindingSource.Position = pos;
        }

        private void DiscountsList_Load(object sender, EventArgs e)
        {
            this.partnersTableAdapter.Fill(gama_transportationdbDataSet.partners);
            discountsTableAdapter.Fill(gama_transportationdbDataSet.discounts);
        }

        private void btnAddDiscount_Click(object sender, EventArgs e)
        {
            if (discountsBindingSource.Position < 0) { return; }
            var minMoney = Convert.ToDecimal(((DataRowView)discountsBindingSource.Current)["MinMoneyAmount"]);
            var disPercent = Convert.ToDecimal(((DataRowView)discountsBindingSource.Current)["DiscountPercentage"]);
            var yearID = Convert.ToInt32(((DataRowView)discountsBindingSource.Current)["DiscountYear"]);
            var sPartner = new SelectPartner() { MinBalance = minMoney, DiscountPercent = disPercent, YEARid = yearID };
            if (sPartner.ShowDialog(this) == DialogResult.Cancel) return;
            if (new AddEditPartnerDiscount(0) {PartnerID = sPartner.SelectedPartnerID ,
                                               Discounts_ID = Convert.ToInt32(((DataRowView)discountsBindingSource.Current)["ID"]),
                                                
            }.ShowDialog(this) == DialogResult.Cancel) return;
            if (discountsBindingSource.Position < 0) { return; }
            try
            {
                discountlinesTableAdapter.FillByDiscountsYearID(gama_transportationdbDataSet.discountlines, Convert.ToInt32(((DataRowView)discountsBindingSource.Current)["ID"]));
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }

        private void discountsBindingSource_PositionChanged(object sender, EventArgs e)
        {
            if (discountsBindingSource.Position < 0) { return; }
            try
            {
                discountlinesTableAdapter.FillByDiscountsYearID(gama_transportationdbDataSet.discountlines, Convert.ToInt32(((DataRowView)discountsBindingSource.Current)["ID"]));
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }

        private void btnEditDiscount_Click(object sender, EventArgs e)
        {
            if (discountlinesBindingSource.Position < 0) { return; }
            if (new AddEditPartnerDiscount(Convert.ToInt32(((DataRowView)discountlinesBindingSource.Current)["ID"]))
            {}.ShowDialog(this) == DialogResult.Cancel) return;
            if (discountsBindingSource.Position < 0) { return; }
            try
            {
                discountlinesTableAdapter.FillByDiscountsYearID(gama_transportationdbDataSet.discountlines, Convert.ToInt32(((DataRowView)discountsBindingSource.Current)["ID"]));
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }

        private void btnDeleteDiscount_Click(object sender, EventArgs e)
        {
            try
            {
                if (discountlinesBindingSource.Position < 0) { return; }
                if (MessageBox.Show("����� ����� ��������", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
                discountlinesTableAdapter.DeleteQuery(Convert.ToInt32(((DataRowView)discountlinesBindingSource.Current)["ID"]));
                discountlinesBindingSource.RemoveCurrent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void mainDataGridViewX_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        public void Refill()
        {
            Fill();
        }
    }
}