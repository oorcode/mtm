﻿namespace MTM
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.metroShell1 = new DevComponents.DotNetBar.Metro.MetroShell();
            this.metroTabPanel4 = new DevComponents.DotNetBar.Metro.MetroTabPanel();
            this.bar4 = new DevComponents.DotNetBar.Bar();
            this.btnAddDeliverer = new DevComponents.DotNetBar.ButtonItem();
            this.btnDelivererList = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem8 = new DevComponents.DotNetBar.ButtonItem();
            this.metroTabPanel1 = new DevComponents.DotNetBar.Metro.MetroTabPanel();
            this.bar2 = new DevComponents.DotNetBar.Bar();
            this.btnAddFactory = new DevComponents.DotNetBar.ButtonItem();
            this.btnAddMerchant = new DevComponents.DotNetBar.ButtonItem();
            this.btnWarehouse = new DevComponents.DotNetBar.ButtonItem();
            this.btnPartnersList = new DevComponents.DotNetBar.ButtonItem();
            this.btnAddPayments = new DevComponents.DotNetBar.ButtonItem();
            this.btnPaymentsList = new DevComponents.DotNetBar.ButtonItem();
            this.btnRC = new DevComponents.DotNetBar.ButtonItem();
            this.metroTabPanel2 = new DevComponents.DotNetBar.Metro.MetroTabPanel();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.btnSend = new DevComponents.DotNetBar.ButtonItem();
            this.btnOperationList = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem11 = new DevComponents.DotNetBar.ButtonItem();
            this.metroTabPanel7 = new DevComponents.DotNetBar.Metro.MetroTabPanel();
            this.bar7 = new DevComponents.DotNetBar.Bar();
            this.buttonItem6 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.metroTabPanel6 = new DevComponents.DotNetBar.Metro.MetroTabPanel();
            this.bar6 = new DevComponents.DotNetBar.Bar();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.metroTabPanel5 = new DevComponents.DotNetBar.Metro.MetroTabPanel();
            this.bar5 = new DevComponents.DotNetBar.Bar();
            this.btnAddDiscountYear = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.metroTabPanel8 = new DevComponents.DotNetBar.Metro.MetroTabPanel();
            this.bar8 = new DevComponents.DotNetBar.Bar();
            this.buttonItem9 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem10 = new DevComponents.DotNetBar.ButtonItem();
            this.metroTabPanel3 = new DevComponents.DotNetBar.Metro.MetroTabPanel();
            this.bar3 = new DevComponents.DotNetBar.Bar();
            this.buttonItem5 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.metroTabItem8 = new DevComponents.DotNetBar.Metro.MetroTabItem();
            this.metroTabItem7 = new DevComponents.DotNetBar.Metro.MetroTabItem();
            this.metroTabItem6 = new DevComponents.DotNetBar.Metro.MetroTabItem();
            this.metroTabItem5 = new DevComponents.DotNetBar.Metro.MetroTabItem();
            this.metroTabItem4 = new DevComponents.DotNetBar.Metro.MetroTabItem();
            this.metroTabItem2 = new DevComponents.DotNetBar.Metro.MetroTabItem();
            this.metroTabItem1 = new DevComponents.DotNetBar.Metro.MetroTabItem();
            this.metroTabItem3 = new DevComponents.DotNetBar.Metro.MetroTabItem();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.contextMenuBar1 = new DevComponents.DotNetBar.ContextMenuBar();
            this.buttonItem12 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem13 = new DevComponents.DotNetBar.ButtonItem();
            this.metroShell1.SuspendLayout();
            this.metroTabPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar4)).BeginInit();
            this.metroTabPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar2)).BeginInit();
            this.metroTabPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.metroTabPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar7)).BeginInit();
            this.metroTabPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar6)).BeginInit();
            this.metroTabPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar5)).BeginInit();
            this.metroTabPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar8)).BeginInit();
            this.metroTabPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contextMenuBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // metroShell1
            // 
            this.metroShell1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.metroShell1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroShell1.CaptionVisible = true;
            this.contextMenuBar1.SetContextMenuEx(this.metroShell1, this.buttonItem12);
            this.metroShell1.Controls.Add(this.metroTabPanel1);
            this.metroShell1.Controls.Add(this.metroTabPanel4);
            this.metroShell1.Controls.Add(this.metroTabPanel2);
            this.metroShell1.Controls.Add(this.metroTabPanel7);
            this.metroShell1.Controls.Add(this.metroTabPanel6);
            this.metroShell1.Controls.Add(this.metroTabPanel5);
            this.metroShell1.Controls.Add(this.metroTabPanel8);
            this.metroShell1.Controls.Add(this.metroTabPanel3);
            this.metroShell1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroShell1.ForeColor = System.Drawing.Color.Black;
            this.metroShell1.HelpButtonText = "مساعدة";
            this.metroShell1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.metroTabItem8,
            this.metroTabItem7,
            this.metroTabItem6,
            this.metroTabItem5,
            this.metroTabItem4,
            this.metroTabItem2,
            this.metroTabItem1,
            this.metroTabItem3});
            this.metroShell1.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.metroShell1.Location = new System.Drawing.Point(1, 1);
            this.metroShell1.Name = "metroShell1";
            this.metroShell1.SettingsButtonText = "إعدادات";
            this.metroShell1.Size = new System.Drawing.Size(998, 108);
            this.metroShell1.SystemText.MaximizeRibbonText = "&Maximize the Ribbon";
            this.metroShell1.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon";
            this.metroShell1.SystemText.QatAddItemText = "&Add to Quick Access Toolbar";
            this.metroShell1.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>";
            this.metroShell1.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar...";
            this.metroShell1.SystemText.QatDialogAddButton = "&Add >>";
            this.metroShell1.SystemText.QatDialogCancelButton = "Cancel";
            this.metroShell1.SystemText.QatDialogCaption = "Customize Quick Access Toolbar";
            this.metroShell1.SystemText.QatDialogCategoriesLabel = "&Choose commands from:";
            this.metroShell1.SystemText.QatDialogOkButton = "OK";
            this.metroShell1.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon";
            this.metroShell1.SystemText.QatDialogRemoveButton = "&Remove";
            this.metroShell1.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon";
            this.metroShell1.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon";
            this.metroShell1.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar";
            this.metroShell1.TabIndex = 0;
            this.metroShell1.TabStripFont = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroShell1.SettingsButtonClick += new System.EventHandler(this.metroShell1_SettingsButtonClick);
            // 
            // metroTabPanel4
            // 
            this.metroTabPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.metroTabPanel4.Controls.Add(this.bar4);
            this.metroTabPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabPanel4.Location = new System.Drawing.Point(0, 62);
            this.metroTabPanel4.Name = "metroTabPanel4";
            this.metroTabPanel4.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.metroTabPanel4.Size = new System.Drawing.Size(998, 46);
            // 
            // 
            // 
            this.metroTabPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroTabPanel4.TabIndex = 4;
            this.metroTabPanel4.Visible = false;
            // 
            // bar4
            // 
            this.bar4.AntiAlias = true;
            this.bar4.Dock = System.Windows.Forms.DockStyle.Top;
            this.bar4.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bar4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar4.IsMaximized = false;
            this.bar4.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnAddDeliverer,
            this.btnDelivererList,
            this.buttonItem8});
            this.bar4.ItemSpacing = 5;
            this.bar4.Location = new System.Drawing.Point(3, 0);
            this.bar4.Name = "bar4";
            this.bar4.Size = new System.Drawing.Size(992, 43);
            this.bar4.Stretch = true;
            this.bar4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar4.TabIndex = 2;
            this.bar4.TabStop = false;
            this.bar4.Text = "bar4";
            // 
            // btnAddDeliverer
            // 
            this.btnAddDeliverer.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAddDeliverer.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.btnAddDeliverer.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnAddDeliverer.Name = "btnAddDeliverer";
            this.btnAddDeliverer.Symbol = "";
            this.btnAddDeliverer.Text = "تسجيل الناقل";
            this.btnAddDeliverer.Click += new System.EventHandler(this.btnAddDeliverer_Click);
            // 
            // btnDelivererList
            // 
            this.btnDelivererList.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDelivererList.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.btnDelivererList.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnDelivererList.Name = "btnDelivererList";
            this.btnDelivererList.Symbol = "";
            this.btnDelivererList.Text = "قائمة الناقلين";
            this.btnDelivererList.Click += new System.EventHandler(this.btnDelivererList_Click);
            // 
            // buttonItem8
            // 
            this.buttonItem8.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem8.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.buttonItem8.Image = global::MTM.Properties.Resources.truck;
            this.buttonItem8.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem8.Name = "buttonItem8";
            this.buttonItem8.Text = "قائمة وصول التسليم";
            this.buttonItem8.Click += new System.EventHandler(this.buttonItem8_Click);
            // 
            // metroTabPanel1
            // 
            this.metroTabPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.metroTabPanel1.Controls.Add(this.bar2);
            this.metroTabPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabPanel1.Location = new System.Drawing.Point(0, 62);
            this.metroTabPanel1.Name = "metroTabPanel1";
            this.metroTabPanel1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.metroTabPanel1.Size = new System.Drawing.Size(998, 46);
            // 
            // 
            // 
            this.metroTabPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroTabPanel1.TabIndex = 1;
            // 
            // bar2
            // 
            this.bar2.AntiAlias = true;
            this.bar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.bar2.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bar2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar2.IsMaximized = false;
            this.bar2.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnAddFactory,
            this.btnAddMerchant,
            this.btnWarehouse,
            this.btnPartnersList,
            this.btnAddPayments,
            this.btnPaymentsList,
            this.btnRC});
            this.bar2.ItemSpacing = 5;
            this.bar2.Location = new System.Drawing.Point(3, 0);
            this.bar2.Name = "bar2";
            this.bar2.Size = new System.Drawing.Size(992, 43);
            this.bar2.Stretch = true;
            this.bar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar2.TabIndex = 1;
            this.bar2.TabStop = false;
            this.bar2.Text = "bar2";
            // 
            // btnAddFactory
            // 
            this.btnAddFactory.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAddFactory.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.btnAddFactory.Image = global::MTM.Properties.Resources.factory;
            this.btnAddFactory.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnAddFactory.Name = "btnAddFactory";
            this.btnAddFactory.Text = "تسجيل مصنع";
            this.btnAddFactory.Click += new System.EventHandler(this.btnAddFactory_Click);
            // 
            // btnAddMerchant
            // 
            this.btnAddMerchant.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAddMerchant.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.btnAddMerchant.Image = global::MTM.Properties.Resources.merchant;
            this.btnAddMerchant.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnAddMerchant.Name = "btnAddMerchant";
            this.btnAddMerchant.Text = "تسجيل تاجر";
            this.btnAddMerchant.Click += new System.EventHandler(this.btnAddMerchant_Click);
            // 
            // btnWarehouse
            // 
            this.btnWarehouse.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnWarehouse.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.btnWarehouse.Image = global::MTM.Properties.Resources.warehouse;
            this.btnWarehouse.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnWarehouse.Name = "btnWarehouse";
            this.btnWarehouse.Text = "تسجيل مخزن";
            this.btnWarehouse.Click += new System.EventHandler(this.btnWarehouse_Click);
            // 
            // btnPartnersList
            // 
            this.btnPartnersList.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPartnersList.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.btnPartnersList.Image = global::MTM.Properties.Resources.customers_;
            this.btnPartnersList.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnPartnersList.Name = "btnPartnersList";
            this.btnPartnersList.Text = "قائمة العملاء";
            this.btnPartnersList.Click += new System.EventHandler(this.btnPartnersList_Click);
            // 
            // btnAddPayments
            // 
            this.btnAddPayments.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAddPayments.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.btnAddPayments.Image = global::MTM.Properties.Resources.NewPayment;
            this.btnAddPayments.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnAddPayments.Name = "btnAddPayments";
            this.btnAddPayments.Text = "تسجيل دفعة";
            this.btnAddPayments.Click += new System.EventHandler(this.btnAddPayments_Click);
            // 
            // btnPaymentsList
            // 
            this.btnPaymentsList.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPaymentsList.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.btnPaymentsList.Image = global::MTM.Properties.Resources.paymentList;
            this.btnPaymentsList.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnPaymentsList.Name = "btnPaymentsList";
            this.btnPaymentsList.Text = "قائمة الدفوعـــات";
            this.btnPaymentsList.Click += new System.EventHandler(this.btnPaymentsList_Click);
            // 
            // btnRC
            // 
            this.btnRC.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRC.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.btnRC.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnRC.Name = "btnRC";
            this.btnRC.Text = "قائمة السجلات التجارية";
            this.btnRC.Click += new System.EventHandler(this.btnRC_Click);
            // 
            // metroTabPanel2
            // 
            this.metroTabPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.metroTabPanel2.Controls.Add(this.bar1);
            this.metroTabPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabPanel2.Location = new System.Drawing.Point(0, 62);
            this.metroTabPanel2.Name = "metroTabPanel2";
            this.metroTabPanel2.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.metroTabPanel2.Size = new System.Drawing.Size(998, 46);
            // 
            // 
            // 
            this.metroTabPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroTabPanel2.TabIndex = 2;
            this.metroTabPanel2.Visible = false;
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.MenuBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bar1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar1.IsMaximized = false;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSend,
            this.btnOperationList,
            this.buttonItem11});
            this.bar1.ItemSpacing = 5;
            this.bar1.Location = new System.Drawing.Point(3, 0);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(992, 43);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 0;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // btnSend
            // 
            this.btnSend.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSend.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.btnSend.Image = global::MTM.Properties.Resources.send;
            this.btnSend.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnSend.Name = "btnSend";
            this.btnSend.Text = "عملية خروج";
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnOperationList
            // 
            this.btnOperationList.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnOperationList.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.btnOperationList.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnOperationList.Name = "btnOperationList";
            this.btnOperationList.Symbol = "";
            this.btnOperationList.Text = "قائمة العمليات";
            this.btnOperationList.Click += new System.EventHandler(this.btnOperationList_Click);
            // 
            // buttonItem11
            // 
            this.buttonItem11.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem11.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.buttonItem11.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem11.Name = "buttonItem11";
            this.buttonItem11.Symbol = "";
            this.buttonItem11.Text = "قائمة العمليات";
            this.buttonItem11.Click += new System.EventHandler(this.buttonItem11_Click);
            // 
            // metroTabPanel7
            // 
            this.metroTabPanel7.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.metroTabPanel7.Controls.Add(this.bar7);
            this.metroTabPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabPanel7.Location = new System.Drawing.Point(0, 62);
            this.metroTabPanel7.Name = "metroTabPanel7";
            this.metroTabPanel7.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.metroTabPanel7.Size = new System.Drawing.Size(998, 46);
            // 
            // 
            // 
            this.metroTabPanel7.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel7.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel7.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroTabPanel7.TabIndex = 7;
            this.metroTabPanel7.Visible = false;
            // 
            // bar7
            // 
            this.bar7.AntiAlias = true;
            this.bar7.Dock = System.Windows.Forms.DockStyle.Top;
            this.bar7.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bar7.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar7.IsMaximized = false;
            this.bar7.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem6,
            this.buttonItem7});
            this.bar7.ItemSpacing = 5;
            this.bar7.Location = new System.Drawing.Point(3, 0);
            this.bar7.Name = "bar7";
            this.bar7.Size = new System.Drawing.Size(992, 33);
            this.bar7.Stretch = true;
            this.bar7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar7.TabIndex = 4;
            this.bar7.TabStop = false;
            this.bar7.Text = "bar7";
            // 
            // buttonItem6
            // 
            this.buttonItem6.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem6.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.buttonItem6.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem6.Name = "buttonItem6";
            this.buttonItem6.Symbol = "";
            this.buttonItem6.Text = "تسجيل عامل";
            this.buttonItem6.Click += new System.EventHandler(this.buttonItem6_Click);
            // 
            // buttonItem7
            // 
            this.buttonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem7.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.buttonItem7.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.Symbol = "";
            this.buttonItem7.Text = "قائمة العمال";
            this.buttonItem7.Click += new System.EventHandler(this.buttonItem7_Click);
            // 
            // metroTabPanel6
            // 
            this.metroTabPanel6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.metroTabPanel6.Controls.Add(this.bar6);
            this.metroTabPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabPanel6.Location = new System.Drawing.Point(0, 62);
            this.metroTabPanel6.Name = "metroTabPanel6";
            this.metroTabPanel6.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.metroTabPanel6.Size = new System.Drawing.Size(998, 46);
            // 
            // 
            // 
            this.metroTabPanel6.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel6.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel6.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroTabPanel6.TabIndex = 6;
            this.metroTabPanel6.Visible = false;
            // 
            // bar6
            // 
            this.bar6.AntiAlias = true;
            this.bar6.Dock = System.Windows.Forms.DockStyle.Top;
            this.bar6.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bar6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar6.IsMaximized = false;
            this.bar6.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem2,
            this.buttonItem4});
            this.bar6.ItemSpacing = 5;
            this.bar6.Location = new System.Drawing.Point(3, 0);
            this.bar6.Name = "bar6";
            this.bar6.Size = new System.Drawing.Size(992, 33);
            this.bar6.Stretch = true;
            this.bar6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar6.TabIndex = 4;
            this.bar6.TabStop = false;
            this.bar6.Text = "bar6";
            // 
            // buttonItem2
            // 
            this.buttonItem2.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem2.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.buttonItem2.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Symbol = "";
            this.buttonItem2.Text = "تسجيل مصاريف";
            this.buttonItem2.Click += new System.EventHandler(this.buttonItem2_Click);
            // 
            // buttonItem4
            // 
            this.buttonItem4.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem4.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.buttonItem4.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.Symbol = "";
            this.buttonItem4.Text = "قائمة المصاريف";
            this.buttonItem4.Click += new System.EventHandler(this.buttonItem4_Click);
            // 
            // metroTabPanel5
            // 
            this.metroTabPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.metroTabPanel5.Controls.Add(this.bar5);
            this.metroTabPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabPanel5.Location = new System.Drawing.Point(0, 62);
            this.metroTabPanel5.Name = "metroTabPanel5";
            this.metroTabPanel5.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.metroTabPanel5.Size = new System.Drawing.Size(998, 46);
            // 
            // 
            // 
            this.metroTabPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroTabPanel5.TabIndex = 5;
            this.metroTabPanel5.Visible = false;
            // 
            // bar5
            // 
            this.bar5.AntiAlias = true;
            this.bar5.Dock = System.Windows.Forms.DockStyle.Top;
            this.bar5.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bar5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar5.IsMaximized = false;
            this.bar5.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnAddDiscountYear,
            this.buttonItem3});
            this.bar5.ItemSpacing = 5;
            this.bar5.Location = new System.Drawing.Point(3, 0);
            this.bar5.Name = "bar5";
            this.bar5.Size = new System.Drawing.Size(992, 33);
            this.bar5.Stretch = true;
            this.bar5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar5.TabIndex = 3;
            this.bar5.TabStop = false;
            this.bar5.Text = "bar5";
            // 
            // btnAddDiscountYear
            // 
            this.btnAddDiscountYear.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAddDiscountYear.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.btnAddDiscountYear.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnAddDiscountYear.Name = "btnAddDiscountYear";
            this.btnAddDiscountYear.Symbol = "";
            this.btnAddDiscountYear.Text = "تسجيل إعدادات التخفيض السنوي";
            this.btnAddDiscountYear.Click += new System.EventHandler(this.btnAddDiscountYear_Click);
            // 
            // buttonItem3
            // 
            this.buttonItem3.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem3.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.buttonItem3.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.Symbol = "";
            this.buttonItem3.Text = "مراجعة التخفيضات";
            this.buttonItem3.Click += new System.EventHandler(this.buttonItem3_Click);
            // 
            // metroTabPanel8
            // 
            this.metroTabPanel8.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.metroTabPanel8.Controls.Add(this.bar8);
            this.metroTabPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabPanel8.Location = new System.Drawing.Point(0, 62);
            this.metroTabPanel8.Name = "metroTabPanel8";
            this.metroTabPanel8.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.metroTabPanel8.Size = new System.Drawing.Size(998, 46);
            // 
            // 
            // 
            this.metroTabPanel8.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel8.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel8.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroTabPanel8.TabIndex = 8;
            this.metroTabPanel8.Visible = false;
            // 
            // bar8
            // 
            this.bar8.AntiAlias = true;
            this.bar8.Dock = System.Windows.Forms.DockStyle.Top;
            this.bar8.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bar8.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar8.IsMaximized = false;
            this.bar8.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem9,
            this.buttonItem10});
            this.bar8.ItemSpacing = 5;
            this.bar8.Location = new System.Drawing.Point(3, 0);
            this.bar8.Name = "bar8";
            this.bar8.Size = new System.Drawing.Size(992, 33);
            this.bar8.Stretch = true;
            this.bar8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar8.TabIndex = 5;
            this.bar8.TabStop = false;
            this.bar8.Text = "bar8";
            // 
            // buttonItem9
            // 
            this.buttonItem9.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem9.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.buttonItem9.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem9.Name = "buttonItem9";
            this.buttonItem9.Symbol = "";
            // 
            // buttonItem10
            // 
            this.buttonItem10.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem10.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.buttonItem10.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem10.Name = "buttonItem10";
            this.buttonItem10.Symbol = "";
            // 
            // metroTabPanel3
            // 
            this.metroTabPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.metroTabPanel3.Controls.Add(this.bar3);
            this.metroTabPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabPanel3.Location = new System.Drawing.Point(0, 62);
            this.metroTabPanel3.Name = "metroTabPanel3";
            this.metroTabPanel3.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.metroTabPanel3.Size = new System.Drawing.Size(998, 46);
            // 
            // 
            // 
            this.metroTabPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroTabPanel3.TabIndex = 3;
            this.metroTabPanel3.Visible = false;
            // 
            // bar3
            // 
            this.bar3.AntiAlias = true;
            this.bar3.Dock = System.Windows.Forms.DockStyle.Top;
            this.bar3.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.bar3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar3.IsMaximized = false;
            this.bar3.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem5,
            this.buttonItem1});
            this.bar3.ItemSpacing = 5;
            this.bar3.Location = new System.Drawing.Point(3, 0);
            this.bar3.Name = "bar3";
            this.bar3.Size = new System.Drawing.Size(992, 43);
            this.bar3.Stretch = true;
            this.bar3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar3.TabIndex = 2;
            this.bar3.TabStop = false;
            this.bar3.Text = "bar3";
            // 
            // buttonItem5
            // 
            this.buttonItem5.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem5.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.buttonItem5.Image = global::MTM.Properties.Resources.products;
            this.buttonItem5.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem5.Name = "buttonItem5";
            this.buttonItem5.Text = "سلعة جديدة";
            this.buttonItem5.Click += new System.EventHandler(this.buttonItem5_Click);
            // 
            // buttonItem1
            // 
            this.buttonItem1.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.buttonItem1.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Symbol = "";
            this.buttonItem1.Text = "قائمة السلعة";
            this.buttonItem1.Click += new System.EventHandler(this.buttonItem1_Click);
            // 
            // metroTabItem8
            // 
            this.metroTabItem8.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.metroTabItem8.Name = "metroTabItem8";
            this.metroTabItem8.Panel = this.metroTabPanel8;
            this.metroTabItem8.Text = "الصندوق";
            this.metroTabItem8.Click += new System.EventHandler(this.metroTabItem8_Click);
            // 
            // metroTabItem7
            // 
            this.metroTabItem7.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.metroTabItem7.Name = "metroTabItem7";
            this.metroTabItem7.Panel = this.metroTabPanel7;
            this.metroTabItem7.Text = "العمال";
            // 
            // metroTabItem6
            // 
            this.metroTabItem6.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.metroTabItem6.Name = "metroTabItem6";
            this.metroTabItem6.Panel = this.metroTabPanel6;
            this.metroTabItem6.Text = "المصاريف";
            // 
            // metroTabItem5
            // 
            this.metroTabItem5.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.metroTabItem5.Name = "metroTabItem5";
            this.metroTabItem5.Panel = this.metroTabPanel5;
            this.metroTabItem5.Text = "التخفيضات";
            // 
            // metroTabItem4
            // 
            this.metroTabItem4.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.metroTabItem4.Name = "metroTabItem4";
            this.metroTabItem4.Panel = this.metroTabPanel4;
            this.metroTabItem4.Text = "عمليات التسليم";
            // 
            // metroTabItem2
            // 
            this.metroTabItem2.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.metroTabItem2.Name = "metroTabItem2";
            this.metroTabItem2.Panel = this.metroTabPanel2;
            this.metroTabItem2.Text = "عمليات النقل";
            // 
            // metroTabItem1
            // 
            this.metroTabItem1.Checked = true;
            this.metroTabItem1.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.metroTabItem1.Name = "metroTabItem1";
            this.metroTabItem1.Panel = this.metroTabPanel1;
            this.metroTabItem1.Text = "العملاء";
            // 
            // metroTabItem3
            // 
            this.metroTabItem3.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.metroTabItem3.Name = "metroTabItem3";
            this.metroTabItem3.Panel = this.metroTabPanel3;
            this.metroTabItem3.Text = "المنتجات";
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2016;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))), System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199))))));
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.DragDropSupport = true;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(1, 577);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(998, 21);
            this.metroStatusBar1.TabIndex = 1;
            this.metroStatusBar1.Text = "metroStatusBar1";
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "------";
            // 
            // superTabControl1
            // 
            this.superTabControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.MenuBox,
            this.superTabControl1.ControlBox.CloseBox});
            this.superTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(1, 109);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = -1;
            this.superTabControl1.Size = new System.Drawing.Size(998, 468);
            this.superTabControl1.TabFont = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.superTabControl1.TabIndex = 2;
            this.superTabControl1.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue;
            this.superTabControl1.Text = "superTabControl1";
            this.superTabControl1.SelectedTabChanged += new System.EventHandler<DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs>(this.superTabControl1_SelectedTabChanged);
            // 
            // contextMenuBar1
            // 
            this.contextMenuBar1.AntiAlias = true;
            this.contextMenuBar1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.contextMenuBar1.IsMaximized = false;
            this.contextMenuBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem12});
            this.contextMenuBar1.Location = new System.Drawing.Point(22, 30);
            this.contextMenuBar1.Name = "contextMenuBar1";
            this.contextMenuBar1.Size = new System.Drawing.Size(149, 27);
            this.contextMenuBar1.Stretch = true;
            this.contextMenuBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.contextMenuBar1.TabIndex = 1;
            this.contextMenuBar1.TabStop = false;
            this.contextMenuBar1.Text = "contextMenuBar1";
            // 
            // buttonItem12
            // 
            this.buttonItem12.AutoExpandOnClick = true;
            this.buttonItem12.Name = "buttonItem12";
            this.buttonItem12.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem13});
            this.buttonItem12.Text = "buttonItem12";
            // 
            // buttonItem13
            // 
            this.buttonItem13.Name = "buttonItem13";
            this.buttonItem13.Text = "migrate";
            this.buttonItem13.Click += new System.EventHandler(this.buttonItem13_Click);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1000, 599);
            this.Controls.Add(this.contextMenuBar1);
            this.Controls.Add(this.superTabControl1);
            this.Controls.Add(this.metroStatusBar1);
            this.Controls.Add(this.metroShell1);
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MTM";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.metroShell1.ResumeLayout(false);
            this.metroShell1.PerformLayout();
            this.metroTabPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar4)).EndInit();
            this.metroTabPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar2)).EndInit();
            this.metroTabPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.metroTabPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar7)).EndInit();
            this.metroTabPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar6)).EndInit();
            this.metroTabPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar5)).EndInit();
            this.metroTabPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar8)).EndInit();
            this.metroTabPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contextMenuBar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroShell metroShell1;
        private DevComponents.DotNetBar.Metro.MetroTabItem metroTabItem1;
        private DevComponents.DotNetBar.Metro.MetroTabItem metroTabItem2;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.Metro.MetroTabPanel metroTabPanel2;
        private DevComponents.DotNetBar.Metro.MetroTabPanel metroTabPanel1;
        private DevComponents.DotNetBar.Bar bar2;
        private DevComponents.DotNetBar.ButtonItem btnAddFactory;
        private DevComponents.DotNetBar.ButtonItem btnAddMerchant;
        private DevComponents.DotNetBar.ButtonItem btnWarehouse;
        private DevComponents.DotNetBar.ButtonItem btnPartnersList;
        private DevComponents.DotNetBar.ButtonItem btnPaymentsList;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.ButtonItem btnSend;
        private DevComponents.DotNetBar.Metro.MetroTabPanel metroTabPanel3;
        private DevComponents.DotNetBar.Bar bar3;
        private DevComponents.DotNetBar.ButtonItem buttonItem5;
        private DevComponents.DotNetBar.Metro.MetroTabItem metroTabItem3;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonItem btnAddPayments;
        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.ButtonItem btnOperationList;
        private DevComponents.DotNetBar.Metro.MetroTabPanel metroTabPanel4;
        private DevComponents.DotNetBar.Bar bar4;
        private DevComponents.DotNetBar.ButtonItem buttonItem8;
        private DevComponents.DotNetBar.Metro.MetroTabItem metroTabItem4;
        private DevComponents.DotNetBar.ButtonItem btnAddDeliverer;
        private DevComponents.DotNetBar.ButtonItem btnDelivererList;
        private DevComponents.DotNetBar.Metro.MetroTabPanel metroTabPanel5;
        private DevComponents.DotNetBar.Metro.MetroTabItem metroTabItem5;
        private DevComponents.DotNetBar.Bar bar5;
        private DevComponents.DotNetBar.ButtonItem btnAddDiscountYear;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.DotNetBar.Metro.MetroTabPanel metroTabPanel6;
        private DevComponents.DotNetBar.Bar bar6;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.Metro.MetroTabItem metroTabItem6;
        private DevComponents.DotNetBar.Metro.MetroTabPanel metroTabPanel7;
        private DevComponents.DotNetBar.Bar bar7;
        private DevComponents.DotNetBar.ButtonItem buttonItem6;
        private DevComponents.DotNetBar.ButtonItem buttonItem7;
        private DevComponents.DotNetBar.Metro.MetroTabItem metroTabItem7;
        private DevComponents.DotNetBar.Metro.MetroTabPanel metroTabPanel8;
        private DevComponents.DotNetBar.Metro.MetroTabItem metroTabItem8;
        private DevComponents.DotNetBar.Bar bar8;
        private DevComponents.DotNetBar.ButtonItem buttonItem9;
        private DevComponents.DotNetBar.ButtonItem buttonItem10;
        private DevComponents.DotNetBar.ButtonItem buttonItem11;
        private DevComponents.DotNetBar.ButtonItem btnRC;
        private DevComponents.DotNetBar.ContextMenuBar contextMenuBar1;
        private DevComponents.DotNetBar.ButtonItem buttonItem12;
        private DevComponents.DotNetBar.ButtonItem buttonItem13;

    }
}

