﻿namespace MTM.Workers
{
    partial class AddEditWorkerDebit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.priceDoubleInput = new DevComponents.Editors.DoubleInput();
            this.workers_paymentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gama_transportationdbDataSet = new MTM.gama_transportationdbDataSet();
            this.datDateTimeInput = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.workers_paymentsTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.workers_paymentsTableAdapter();
            this.tableAdapterManager = new MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager();
            this.noteTextBoxX = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.comboBoxEx1 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.partnersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.partnersTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.partnersTableAdapter();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.priceDoubleInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workers_paymentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datDateTimeInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.partnersBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(12, 151);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(114, 32);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.Symbol = "";
            this.btnCancel.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "إلغاء الأمر";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSave.Location = new System.Drawing.Point(132, 151);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(114, 32);
            this.btnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSave.Symbol = "";
            this.btnSave.SymbolColor = System.Drawing.Color.Green;
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "حفظ";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(345, 12);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(81, 27);
            this.labelX1.TabIndex = 4;
            this.labelX1.Text = "مبلغ الأجرة:";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // priceDoubleInput
            // 
            this.priceDoubleInput.AllowEmptyState = false;
            this.priceDoubleInput.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.priceDoubleInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.priceDoubleInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.priceDoubleInput.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.priceDoubleInput.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.workers_paymentsBindingSource, "Amount", true));
            this.priceDoubleInput.FocusHighlightEnabled = true;
            this.priceDoubleInput.ForeColor = System.Drawing.Color.Black;
            this.priceDoubleInput.Increment = 1D;
            this.priceDoubleInput.Location = new System.Drawing.Point(182, 12);
            this.priceDoubleInput.MaxValue = 99999999D;
            this.priceDoubleInput.MinValue = -99999999D;
            this.priceDoubleInput.Name = "priceDoubleInput";
            this.priceDoubleInput.Size = new System.Drawing.Size(157, 27);
            this.priceDoubleInput.TabIndex = 0;
            // 
            // workers_paymentsBindingSource
            // 
            this.workers_paymentsBindingSource.DataMember = "workers_payments";
            this.workers_paymentsBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // gama_transportationdbDataSet
            // 
            this.gama_transportationdbDataSet.DataSetName = "gama_transportationdbDataSet";
            this.gama_transportationdbDataSet.EnforceConstraints = false;
            this.gama_transportationdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // datDateTimeInput
            // 
            this.datDateTimeInput.AllowEmptyState = false;
            this.datDateTimeInput.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.datDateTimeInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.datDateTimeInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.datDateTimeInput.ButtonDropDown.Visible = true;
            this.datDateTimeInput.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.workers_paymentsBindingSource, "dat", true));
            this.datDateTimeInput.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.workers_paymentsBindingSource, "dat", true));
            this.datDateTimeInput.FocusHighlightEnabled = true;
            this.datDateTimeInput.ForeColor = System.Drawing.Color.Black;
            this.datDateTimeInput.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center;
            this.datDateTimeInput.IsPopupCalendarOpen = false;
            this.datDateTimeInput.Location = new System.Drawing.Point(182, 45);
            // 
            // 
            // 
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.datDateTimeInput.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.DisplayMonth = new System.DateTime(2019, 2, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.TodayButtonVisible = true;
            this.datDateTimeInput.Name = "datDateTimeInput";
            this.datDateTimeInput.Size = new System.Drawing.Size(157, 27);
            this.datDateTimeInput.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.datDateTimeInput.TabIndex = 1;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(345, 45);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(81, 27);
            this.labelX2.TabIndex = 5;
            this.labelX2.Text = "التاريـــــــخ:";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // workers_paymentsTableAdapter
            // 
            this.workers_paymentsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.deliverersTableAdapter = null;
            this.tableAdapterManager.discountlinesTableAdapter = null;
            this.tableAdapterManager.discountsTableAdapter = null;
            this.tableAdapterManager.expensesTableAdapter = null;
            this.tableAdapterManager.locationsTableAdapter = null;
            this.tableAdapterManager.operationsTableAdapter = null;
            this.tableAdapterManager.package_delivrer_orderTableAdapter = null;
            this.tableAdapterManager.package_order_expensesTableAdapter = null;
            this.tableAdapterManager.package_order_payments1TableAdapter = null;
            this.tableAdapterManager.package_order_paymentsTableAdapter = null;
            this.tableAdapterManager.packages_order_line_delivererTableAdapter = null;
            this.tableAdapterManager.packages_order_linesTableAdapter = null;
            this.tableAdapterManager.packages_orderTableAdapter = null;
            this.tableAdapterManager.partners_paymentsTableAdapter = null;
            this.tableAdapterManager.partnersTableAdapter = null;
            this.tableAdapterManager.productsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.workers_absenceTableAdapter = null;
            this.tableAdapterManager.workers_accountTableAdapter = null;
            this.tableAdapterManager.workers_paymentsTableAdapter = this.workers_paymentsTableAdapter;
            this.tableAdapterManager.workersTableAdapter = null;
            // 
            // noteTextBoxX
            // 
            this.noteTextBoxX.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.noteTextBoxX.Border.Class = "TextBoxBorder";
            this.noteTextBoxX.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.noteTextBoxX.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.workers_paymentsBindingSource, "Note", true));
            this.noteTextBoxX.DisabledBackColor = System.Drawing.Color.White;
            this.noteTextBoxX.ForeColor = System.Drawing.Color.Black;
            this.noteTextBoxX.Location = new System.Drawing.Point(12, 78);
            this.noteTextBoxX.Name = "noteTextBoxX";
            this.noteTextBoxX.PreventEnterBeep = true;
            this.noteTextBoxX.Size = new System.Drawing.Size(327, 27);
            this.noteTextBoxX.TabIndex = 7;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(345, 78);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(81, 27);
            this.labelX3.TabIndex = 5;
            this.labelX3.Text = "ملاحظة:";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // comboBoxEx1
            // 
            this.comboBoxEx1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.workers_paymentsBindingSource, "CutFromDepotID", true));
            this.comboBoxEx1.DataSource = this.partnersBindingSource;
            this.comboBoxEx1.DisplayMember = "CustomerName";
            this.comboBoxEx1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxEx1.ForeColor = System.Drawing.Color.Black;
            this.comboBoxEx1.FormattingEnabled = true;
            this.comboBoxEx1.ItemHeight = 21;
            this.comboBoxEx1.Location = new System.Drawing.Point(51, 111);
            this.comboBoxEx1.Name = "comboBoxEx1";
            this.comboBoxEx1.Size = new System.Drawing.Size(288, 27);
            this.comboBoxEx1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.comboBoxEx1.TabIndex = 8;
            this.comboBoxEx1.ValueMember = "ID";
            // 
            // partnersBindingSource
            // 
            this.partnersBindingSource.DataMember = "partners";
            this.partnersBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // partnersTableAdapter
            // 
            this.partnersTableAdapter.ClearBeforeFill = true;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(345, 111);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(81, 27);
            this.labelX4.TabIndex = 5;
            this.labelX4.Text = "المخزن:";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(12, 111);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(33, 27);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonX1.TabIndex = 9;
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // AddEditWorkerDebit
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(438, 195);
            this.ControlBox = false;
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.comboBoxEx1);
            this.Controls.Add(this.noteTextBoxX);
            this.Controls.Add(this.datDateTimeInput);
            this.Controls.Add(this.priceDoubleInput);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "AddEditWorkerDebit";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تسجيل أجرة";
            this.Load += new System.EventHandler(this.AddEditProduct_Load);
            ((System.ComponentModel.ISupportInitialize)(this.priceDoubleInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workers_paymentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datDateTimeInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.partnersBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.Editors.DoubleInput priceDoubleInput;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput datDateTimeInput;
        private DevComponents.DotNetBar.LabelX labelX2;
        private gama_transportationdbDataSet gama_transportationdbDataSet;
        private System.Windows.Forms.BindingSource workers_paymentsBindingSource;
        private gama_transportationdbDataSetTableAdapters.workers_paymentsTableAdapter workers_paymentsTableAdapter;
        private gama_transportationdbDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private DevComponents.DotNetBar.Controls.TextBoxX noteTextBoxX;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx comboBoxEx1;
        private System.Windows.Forms.BindingSource partnersBindingSource;
        private gama_transportationdbDataSetTableAdapters.partnersTableAdapter partnersTableAdapter;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.ButtonX buttonX1;
    }
}