using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Workers
{
    public partial class AddEditABS : DevComponents.DotNetBar.Metro.MetroForm
    {
        int ID = 0,ID_Worker;
        public double Salerie { get; set; }
        public AddEditABS(int Id_, int workerid)
        {
            InitializeComponent();
            ID = Id_; ID_Worker = workerid;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void AddEditProduct_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.workers_absence' table. You can move, or remove it, as needed.
            if (ID == 0)
            {
                this.workers_absenceBindingSource.AddNew();
                ((DataRowView)workers_absenceBindingSource.Current)["workers_ID"] = ID_Worker;
                DaysdoubleInput1.Value = 1;
                AmountDoubleInput.Value = (Salerie / 30) * DaysdoubleInput1.Value;
            }
            else
            {
                this.workers_absenceTableAdapter.FillByID(this.gama_transportationdbDataSet.workers_absence,ID);
            }

            DaysdoubleInput1.ValueChanged += (ec, ds) =>
            {
                AmountDoubleInput.Value = (Salerie / 30) * DaysdoubleInput1.Value;
            };
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.workers_absenceBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(gama_transportationdbDataSet);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }
    }
}