using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Microsoft.Reporting.WinForms;

namespace MTM.Workers.Reports
{
    public partial class WorkerReportForm : DevComponents.DotNetBar.Metro.MetroForm
    {
        public WorkerReportForm()
        {
            InitializeComponent();
            this.FormClosing += (se, ee) =>
            {
                this.reportViewer1.LocalReport.ReleaseSandboxAppDomain();
            };
        }

        private void PrintReportForm_Load(object sender, EventArgs e)
        {
            dateTimeInput1.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            workersStatementTableAdapter.Fill(viewDataSet.WorkersStatement, d1, d2, WorkerID);
            workersTableAdapter.FillByID(gama_transportationdbDataSet.workers, WorkerID);
            ReportParameter rp1, rp2, rp3, rp4;
            SSUtilities.SSConfigFile cfg = new SSUtilities.SSConfigFile();
            rp1 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName, ""));
            rp2 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity, ""));
            rp3 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1, ""));
            rp4 = new ReportParameter(SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, ""));
            var rp5 = new ReportParameter("OldSolde", (0.00M).ToString());
            reportViewer1.LocalReport.SetParameters(new ReportParameter[] { rp1, rp2, rp3, rp4, rp5 });
            this.reportViewer1.RefreshReport();
        }

        public DateTime d1 { get { return dateTimeInput1.Value.Date; } }

        public DateTime d2 { get { return dateTimeInput2.Value.Date.AddDays(1); } }

        public int WorkerID { get; set; }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            decimal balance = 0;

            workersStatementTableAdapter.Fill(viewDataSet.WorkersStatement, dateTimeInput1.Value.Date, dateTimeInput2.Value.Date.AddDays(1), WorkerID);
            balance = workersStatementTableAdapter.GetBalanceAt(dateTimeInput1.Value.Date, WorkerID).GetValueOrDefault(0);
            
            //viewDataSet.partners.FirstOrDefault().Balance;
            var rp5 = new ReportParameter("OldSolde", balance.ToString("N2"));
            var rp6 = new ReportParameter("dat1", string.Format("{0} ��� {1}", dateTimeInput1.Value.ToString("d"), dateTimeInput2.Value.ToString("d")));
            reportViewer1.LocalReport.SetParameters(new ReportParameter[] { rp5, rp6 });

            this.reportViewer1.RefreshReport();
        }
    }
}