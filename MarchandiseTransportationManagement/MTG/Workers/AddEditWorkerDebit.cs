using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Workers
{
    public partial class AddEditWorkerDebit : DevComponents.DotNetBar.Metro.MetroForm
    {
        int ID = 0,ID_Worker;

        public AddEditWorkerDebit(int Id_, int workerid)
        {
            InitializeComponent();
            ID = Id_; ID_Worker = workerid;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void AddEditProduct_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.partners' table. You can move, or remove it, as needed.
            this.partnersTableAdapter.FillByDepot(this.gama_transportationdbDataSet.partners);
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.workers_payments' table. You can move, or remove it, as needed.
            if (ID == 0)
            {
                this.workers_paymentsBindingSource.AddNew();
                ((DataRowView)workers_paymentsBindingSource.Current)["workers_ID"] = ID_Worker;
            }
            else
            {
                this.workers_paymentsTableAdapter.FillByID(this.gama_transportationdbDataSet.workers_payments,ID);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.workers_paymentsBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.gama_transportationdbDataSet);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            ((DataRowView)workers_paymentsBindingSource.Current)["CutFromDepotID"] = DBNull.Value;
            comboBoxEx1.SelectedIndex = -1;
        }
    }
}