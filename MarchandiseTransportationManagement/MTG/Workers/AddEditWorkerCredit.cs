using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Workers
{
    public partial class AddEditWorkerCredit : DevComponents.DotNetBar.Metro.MetroForm
    {
        int ID = 0,ID_Worker;

        public AddEditWorkerCredit(int Id_,int workerid)
        {
            InitializeComponent();
            ID = Id_; ID_Worker = workerid;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void AddEditProduct_Load(object sender, EventArgs e)
        {
            if (ID == 0)
            {
                this.workers_accountBindingSource.AddNew();
                ((DataRowView)workers_accountBindingSource.Current)["workers_ID"] = ID_Worker;
            }
            else
            {
                this.workers_accountTableAdapter.FillByID(this.gama_transportationdbDataSet.workers_account,ID);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.workers_accountBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(gama_transportationdbDataSet);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }
    }
}