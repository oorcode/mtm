using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Workers
{
    public partial class AddEditWorker : DevComponents.DotNetBar.Metro.MetroForm
    {
        int ID = 0;

        public AddEditWorker(int Id_)
        {
            InitializeComponent();
            ID = Id_;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void AddEditProduct_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.expenses' table. You can move, or remove it, as needed.
            if (ID == 0)
            {
                workersBindingSource.AddNew();
            }
            else
            {
                this.workersTableAdapter.FillByID(this.gama_transportationdbDataSet.workers,ID);
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.workersBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(gama_transportationdbDataSet);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }
    }
}