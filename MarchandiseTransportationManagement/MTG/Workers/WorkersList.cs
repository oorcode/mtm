using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Workers
{
    public partial class WorkersList : UserControl,IIUserControl
    {
        public WorkersList()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            new Workers.AddEditWorker(0).ShowDialog();
            int pos = workersBindingSource.Position;
            btnFillByDate.PerformClick();
            workersBindingSource.Position = pos;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (workersBindingSource.Position < 0) return;
            if (new AddEditWorker(Convert.ToInt32(((DataRowView)workersBindingSource.Current)["ID"])).ShowDialog() == DialogResult.Cancel) return;
            int pos = workersBindingSource.Position;
            btnFillByDate.PerformClick();
            workersBindingSource.Position = pos;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (workersBindingSource.Position < 0) return;
            try
            {
                if (MessageBox.Show("����� ����� ��������", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
                ((DataRowView)workersBindingSource.Current)["deleted"] = 1;
                this.Validate();
                this.workersBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(gama_transportationdbDataSet);
                int pos = workersBindingSource.Position;
                btnFillByDate.PerformClick();
                workersBindingSource.Position = pos;
            }
            catch (Exception)
            {
            }
        }

        private void PartnersList_Load(object sender, EventArgs e)
        {
            DateTime dat = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            if (!buttonX1.Checked)
            {
                this.workers_accountTableAdapter.Fill(gama_transportationdbDataSet.workers_account);
                this.workers_absenceTableAdapter.Fill(gama_transportationdbDataSet.workers_absence);
                this.workers_paymentsTableAdapter.Fill(gama_transportationdbDataSet.workers_payments);
                this.workersTableAdapter.Fill(gama_transportationdbDataSet.workers, dat.ToString("yyyy-MM-dd"));
            }
            else
            {
                this.workers_accountTableAdapter.FillByAll(gama_transportationdbDataSet.workers_account);
                this.workers_absenceTableAdapter.FillByAll(gama_transportationdbDataSet.workers_absence);
                this.workers_paymentsTableAdapter.FillByAll(gama_transportationdbDataSet.workers_payments);
                this.workersTableAdapter.FillByAll(gama_transportationdbDataSet.workers, dat.ToString("yyyy-MM-dd"));
            }
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            workersBindingSource.Filter = "WorkerName LIKE '%" + SearchBox.Text + "%'";
        }

        private void btnAddP_Click(object sender, EventArgs e)
        {
            if (workersBindingSource.Position < 0) return;
            new AddEditWorkerCredit(0, Convert.ToInt32(((DataRowView)workersBindingSource.Current)["ID"])).ShowDialog();
            this.workers_accountTableAdapter.Fill(gama_transportationdbDataSet.workers_account);
            int pos = workersBindingSource.Position;
            btnFillByDate.PerformClick();
            workersBindingSource.Position = pos;
        }
        private void btnEditAccount_Click(object sender, EventArgs e)
        {
            if (workers_accountBindingSource.Position < 0) return;
            if (Convert.ToInt32(((DataRowView)workers_accountBindingSource.Current)["IsActive"]) == 0) return;
            new AddEditWorkerCredit(Convert.ToInt32(((DataRowView)workers_accountBindingSource.Current)["ID"]), Convert.ToInt32(((DataRowView)workersBindingSource.Current)["ID"])).ShowDialog();
            this.workers_accountTableAdapter.Fill(gama_transportationdbDataSet.workers_account);
            int pos = workersBindingSource.Position;
            btnFillByDate.PerformClick();
            workersBindingSource.Position = pos;
        }
        private void btnDeleteP_Click(object sender, EventArgs e)
        {
            if (workers_accountBindingSource.Position < 0) return;
            if (Convert.ToInt32(((DataRowView)workers_accountBindingSource.Current)["IsActive"]) == 0) return;
            if (MessageBox.Show("����� ����� ��������", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
            int Id = Convert.ToInt32(((DataRowView)workers_accountBindingSource.Current)["ID"]);
            workers_accountTableAdapter.UpdateDeleted(true, Id);
            workers_accountBindingSource.RemoveCurrent();
            int pos = workersBindingSource.Position;
            btnFillByDate.PerformClick();
            workersBindingSource.Position = pos;
        }

        private void btnAddD_Click(object sender, EventArgs e)
        {
            if (workersBindingSource.Position < 0) return;
            new AddEditWorkerDebit(0, Convert.ToInt32(((DataRowView)workersBindingSource.Current)["ID"])).ShowDialog();
            this.workers_paymentsTableAdapter.Fill(gama_transportationdbDataSet.workers_payments);
            int pos = workersBindingSource.Position;
            btnFillByDate.PerformClick();
            workersBindingSource.Position = pos;
        }
        private void btnEditPayment_Click(object sender, EventArgs e)
        {
            if (workers_paymentsBindingSource.Position < 0) return;
            if (Convert.ToInt32(((DataRowView)workers_paymentsBindingSource.Current)["IsActive"]) == 0) return;
            if (!Convert.ToBoolean(((DataRowView)workers_paymentsBindingSource.Current)["IsActive"])) return;
            new AddEditWorkerDebit(Convert.ToInt32(((DataRowView)workers_paymentsBindingSource.Current)["ID"]), Convert.ToInt32(((DataRowView)workersBindingSource.Current)["ID"])).ShowDialog();
            this.workers_paymentsTableAdapter.Fill(gama_transportationdbDataSet.workers_payments);
            int pos = workersBindingSource.Position;
            btnFillByDate.PerformClick();
            workersBindingSource.Position = pos;
        }
        private void btnDeleteD_Click(object sender, EventArgs e)
        {
            if (workers_paymentsBindingSource.Position< 0) return;
            if (Convert.ToInt32(((DataRowView)workers_paymentsBindingSource.Current)["IsActive"]) == 0) return;
            if (MessageBox.Show("����� ����� ��������", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
            int Id = Convert.ToInt32(((DataRowView)workers_paymentsBindingSource.Current)["ID"]);
            workers_paymentsTableAdapter.UpdateDeleted(true, Id);
            workers_paymentsBindingSource.RemoveCurrent();
            int pos = workersBindingSource.Position;
            btnFillByDate.PerformClick();
            workersBindingSource.Position = pos;
        }

        private void btnAddABS_Click(object sender, EventArgs e)
        {
            if (workersBindingSource.Position < 0) return;
            new AddEditABS(0, Convert.ToInt32(((DataRowView)workersBindingSource.Current)["ID"])) { Salerie = Convert.ToDouble(((DataRowView)workersBindingSource.Current)["Salaire"]) }.ShowDialog();
            this.workers_absenceTableAdapter.Fill(gama_transportationdbDataSet.workers_absence);

            int pos = workersBindingSource.Position;
            btnFillByDate.PerformClick();
            workersBindingSource.Position = pos;
        }
        private void btnEditABS_Click(object sender, EventArgs e)
        {
            if (workers_absenceBindingSource.Position < 0) return;
            if (Convert.ToInt32(((DataRowView)workers_absenceBindingSource.Current)["IsActive"]) == 0) return;
            int Id = Convert.ToInt32(((DataRowView)workers_absenceBindingSource.Current)["ID"]);
            new AddEditABS(Id, Convert.ToInt32(((DataRowView)workersBindingSource.Current)["ID"])).ShowDialog();
            this.workers_absenceTableAdapter.Fill(gama_transportationdbDataSet.workers_absence);
            int pos = workersBindingSource.Position;
            btnFillByDate.PerformClick();
            workersBindingSource.Position = pos;
        }
        private void btnDeleteABS_Click(object sender, EventArgs e)
        {
            if (workers_absenceBindingSource.Position < 0) return;
            if (Convert.ToInt32(((DataRowView)workers_absenceBindingSource.Current)["IsActive"]) == 0) return;
            if (MessageBox.Show("����� ����� ��������", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
            int Id = Convert.ToInt32(((DataRowView)workers_absenceBindingSource.Current)["ID"]);
            workers_absenceTableAdapter.UpdateDeleted(true, Id);
            workers_absenceBindingSource.RemoveCurrent();
            int pos = workersBindingSource.Position;
            btnFillByDate.PerformClick();
            workersBindingSource.Position = pos;
        }

        private void btnFillByDate_Click(object sender, EventArgs e)
        {
            DateTime dat = new DateTime(DateTime.Now.Year,DateTime.Now.Month,1);
            this.workersTableAdapter.Fill(gama_transportationdbDataSet.workers, dat.ToString("yyyy-MM-dd"));
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (workersBindingSource.Position < 0) return;
            new Reports.WorkerReportForm() { WorkerID = Convert.ToInt32(((DataRowView)workersBindingSource.Current)["ID"]) }.ShowDialog();
        }

        public void Refill()
        {
            PartnersList_Load(null, null);
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (buttonX1.Checked)
            {
                buttonX1.Checked = false;
            }
            else
            {
                buttonX1.Checked = true;
            }
            Refill();
        }
       
        private void workers_accountBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            foreach (DataGridViewRow row in dataGridViewX1.Rows)
            {
                if (Convert.ToInt32(row.Cells[IsActiveColumn.Name].Value) == 0)
                {
                    row.DefaultCellStyle.BackColor = Color.OrangeRed;
                }
            }
        }

        private void workers_paymentsBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            foreach (DataGridViewRow row in dataGridViewX2.Rows)
            {
                if (Convert.ToInt32(row.Cells[IsActiveColumn1.Name].Value) == 0)
                {
                    row.DefaultCellStyle.BackColor = Color.OrangeRed;
                }
            }
        }

        private void workers_absenceBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            foreach (DataGridViewRow row in dataGridViewX3.Rows)
            {
                if (Convert.ToInt32(row.Cells[IsActiveColumn2.Name].Value) == 0)
                {
                    row.DefaultCellStyle.BackColor = Color.OrangeRed;
                }
            }
        }

        private void workersBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            foreach (DataGridViewRow row in mainDataGridViewX.Rows)
            {
                if (Convert.ToInt32(row.Cells[deletedDataGridViewCheckBoxColumn.Name].Value) == 1)
                {
                    row.DefaultCellStyle.BackColor = Color.OrangeRed;
                }
            }
        }
    }
}