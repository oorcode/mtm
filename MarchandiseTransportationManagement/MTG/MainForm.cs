using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM
{
    public partial class MainForm : DevComponents.DotNetBar.Metro.MetroAppForm
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            BackgroundWorker bg = new BackgroundWorker();
            bg.DoWork += (fd, rt) =>
            {
                DB();
                try
                {
                    var date = DateTime.Now.AddMonths(-1);
                    var dataw = new gama_transportationdbDataSetTableAdapters.WorkersCheckerTableAdapter().GetData(
                       Convert.ToDecimal(date.Month), Convert.ToDecimal(date.Year));
                    date = date.AddDays(-date.Day + 1).AddMonths(1).Date.AddSeconds(-1);
                    var adaptAccoun = new gama_transportationdbDataSetTableAdapters.workers_accountTableAdapter();
                    adaptAccoun.UpdateIsActive0(date);
                   
                    new gama_transportationdbDataSetTableAdapters.workers_paymentsTableAdapter().UpdateIsActive0(date);
                    new gama_transportationdbDataSetTableAdapters.workers_absenceTableAdapter().UpdateIsActive0(date);
                    foreach (var item in dataw)
                    {
                        var salerie = item.Salaire;
                        if (item.BeginWorkingAt.Month == date.Month && item.BeginWorkingAt.Year == date.Year)
                        {
                            salerie = (item.Salaire / 30) * (date.Day - item.BeginWorkingAt.Day);
                        }
                        adaptAccoun.InsertQuery(salerie, item.ID, 0, date);
                    }

                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            };
            bg.RunWorkerAsync();

            
 
            try
            {
                Helper.ReceiversSource.ChargeSource();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            try
            {
                Helper.ReceiversPhonesSource.ChargeSource();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void btnAddFactory_Click(object sender, EventArgs e)
        {
            new Partners.Factory.AddEditFactory(0).ShowDialog(this);
        }

        private void btnAddMerchant_Click(object sender, EventArgs e)
        {
            new Partners.Merchant.AddEditMerchant2(0).ShowDialog(this);
        }

        private void btnWarehouse_Click(object sender, EventArgs e)
        {
            new Partners.Warehouse.AddEditWarehouse(0).ShowDialog(this);
        }

        private void btnPartnersList_Click(object sender, EventArgs e)
        {
            AddTab(new Partners.PartnersList(), "����� �������");
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            var IdOperation = 0;
            var operationView = new Operations.AddEditOperationLeaving();
            if (operationView.ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
            if (operationView.OperationID > 0)
            {
                IdOperation = operationView.OperationID;
            }
            else
            {
                IdOperation = new gama_transportationdbDataSetTableAdapters.operationsTableAdapter().MaxID().GetValueOrDefault(0);
            }
            var view = new Operations.AddEditOperationPackageList();
            view.OperationID = IdOperation;
            AddTab(view, "����� ����: " + IdOperation);
        }

        private void btnReceive_Click(object sender, EventArgs e)
        {
            var IdOperation = 0;
            var operationView = new Operations.AddEditOperationArriving();
            if (operationView.ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
            if (operationView.OperationID > 0)
            {
                IdOperation = operationView.OperationID;
            }
            else
            {
                IdOperation = new gama_transportationdbDataSetTableAdapters.operationsTableAdapter().MaxID().GetValueOrDefault(0);
            }
            var view = new Operations.AddEditOperationPackageList();
            view.OperationID = IdOperation;
            AddTab(view, "����� ����: " + IdOperation);
        }

        private void buttonItem5_Click(object sender, EventArgs e)
        {
            new Products.AddEditProduct(0, 0).ShowDialog(this);
        }

        private void buttonItem1_Click(object sender, EventArgs e)
        {
            AddTab(new Products.ProductsList(), "����� �����");
        }

        private void btnPaymentsList_Click(object sender, EventArgs e)
        {
            AddTab(new Payments.PaymentsList(), "����� �������");
        }

        private void btnAddPayments_Click(object sender, EventArgs e)
        {
            var select = new Partners.SelectPartner();
            select.IsFactory = false;
            select.IsMerchant = false;
            select.IsWarehouse = false;
            if (select.ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
           var ppppp = new ViewDataSetTableAdapters.partnersTableAdapter().GetDataBy(select.IsFactory, select.IsMerchant, select.SelectedPartnerID).FindByID(select.SelectedPartnerID);
         decimal d =0;
            if (ppppp != null)
           {
            d=   ppppp.Balance;
           }
           new Payments.AddEditPayment(0, select.SelectedPartnerID) { StartUpAmount=Convert.ToDouble( d)}.ShowDialog(this);
        }

        #region SuperTAB

        public bool FindTab(string Title)
        {
            foreach (SuperTabItem iii in superTabControl1.Tabs)
            {
                if (iii.Text == Title)
                {
                    superTabControl1.SelectedTab = iii;
                    iii.AttachedControl.Controls[0].Focus();
                    return true;
                }
            }
            return false;
        }

        public IList<T> GetAllControlsRecusrvive<T>(Control control) where T : Control
        {
            var rtn = new List<T>();
            foreach (Control item in control.Controls)
            {
                var ctr = item as T;
                if (ctr != null)
                {
                    rtn.Add(ctr);
                }
                else
                {
                    rtn.AddRange(GetAllControlsRecusrvive<T>(item));
                }

            }
            return rtn;
        }

        public void AddTab(UserControl ctrl, string Title)
        {
            try
            {
                if (FindTab(Title)) return;
                SuperTabControlPanel panel = new SuperTabControlPanel();
                SuperTabItem tab = new SuperTabItem() { Text = Title };
                AddQuitEvent(ctrl);

                panel.AutoScroll = true;
                attachTab(panel, tab);
                panel.Controls.Add(ctrl);
                ctrl.Dock = DockStyle.Fill;
                ctrl.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private bool AddQuitEvent(UserControl ctrl)
        {
            try
            {
                ctrl.Controls.Find("btnExit", true)[0].Click += (se, ee) =>
                {
                    superTabControl1.SelectedTab.Close();
                };
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void attachTab(SuperTabControlPanel stcl, SuperTabItem sti)
        {
            try
            {
                sti.AttachedControl = stcl;
                stcl.TabItem = sti;
                stcl.Dock = DockStyle.Fill;
                stcl.AutoScroll = true;
                stcl.Style.BackColor = Color.White;
                superTabControl1.Tabs.Add(sti);
                superTabControl1.Controls.Add(stcl);
                superTabControl1.SelectedTab = sti;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void CloseSelectedTab()
        {
            try
            {
                superTabControl1.SelectedTab.Close();
            }
            catch (Exception)
            {
            }
        }

        #endregion

        private void btnOperationList_Click(object sender, EventArgs e)
        {
            AddTab(new Operations.OperationsList(), "����� ��������");
        }

        private void buttonItem8_Click(object sender, EventArgs e)
        {
            AddTab(new Deliverer.DeliveryOrdersList(), "����� ���� �������");
        }

        private void btnAddDeliverer_Click(object sender, EventArgs e)
        {
            new Deliverer.AddEditDeliverer(0).ShowDialog(this);
        }

        private void metroShell1_SettingsButtonClick(object sender, EventArgs e)
        {
            new OwnerInfo.CompanyInfoForm().ShowDialog(this);
        }

        private void btnDelivererList_Click(object sender, EventArgs e)
        {
            AddTab(new Deliverer.DeliverersList(), "����� ��������");
        }

        private void btnAddDiscountYear_Click(object sender, EventArgs e)
        {
            new Discount.AddEditDiscountYear(0).ShowDialog(this);
        }

        private void buttonItem3_Click(object sender, EventArgs e)
        {
            AddTab(new Discount.DiscountsList(), "������ ���������");
        }

        #region SQL
        private void DB()
        {
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_expenses` ADD COLUMN `packages_order_YearID` INT NOT NULL;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_payments` ADD COLUMN `packages_order_YearID` INT NOT NULL;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`packages_order_line_deliverer` DROP FOREIGN KEY `fk_packages_order_line_deliverer_packages_order_lines1`;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`packages_order_line_deliverer` DROP INDEX `fk_packages_order_line_deliverer_packages_order_lines1_idx` ;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` DROP FOREIGN KEY `fk_package_delivrer_order_packages_order1`;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` DROP INDEX `fk_package_delivrer_order_packages_order1_idx` ; ", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_payments` DROP FOREIGN KEY `fk_package_order_payments_packages_order1`;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_payments` DROP INDEX `fk_package_order_payments_packages_order1_idx` ; ", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_payments` ADD INDEX `deliverers_Payment_idx` (`packages_order_ID` ASC, `packages_order_YearID` ASC);
        ALTER TABLE `gama_transportationdb`.`package_order_payments` ADD CONSTRAINT `deliverers_Payment` FOREIGN KEY (`packages_order_ID` , `packages_order_YearID`)  REFERENCES `gama_transportationdb`.`package_delivrer_order` (`ID` , `SerialYear`) 
          ON DELETE NO ACTION
          ON UPDATE NO ACTION;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_expenses` DROP FOREIGN KEY `fk_package_order_others_packages_order1`;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_expenses` DROP INDEX `fk_package_order_others_packages_order1_idx` ;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_expenses` ADD INDEX `PackageOrder_Expenses_idx` (`packages_order_ID` ASC, `packages_order_YearID` ASC);", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_expenses` ADD CONSTRAINT `PackageOrder_Expenses`
          FOREIGN KEY (`packages_order_ID` , `packages_order_YearID`)
          REFERENCES `gama_transportationdb`.`package_delivrer_order` (`ID` , `SerialYear`)
          ON DELETE RESTRICT
          ON UPDATE NO ACTION;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` 
ADD COLUMN `partners_ID` INT NOT NULL AFTER `Total`,
ADD INDEX `partners_orders_idx` (`partners_ID` ASC);", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` 
ADD CONSTRAINT `partners_orders`
  FOREIGN KEY (`partners_ID`)
  REFERENCES `gama_transportationdb`.`partners` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` CHANGE COLUMN `packages_order_ID` `packages_order_ID` INT(11) NULL ;",
                Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@" ALTER TABLE `gama_transportationdb`.`package_delivrer_order` 
ADD COLUMN `partners_ID` INT NOT NULL AFTER `Total`,
ADD INDEX `fk_order_partners_idx` (`partners_ID` ASC);",
                Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` 
ADD CONSTRAINT `fk_order_partners`
  FOREIGN KEY (`partners_ID`)
  REFERENCES `gama_transportationdb`.`partners` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` DROP COLUMN `packages_order_ID`;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`packages_order_line_deliverer` ADD COLUMN `payed` TINYINT(1) NOT NULL DEFAULT 0;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`packages_order_line_deliverer` DROP COLUMN `packages_order_lines_ID`;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`packages_order_line_deliverer` ADD COLUMN `Designation` TINYTEXT NOT NULL;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` ADD COLUMN `depotID` INT NOT NULL DEFAULT 0;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` ADD COLUMN `IsPayedInDepot` TINYINT(1) NOT NULL DEFAULT 0;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` ADD COLUMN `OnDepotAmount` DECIMAL(18,2) NOT NULL DEFAULT 0;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` ADD INDEX `depotID` (`depotID` ASC, `IsPayedInDepot` ASC);", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` ADD INDEX `depotIDdate` (`depotID` ASC, `IsPayedInDepot` ASC, `dat` ASC);", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` ADD INDEX `partnerID_Date` (`partners_ID` ASC, `dat` ASC);", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` ADD COLUMN `Discount` DECIMAL(18,2) NOT NULL DEFAULT 0 AFTER `OnDepotAmount`;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`workers_account` RENAME TO  `gama_transportationdb`.`workers_contract` ;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"CREATE TABLE `gama_transportationdb`.`expenses` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `dat` datetime NOT NULL,
  `DateStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Note` tinytext NOT NULL,
  `Amount` decimal(18,2) NOT NULL DEFAULT '0.00',
  `workers_ID` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"CREATE TABLE `workers_absence` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `dat` datetime NOT NULL,
  `Note` tinytext,
  `AbsenceDaysCount` double NOT NULL DEFAULT '1',
  `workers_ID` int(11) NOT NULL,
  `Amount` decimal(18,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`ID`),
  KEY `FK_workers_idx` (`workers_ID`),
  CONSTRAINT `FK_workers` FOREIGN KEY (`workers_ID`) REFERENCES `workers` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;", Properties.Settings.Default.gama_transportationdbConnectionString);


            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_expenses` 
DROP FOREIGN KEY `PackageOrder_Expenses`;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_expenses` 
CHANGE COLUMN `packages_order_ID` `packages_order_ID` INT(11) NULL ,
CHANGE COLUMN `packages_order_YearID` `packages_order_YearID` INT(11) NULL ,
ADD COLUMN `sending_OrderID` INT NULL AFTER `Deleted`;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_expenses` 
ADD CONSTRAINT `PackageOrder_Expenses`
  FOREIGN KEY (`packages_order_ID` , `packages_order_YearID`)
  REFERENCES `gama_transportationdb`.`package_delivrer_order` (`ID` , `SerialYear`)
  ON UPDATE NO ACTION;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_payments` 
DROP FOREIGN KEY `deliverers_Payment`;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_payments` 
CHANGE COLUMN `packages_order_ID` `packages_order_ID` INT(11) NULL ,
CHANGE COLUMN `packages_order_YearID` `packages_order_YearID` INT(11) NULL ,
ADD COLUMN `sending_OrderID` INT NULL AFTER `package_order_expenses_ID`;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_payments` 
ADD CONSTRAINT `deliverers_Payment`
  FOREIGN KEY (`packages_order_ID` , `packages_order_YearID`)
  REFERENCES `gama_transportationdb`.`package_delivrer_order` (`ID` , `SerialYear`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`workers_absence` 
ADD COLUMN `deleted` TINYINT(1) NOT NULL DEFAULT 0 AFTER `Amount`,
ADD COLUMN `DateStamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `deleted`;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`packages_order` 
ADD COLUMN `DepotID` INT NULL AFTER `Delivered`;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_payments` 
ADD INDEX `sendingOrder_p_idx` (`sending_OrderID` ASC);", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_payments` 
ADD CONSTRAINT `sendingOrder_p`
  FOREIGN KEY (`sending_OrderID`)
  REFERENCES `gama_transportationdb`.`packages_order` (`ID`)
  ON DELETE RESTRICT
  ON UPDATE NO ACTION;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"DROP FUNCTION `WorkerBalance`;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"CREATE FUNCTION `WorkerBalance`(workerID INTEGER,maxDate datetime) RETURNS decimal(18,2)
BEGIN
DECLARE debit DECIMAL(18,2); 
DECLARE absence DECIMAL(18,2); 
DECLARE credit DECIMAL(18,2);
SELECT       ifnull( SUM(Amount),0) 
FROM            workers_contract
WHERE        (Deleted = 0) AND (workers_ID = workerID) and dat < maxDate INTO credit;

SELECT       ifnull( SUM(Amount),0)
FROM            workers_payments
WHERE        (Deleted = 0) AND (workers_ID = workerID) and dat < maxDate INTO debit;

SELECT       ifnull( SUM(Amount),0)
FROM            workers_absence
WHERE        (Deleted = 0) AND (workers_ID = workerID) and dat < maxDate INTO absence;

RETURN credit - debit - absence;
END;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`partners_payments` 
ADD COLUMN `sending_OrderID` INT NULL;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`partners_payments` 
ADD INDEX `fk_sendingOrderID_idx` (`sending_OrderID` ASC);", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`partners_payments` 
ADD CONSTRAINT `fk_sendingOrderID`
  FOREIGN KEY (`sending_OrderID`)
  REFERENCES `gama_transportationdb`.`packages_order` (`ID`)
  ON DELETE RESTRICT
  ON UPDATE NO ACTION;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_expenses` 
ADD INDEX `SendingOrder_Expenses_idx` (`sending_OrderID` ASC);", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_expenses` 
ADD CONSTRAINT `SendingOrder_Expenses`
  FOREIGN KEY (`sending_OrderID`)
  REFERENCES `gama_transportationdb`.`packages_order` (`ID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_order_payments` 
ADD COLUMN `Note` TINYTEXT NULL AFTER `sending_OrderID`;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`workers_contract` 
ADD COLUMN `Note` TEXT NULL;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`workers_payments` 
ADD COLUMN `Note` TEXT NULL;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`workers_contract` 
ADD COLUMN `IsActive` TINYINT(1) NULL DEFAULT 1;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`workers_payments` 
ADD COLUMN `IsActive` TINYINT(1) NULL DEFAULT 1;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`workers_absence` 
ADD COLUMN `IsActive` TINYINT(1) NULL DEFAULT 1;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`workers` ADD COLUMN `BeginWorkingAt` DATE NOT NULL DEFAULT '2019-1-1';", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`workers_payments` ADD COLUMN `CutFromDepotID` INT NULL;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`partners_payments` 
ADD INDEX `fk_delivererOrder_idx` (`deliverer_Order_ID` ASC, `deliverer_Order_SerialYear` ASC);
ALTER TABLE `gama_transportationdb`.`partners_payments` 
ADD CONSTRAINT `fk_delivererOrder`
  FOREIGN KEY (`deliverer_Order_ID` , `deliverer_Order_SerialYear`)
  REFERENCES `gama_transportationdb`.`package_delivrer_order` (`ID` , `SerialYear`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`workers_contract` CHANGE COLUMN `IsActive` `IsActive` TINYINT(1) NOT NULL DEFAULT '1' ;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"CREATE TABLE `gama_transportationdb`.`payment_methods` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC));
", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"INSERT INTO `gama_transportationdb`.`payment_methods`
(`id`,`Name`)
VALUES
(2,'����');", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"INSERT INTO `gama_transportationdb`.`payment_methods`
(`id`,`Name`)
VALUES
(1,'����');", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"INSERT INTO `gama_transportationdb`.`payment_methods`
(`id`,`Name`)
VALUES
(0,'��� ����');", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"UPDATE `gama_transportationdb`.`payment_methods` SET `id`='0' WHERE `Name`='��� ����';", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`packages_order` 
CHANGE COLUMN `Payed` `Payed` INT NOT NULL DEFAULT '0' ;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`packages_order` 
ADD INDEX `fk_payMothod_idx` (`Payed` ASC);", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`packages_order` 
ADD CONSTRAINT `fk_payMothod`
  FOREIGN KEY (`Payed`)
  REFERENCES `gama_transportationdb`.`payment_methods` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`packages_order_lines` CHANGE COLUMN `Payed` `Payed` INT NOT NULL DEFAULT '0' ;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"CREATE TABLE `gama_transportationdb`.`partners_rc` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `RC` VARCHAR(45) NOT NULL,
  `CompanyName` TEXT NOT NULL,
  `CompanyPhone` TEXT NULL,
  `Note` TEXT NULL,
  PRIMARY KEY (`id`));
", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE packages_order ADD COLUMN Note TEXT NULL;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `workers` ADD COLUMN `Phone` VARCHAR(45) NULL;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `workers` ADD COLUMN `EndWorkingAt` DATE NULL AFTER `Phone`;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`packages_order_lines` 
CHANGE COLUMN `Quantity` `Quantity` DECIMAL(18,3) NOT NULL DEFAULT '0';", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`packages_order_line_deliverer` 
CHANGE COLUMN `Quantity` `Quantity` DECIMAL(18,3) NOT NULL DEFAULT '0';", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`partners_payments` DROP FOREIGN KEY `fk_delivererOrder`;",
                Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`partners_payments` ADD CONSTRAINT `fk_delivererOrder`
FOREIGN KEY (`deliverer_Order_ID` , `deliverer_Order_SerialYear`)  REFERENCES `gama_transportationdb`.`package_delivrer_order` (`ID` , `SerialYear`)
  ON DELETE SET NULL
  ON UPDATE SET NULL;", Properties.Settings.Default.gama_transportationdbConnectionString);

            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`package_delivrer_order` 
ADD COLUMN `ResteToPay` DECIMAL(18,2) NOT NULL DEFAULT 0 AFTER `Discount`;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"ALTER TABLE `gama_transportationdb`.`packages_order` 
ADD COLUMN `TotalPayments` DECIMAL(18,2) NOT NULL DEFAULT 0;", Properties.Settings.Default.gama_transportationdbConnectionString);
            //DAL.DAL_EXECUTE.ExecuteNonQuery(@"", Properties.Settings.Default.gama_transportationdbConnectionString); 
            //DAL.DAL_EXECUTE.ExecuteNonQuery(@"", Properties.Settings.Default.gama_transportationdbConnectionString); 
            //DAL.DAL_EXECUTE.ExecuteNonQuery(@"", Properties.Settings.Default.gama_transportationdbConnectionString); 
            //DAL.DAL_EXECUTE.ExecuteNonQuery(@"", Properties.Settings.Default.gama_transportationdbConnectionString); 
        }
        #endregion      
       
        private void buttonItem2_Click(object sender, EventArgs e)
        {
            new Expenses.AddEditExpenses(0).ShowDialog();
        }

        private void buttonItem4_Click(object sender, EventArgs e)
        {
            AddTab(new Expenses.ExpensesList(), "����� ��������");
        }

        private void buttonItem6_Click(object sender, EventArgs e)
        {
            new Workers.AddEditWorker(0).ShowDialog();
        }

        private void buttonItem7_Click(object sender, EventArgs e)
        {
            AddTab(new Workers.WorkersList(), "����� ������");

        }

        private void metroTabItem8_Click(object sender, EventArgs e)
        {
            AddTab(new Caisse.Caisse(), "�������");
        }

        private void buttonItem11_Click(object sender, EventArgs e)
        {
            AddTab(new Operations.OperationsListJournal(), "����� �������� 2");
        }

        private void superTabControl1_SelectedTabChanged(object sender, SuperTabStripSelectedTabChangedEventArgs e)
        {
            try
            {
                ((IIUserControl)superTabControl1.SelectedTab.AttachedControl.Controls[0]).Refill();
            }
            catch (Exception)
            {
            }
        }

        private void btnRC_Click(object sender, EventArgs e)
        {
            new RC.RC().ShowDialog();
        }

        private void btnMigrate_Click(object sender, EventArgs e)
        {
           }

        private void buttonItem13_Click(object sender, EventArgs e)
        {
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"drop table if exists package_delivrer_order_payments;", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"Create table package_delivrer_order_payments (	SELECT        IFNULL(SUM(p.Amount), 0) AS Amount, package_delivrer_order.ID AS deliverer_Order_ID, package_delivrer_order.SerialYear AS deliverer_Order_SerialYear
FROM            package_order_payments RIGHT OUTER JOIN
                         package_order_expenses ON package_order_payments.package_order_expenses_ID = package_order_expenses.ID RIGHT OUTER JOIN
                         partners_payments p RIGHT OUTER JOIN
                         package_delivrer_order ON p.deliverer_Order_ID = package_delivrer_order.ID AND p.deliverer_Order_SerialYear = package_delivrer_order.SerialYear ON package_order_payments.partners_payments_ID = p.ID
WHERE        (package_order_expenses.ID IS NULL)
GROUP BY package_delivrer_order.ID, package_delivrer_order.SerialYear);", Properties.Settings.Default.gama_transportationdbConnectionString);
            DAL.DAL_EXECUTE.ExecuteNonQuery(@"UPDATE package_delivrer_order o, package_delivrer_order_payments p SET o.ResteToPay = o.Total - p.Amount WHERE o.ID = p.deliverer_Order_ID AND p.deliverer_Order_SerialYear = o.SerialYear;", Properties.Settings.Default.gama_transportationdbConnectionString); 
        }
    }
}