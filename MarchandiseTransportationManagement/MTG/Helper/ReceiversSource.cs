﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MTM.Helper
{
    public static class ReceiversSource
    {
        public static gama_transportationdbDataSet.RecipientSourceDataTable Source { get; set; }
        public static bool  RechargeRecipientAfterClosing  { get; set; }
        public static void ChargeSource()
        {
            BackgroundWorker bg = new BackgroundWorker();
            bg.DoWork += (fd, rt) =>
            {
                RechargeRecipientAfterClosing = true;
                Source = new gama_transportationdbDataSetTableAdapters.RecipientSourceTableAdapter().GetData();
                RechargeRecipientAfterClosing = false;
            };
            bg.RunWorkerAsync();
           
        }
    }

    public static class ReceiversPhonesSource
    {
        public static gama_transportationdbDataSet.RecipientSourceDataTable Source { get; set; }
        public static bool RechargeRecipientAfterClosing { get; set; }
        public static void ChargeSource()
        {
            BackgroundWorker bg = new BackgroundWorker();
            bg.DoWork += (fd, rt) =>
            {
                RechargeRecipientAfterClosing = true;
                Source = new gama_transportationdbDataSetTableAdapters.RecipientSourceTableAdapter().GetData();
                RechargeRecipientAfterClosing = false;
            };
            bg.RunWorkerAsync();

        }
    }
}
