using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Expenses
{
    public partial class ReportForm1 : DevComponents.DotNetBar.Metro.MetroForm
    {
        public DateTime d1 { get; set; }
        public DateTime d2 { get; set; }
        public ReportForm1()
        {
            InitializeComponent();
            this.FormClosing += (se, ee) =>
            {
                this.reportViewer1.LocalReport.ReleaseSandboxAppDomain();
            };
        }

        private void PrintReportForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.expenses' table. You can move, or remove it, as needed.
            this.expensesTableAdapter.FillByDate(this.gama_transportationdbDataSet.expenses,d1,d2);

            this.reportViewer1.RefreshReport();
        }
    }
}