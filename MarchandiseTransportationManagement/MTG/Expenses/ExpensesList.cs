using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Expenses
{
    public partial class ExpensesList : UserControl,IIUserControl
    {
        public ExpensesList()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (new Expenses.AddEditExpenses(0).ShowDialog() == DialogResult.Cancel) return;
            btnFillByDate.PerformClick();
            expensesBindingSource.MoveLast();
            doubleInput1.Value = Convert.ToDouble(gama_transportationdbDataSet.expenses.Sum(x => !x.deleted && x.Note.Contains(SearchBox.Text) ? x.Amount : 0));
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (expensesBindingSource.Position < 0) return;
            int pos = expensesBindingSource.Position;
            if (new Expenses.AddEditExpenses((int)((DataRowView)expensesBindingSource.Current)["ID"]).ShowDialog() == DialogResult.Cancel) return;
            btnFillByDate.PerformClick();
            expensesBindingSource.Position = pos;
            doubleInput1.Value = Convert.ToDouble(gama_transportationdbDataSet.expenses.Sum(x => !x.deleted && x.Note.Contains(SearchBox.Text) ? x.Amount : 0));
        }

        private void PartnersList_Load(object sender, EventArgs e)
        {
            dateTimeInput1.Value = dateTimeInput1.Value.Date.AddDays(-7);
            this.expensesTableAdapter.FillByDate(gama_transportationdbDataSet.expenses, dateTimeInput1.Value.Date, dateTimeInput2.Value.Date.AddDays(1));
            doubleInput1.Value = Convert.ToDouble(gama_transportationdbDataSet.expenses.Sum(x => !x.deleted && x.Note.Contains(SearchBox.Text) ? x.Amount : 0));
        }

        private void WarehouseChBox_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            expensesBindingSource.Filter = "Note LIKE '%" + SearchBox.Text + "%' AND deleted = 0";
            doubleInput1.Value = Convert.ToDouble(gama_transportationdbDataSet.expenses.Sum(x => !x.deleted && x.Note.Contains(SearchBox.Text) ?  x.Amount: 0));
        }

        private void partnersBindingSource_PositionChanged(object sender, EventArgs e)
        {
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (expensesBindingSource.Position < 0) return;

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (expensesBindingSource.Position < 0) return;
            try
            {
                if (MessageBox.Show("����� ����� ��������", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
                ((DataRowView)expensesBindingSource.Current)["deleted"] = 1;
                this.Validate();
                this.expensesBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(gama_transportationdbDataSet);
                int pos = expensesBindingSource.Position;
                btnFillByDate.PerformClick();
                expensesBindingSource.Position = pos;
                doubleInput1.Value = Convert.ToDouble(gama_transportationdbDataSet.expenses.Sum(x => !x.deleted && x.Note.Contains(SearchBox.Text) ? x.Amount : 0));
            }
            catch (Exception)
            {
            }
        }

        private void btnFillByDate_Click(object sender, EventArgs e)
        {
            this.expensesTableAdapter.FillByDate(gama_transportationdbDataSet.expenses, dateTimeInput1.Value.Date, dateTimeInput2.Value.Date.AddDays(1));
            doubleInput1.Value = Convert.ToDouble(gama_transportationdbDataSet.expenses.Sum(x => !x.deleted && x.Note.Contains(SearchBox.Text) ? x.Amount : 0));
        }

        private void dataGridViewX1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
        }

        private void dataGridViewX1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnEdit.PerformClick();
        }

        private void expensesBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
        }

        private void mainDataGridViewX_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnEdit.PerformClick();
        }

        public void Refill()
        {
            btnFillByDate_Click(null, null);
        }

        private void btnPrint_Click_1(object sender, EventArgs e)
        {
            new ReportForm1() { d1=dateTimeInput1.Value.Date,d2 = dateTimeInput2.Value.Date.AddDays(1) }.ShowDialog(this);
        }
    }
}