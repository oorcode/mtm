using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Drawing.Printing;

namespace MTM.OwnerInfo
{
    public partial class CompanyInfoForm : DevComponents.DotNetBar.Metro.MetroForm
    {
        SSUtilities.SSConfigFile cfg = new SSUtilities.SSConfigFile();
        public CompanyInfoForm()
        {
            InitializeComponent();
        }

        private void CompanyInfoForm_Load(object sender, EventArgs e)
        {
            textBoxX1.Text = cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName,"");
            textBoxX2.Text = cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity,"");
            textBoxX3.Text = cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1,"");
            textBoxX4.Text = cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, "");
            String[] ss = new String[PrinterSettings.InstalledPrinters.Count];
            PrinterSettings.InstalledPrinters.CopyTo(ss, 0);
            comboBoxEx1.Items.AddRange(ss);
            try
            {
                comboBoxEx1.SelectedIndex = 0;
            }
            catch (Exception) { }
            comboBoxEx1.Text = cfg.GetValue<string>(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.MTM.DefaultPrinter, "");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            cfg.SetValue(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyName, textBoxX1.Text);
            cfg.SetValue(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.CompanyActivity, textBoxX2.Text);
            cfg.SetValue(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile1,textBoxX3.Text);
            cfg.SetValue(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.CompanyInfo.Mobile2, textBoxX4.Text);
            cfg.SetValue(SSUtilities.CONFIG_SECTIONS.MTM.NAME, SSUtilities.CONFIG_SECTIONS.MTM.DefaultPrinter, comboBoxEx1.Text);
            this.Close();
        }
    }
}