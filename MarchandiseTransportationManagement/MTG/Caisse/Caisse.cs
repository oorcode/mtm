using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Caisse
{
    public partial class Caisse : UserControl
    {
        gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter payments;
        gama_transportationdbDataSetTableAdapters.expensesTableAdapter expenses;
        gama_transportationdbDataSetTableAdapters.workers_paymentsTableAdapter wPayments;
        gama_transportationdbDataSetTableAdapters.package_order_paymentsTableAdapter poPayMinus;
        public Caisse()
        {
            InitializeComponent();
        }

        private void btnFillByDate_Click(object sender, EventArgs e)
        {
            var d1 = dateTimeInput1.Value.Date;
            var d2 = dateTimeInput2.Value.Date.AddDays(1);
            PayBox1.Value = Convert.ToDouble(payments.SumAmountByDate(d1, d2).GetValueOrDefault(0) - poPayMinus.SumAllByDate(d1, d2).GetValueOrDefault(0));
            wPayBox1.Value = Convert.ToDouble(wPayments.SumOfPaymentsByDate(d1, d2).GetValueOrDefault(0));
            ExpBox1.Value = Convert.ToDouble(expenses.SumByDate(d1, d2).GetValueOrDefault(0));
            ResteBox1.Value = PayBox1.Value - wPayBox1.Value - ExpBox1.Value;

            PayBox.Value = Convert.ToDouble(payments.SumAmount().GetValueOrDefault(0) - poPayMinus.SumAll().GetValueOrDefault(0));
            wPayBox.Value = Convert.ToDouble(wPayments.SumOfPayments().GetValueOrDefault(0));
            ExpBox.Value = Convert.ToDouble(expenses.SumAll().GetValueOrDefault(0));
            ResteBox.Value = PayBox.Value - wPayBox.Value - ExpBox.Value;
        }

        private void WorkersList_Load(object sender, EventArgs e)
        {
            payments = new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter();
            wPayments = new gama_transportationdbDataSetTableAdapters.workers_paymentsTableAdapter();
            expenses = new gama_transportationdbDataSetTableAdapters.expensesTableAdapter();
            poPayMinus = new gama_transportationdbDataSetTableAdapters.package_order_paymentsTableAdapter();
         
            PayBox.Value = Convert.ToDouble(payments.SumAmount().GetValueOrDefault(0) - poPayMinus.SumAll().GetValueOrDefault(0));
            wPayBox.Value = Convert.ToDouble(wPayments.SumOfPayments().GetValueOrDefault(0));
            ExpBox.Value = Convert.ToDouble(expenses.SumAll().GetValueOrDefault(0));
            ResteBox.Value = PayBox.Value - wPayBox.Value - ExpBox.Value;
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            new ReportForm1() {
            t1 = PayBox.Value,
            t2 = wPayBox.Value,
            t3 = ExpBox.Value,
            t4 = ResteBox.Value,
            t5 = PayBox1.Value,
            t6 = wPayBox1.Value,
            t7 = ExpBox1.Value,
            t8 = ResteBox1.Value,
            dat1 = dateTimeInput1.Value,
            dat2 = dateTimeInput2.Value
            }.ShowDialog(this);
        }
    }
}