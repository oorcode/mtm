using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Microsoft.Reporting.WinForms;

namespace MTM.Caisse
{
    public partial class ReportForm1 : DevComponents.DotNetBar.Metro.MetroForm
    {
        public double t1 { get; set; }
        public double t2 { get; set; }
        public double t3 { get; set; }
        public double t4 { get; set; }
        public double t5 { get; set; }
        public double t6 { get; set; }
        public double t7 { get; set; }
        public double t8 { get; set; }
        public DateTime dat1 { get; set; }
        public DateTime dat2 { get; set; }

        public ReportForm1()
        {
            InitializeComponent();
            this.FormClosing += (se, ee) =>
            {
                this.reportViewer1.LocalReport.ReleaseSandboxAppDomain();
            };
        }

        private void PrintReportForm_Load(object sender, EventArgs e)
        {
            reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter[] { 
                new ReportParameter("tot1",t1.ToString()),
                new ReportParameter("tot2",t2.ToString()),
                new ReportParameter("tot3",t3.ToString()),
                new ReportParameter("tot4",t4.ToString()),
                new ReportParameter("tot5",t5.ToString()),
                new ReportParameter("tot6",t6.ToString()),
                new ReportParameter("tot7",t7.ToString()),
                new ReportParameter("tot8",t8.ToString()),
                new ReportParameter("dat1",dat1.ToString("yyyy-MM-dd")),
                new ReportParameter("dat2",dat2.ToString("yyyy-MM-dd")),
            });
            this.reportViewer1.RefreshReport();
        }
    }
}