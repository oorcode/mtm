﻿namespace MTM.Caisse
{
    partial class Caisse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnExit = new DevComponents.DotNetBar.ButtonX();
            this.btnFillByDate = new DevComponents.DotNetBar.ButtonX();
            this.dateTimeInput2 = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.dateTimeInput1 = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.workersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gama_transportationdbDataSet = new MTM.gama_transportationdbDataSet();
            this.tableAdapterManager = new MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager();
            this.workersTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.workersTableAdapter();
            this.workers_accountTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.workers_accountTableAdapter();
            this.workers_paymentsTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.workers_paymentsTableAdapter();
            this.workers_absenceTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.workers_absenceTableAdapter();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.ResteBox = new DevComponents.Editors.DoubleInput();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.ExpBox = new DevComponents.Editors.DoubleInput();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.wPayBox = new DevComponents.Editors.DoubleInput();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.PayBox = new DevComponents.Editors.DoubleInput();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.ResteBox1 = new DevComponents.Editors.DoubleInput();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.ExpBox1 = new DevComponents.Editors.DoubleInput();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.wPayBox1 = new DevComponents.Editors.DoubleInput();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.PayBox1 = new DevComponents.Editors.DoubleInput();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.workers_accountBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.workers_absenceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.workers_paymentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimeInput2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimeInput1)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.workersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResteBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wPayBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PayBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResteBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wPayBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PayBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workers_accountBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workers_absenceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workers_paymentsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.Controls.Add(this.btnExit, 5, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(868, 39);
            this.tableLayoutPanel1.TabIndex = 54;
            // 
            // btnExit
            // 
            this.btnExit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(3, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(106, 33);
            this.btnExit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnExit.Symbol = "";
            this.btnExit.SymbolColor = System.Drawing.Color.Maroon;
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "خروج";
            // 
            // btnFillByDate
            // 
            this.btnFillByDate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFillByDate.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFillByDate.Location = new System.Drawing.Point(101, 3);
            this.btnFillByDate.Name = "btnFillByDate";
            this.btnFillByDate.Size = new System.Drawing.Size(88, 28);
            this.btnFillByDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFillByDate.TabIndex = 2;
            this.btnFillByDate.Text = "تحديث";
            this.btnFillByDate.Click += new System.EventHandler(this.btnFillByDate_Click);
            // 
            // dateTimeInput2
            // 
            this.dateTimeInput2.AllowEmptyState = false;
            this.dateTimeInput2.AutoAdvance = true;
            this.dateTimeInput2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.dateTimeInput2.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dateTimeInput2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dateTimeInput2.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dateTimeInput2.ButtonDropDown.Visible = true;
            this.dateTimeInput2.FocusHighlightEnabled = true;
            this.dateTimeInput2.ForeColor = System.Drawing.Color.Black;
            this.dateTimeInput2.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center;
            this.dateTimeInput2.IsPopupCalendarOpen = false;
            this.dateTimeInput2.Location = new System.Drawing.Point(198, 3);
            // 
            // 
            // 
            // 
            // 
            // 
            this.dateTimeInput2.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dateTimeInput2.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.dateTimeInput2.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dateTimeInput2.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dateTimeInput2.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dateTimeInput2.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dateTimeInput2.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dateTimeInput2.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dateTimeInput2.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dateTimeInput2.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dateTimeInput2.MonthCalendar.DisplayMonth = new System.DateTime(2018, 12, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.dateTimeInput2.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dateTimeInput2.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dateTimeInput2.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dateTimeInput2.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dateTimeInput2.MonthCalendar.TodayButtonVisible = true;
            this.dateTimeInput2.Name = "dateTimeInput2";
            this.dateTimeInput2.Size = new System.Drawing.Size(129, 27);
            this.dateTimeInput2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dateTimeInput2.TabIndex = 1;
            // 
            // dateTimeInput1
            // 
            this.dateTimeInput1.AllowEmptyState = false;
            this.dateTimeInput1.AutoAdvance = true;
            this.dateTimeInput1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.dateTimeInput1.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dateTimeInput1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dateTimeInput1.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dateTimeInput1.ButtonDropDown.Visible = true;
            this.dateTimeInput1.FocusHighlightEnabled = true;
            this.dateTimeInput1.ForeColor = System.Drawing.Color.Black;
            this.dateTimeInput1.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center;
            this.dateTimeInput1.IsPopupCalendarOpen = false;
            this.dateTimeInput1.Location = new System.Drawing.Point(374, 3);
            // 
            // 
            // 
            // 
            // 
            // 
            this.dateTimeInput1.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dateTimeInput1.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.dateTimeInput1.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dateTimeInput1.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dateTimeInput1.MonthCalendar.DisplayMonth = new System.DateTime(2018, 12, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.dateTimeInput1.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dateTimeInput1.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dateTimeInput1.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dateTimeInput1.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dateTimeInput1.MonthCalendar.TodayButtonVisible = true;
            this.dateTimeInput1.Name = "dateTimeInput1";
            this.dateTimeInput1.Size = new System.Drawing.Size(129, 27);
            this.dateTimeInput1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dateTimeInput1.TabIndex = 1;
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(333, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(32, 28);
            this.labelX3.TabIndex = 0;
            this.labelX3.Text = "إلى:";
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(509, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(78, 28);
            this.labelX2.TabIndex = 0;
            this.labelX2.Text = "التاريخ من:";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tableLayoutPanel2.ColumnCount = 8;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 138F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 138F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 96F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 96F));
            this.tableLayoutPanel2.Controls.Add(this.labelX2, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelX3, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.dateTimeInput1, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.dateTimeInput2, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnFillByDate, 6, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonX1, 7, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 39);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(868, 34);
            this.tableLayoutPanel2.TabIndex = 57;
            // 
            // workersBindingSource
            // 
            this.workersBindingSource.DataMember = "workers";
            this.workersBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // gama_transportationdbDataSet
            // 
            this.gama_transportationdbDataSet.DataSetName = "gama_transportationdbDataSet";
            this.gama_transportationdbDataSet.EnforceConstraints = false;
            this.gama_transportationdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.deliverersTableAdapter = null;
            this.tableAdapterManager.discountlinesTableAdapter = null;
            this.tableAdapterManager.discountsTableAdapter = null;
            this.tableAdapterManager.expensesTableAdapter = null;
            this.tableAdapterManager.locationsTableAdapter = null;
            this.tableAdapterManager.operationsTableAdapter = null;
            this.tableAdapterManager.package_delivrer_orderTableAdapter = null;
            this.tableAdapterManager.package_order_expensesTableAdapter = null;
            this.tableAdapterManager.package_order_payments1TableAdapter = null;
            this.tableAdapterManager.package_order_paymentsTableAdapter = null;
            this.tableAdapterManager.packages_order_line_delivererTableAdapter = null;
            this.tableAdapterManager.packages_order_linesTableAdapter = null;
            this.tableAdapterManager.packages_orderTableAdapter = null;
            this.tableAdapterManager.partners_paymentsTableAdapter = null;
            this.tableAdapterManager.partners_rcTableAdapter = null;
            this.tableAdapterManager.partnersTableAdapter = null;
            this.tableAdapterManager.payment_methodsTableAdapter = null;
            this.tableAdapterManager.productsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.workers_absenceTableAdapter = null;
            this.tableAdapterManager.workers_accountTableAdapter = null;
            this.tableAdapterManager.workers_paymentsTableAdapter = null;
            this.tableAdapterManager.workersTableAdapter = null;
            // 
            // workersTableAdapter
            // 
            this.workersTableAdapter.ClearBeforeFill = true;
            // 
            // workers_accountTableAdapter
            // 
            this.workers_accountTableAdapter.ClearBeforeFill = true;
            // 
            // workers_paymentsTableAdapter
            // 
            this.workers_paymentsTableAdapter.ClearBeforeFill = true;
            // 
            // workers_absenceTableAdapter
            // 
            this.workers_absenceTableAdapter.ClearBeforeFill = true;
            // 
            // labelX4
            // 
            this.labelX4.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.Location = new System.Drawing.Point(487, 249);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(236, 47);
            this.labelX4.TabIndex = 0;
            this.labelX4.Text = "باقي الصندوق:";
            // 
            // ResteBox
            // 
            this.ResteBox.AllowEmptyState = false;
            this.ResteBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.ResteBox.BackgroundStyle.Class = "DateTimeInputBackground";
            this.ResteBox.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ResteBox.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.ResteBox.FocusHighlightEnabled = true;
            this.ResteBox.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResteBox.Increment = 1D;
            this.ResteBox.IsInputReadOnly = true;
            this.ResteBox.Location = new System.Drawing.Point(145, 253);
            this.ResteBox.Name = "ResteBox";
            this.ResteBox.Size = new System.Drawing.Size(269, 43);
            this.ResteBox.TabIndex = 1;
            // 
            // labelX5
            // 
            this.labelX5.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.Location = new System.Drawing.Point(487, 200);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(236, 47);
            this.labelX5.TabIndex = 0;
            this.labelX5.Text = "مجموع المصاريف:";
            // 
            // ExpBox
            // 
            this.ExpBox.AllowEmptyState = false;
            this.ExpBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.ExpBox.BackgroundStyle.Class = "DateTimeInputBackground";
            this.ExpBox.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ExpBox.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.ExpBox.FocusHighlightEnabled = true;
            this.ExpBox.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExpBox.Increment = 1D;
            this.ExpBox.IsInputReadOnly = true;
            this.ExpBox.Location = new System.Drawing.Point(145, 204);
            this.ExpBox.Name = "ExpBox";
            this.ExpBox.Size = new System.Drawing.Size(269, 43);
            this.ExpBox.TabIndex = 1;
            // 
            // labelX6
            // 
            this.labelX6.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.Location = new System.Drawing.Point(487, 151);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(236, 47);
            this.labelX6.TabIndex = 0;
            this.labelX6.Text = "مجموع الأجور:";
            // 
            // wPayBox
            // 
            this.wPayBox.AllowEmptyState = false;
            this.wPayBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.wPayBox.BackgroundStyle.Class = "DateTimeInputBackground";
            this.wPayBox.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wPayBox.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.wPayBox.FocusHighlightEnabled = true;
            this.wPayBox.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wPayBox.Increment = 1D;
            this.wPayBox.IsInputReadOnly = true;
            this.wPayBox.Location = new System.Drawing.Point(145, 155);
            this.wPayBox.Name = "wPayBox";
            this.wPayBox.Size = new System.Drawing.Size(269, 43);
            this.wPayBox.TabIndex = 1;
            // 
            // labelX7
            // 
            this.labelX7.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.Location = new System.Drawing.Point(487, 102);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(236, 47);
            this.labelX7.TabIndex = 0;
            this.labelX7.Text = "مجموع الدفوعات:";
            // 
            // PayBox
            // 
            this.PayBox.AllowEmptyState = false;
            this.PayBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.PayBox.BackgroundStyle.Class = "DateTimeInputBackground";
            this.PayBox.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.PayBox.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.PayBox.FocusHighlightEnabled = true;
            this.PayBox.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PayBox.Increment = 1D;
            this.PayBox.IsInputReadOnly = true;
            this.PayBox.Location = new System.Drawing.Point(145, 106);
            this.PayBox.Name = "PayBox";
            this.PayBox.Size = new System.Drawing.Size(269, 43);
            this.PayBox.TabIndex = 1;
            // 
            // labelX1
            // 
            this.labelX1.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.Location = new System.Drawing.Point(452, 498);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(271, 47);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "باقي الصندوق:";
            // 
            // ResteBox1
            // 
            this.ResteBox1.AllowEmptyState = false;
            this.ResteBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.ResteBox1.BackgroundStyle.Class = "DateTimeInputBackground";
            this.ResteBox1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ResteBox1.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.ResteBox1.FocusHighlightEnabled = true;
            this.ResteBox1.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResteBox1.Increment = 1D;
            this.ResteBox1.IsInputReadOnly = true;
            this.ResteBox1.Location = new System.Drawing.Point(145, 502);
            this.ResteBox1.Name = "ResteBox1";
            this.ResteBox1.Size = new System.Drawing.Size(269, 43);
            this.ResteBox1.TabIndex = 1;
            // 
            // labelX8
            // 
            this.labelX8.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX8.Location = new System.Drawing.Point(452, 449);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(271, 47);
            this.labelX8.TabIndex = 0;
            this.labelX8.Text = "مجموع المصاريف:";
            // 
            // ExpBox1
            // 
            this.ExpBox1.AllowEmptyState = false;
            this.ExpBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.ExpBox1.BackgroundStyle.Class = "DateTimeInputBackground";
            this.ExpBox1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ExpBox1.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.ExpBox1.FocusHighlightEnabled = true;
            this.ExpBox1.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExpBox1.Increment = 1D;
            this.ExpBox1.IsInputReadOnly = true;
            this.ExpBox1.Location = new System.Drawing.Point(145, 453);
            this.ExpBox1.Name = "ExpBox1";
            this.ExpBox1.Size = new System.Drawing.Size(269, 43);
            this.ExpBox1.TabIndex = 1;
            // 
            // labelX9
            // 
            this.labelX9.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.Location = new System.Drawing.Point(452, 400);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(271, 47);
            this.labelX9.TabIndex = 0;
            this.labelX9.Text = "مجموع الأجور:";
            // 
            // wPayBox1
            // 
            this.wPayBox1.AllowEmptyState = false;
            this.wPayBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.wPayBox1.BackgroundStyle.Class = "DateTimeInputBackground";
            this.wPayBox1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wPayBox1.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.wPayBox1.FocusHighlightEnabled = true;
            this.wPayBox1.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wPayBox1.Increment = 1D;
            this.wPayBox1.IsInputReadOnly = true;
            this.wPayBox1.Location = new System.Drawing.Point(145, 404);
            this.wPayBox1.Name = "wPayBox1";
            this.wPayBox1.Size = new System.Drawing.Size(269, 43);
            this.wPayBox1.TabIndex = 1;
            // 
            // labelX10
            // 
            this.labelX10.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX10.Location = new System.Drawing.Point(452, 351);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(271, 47);
            this.labelX10.TabIndex = 0;
            this.labelX10.Text = "مجموع الدفوعات:";
            // 
            // PayBox1
            // 
            this.PayBox1.AllowEmptyState = false;
            this.PayBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.PayBox1.BackgroundStyle.Class = "DateTimeInputBackground";
            this.PayBox1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.PayBox1.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.PayBox1.FocusHighlightEnabled = true;
            this.PayBox1.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PayBox1.Increment = 1D;
            this.PayBox1.IsInputReadOnly = true;
            this.PayBox1.Location = new System.Drawing.Point(145, 355);
            this.PayBox1.Name = "PayBox1";
            this.PayBox1.Size = new System.Drawing.Size(269, 43);
            this.PayBox1.TabIndex = 1;
            // 
            // labelX11
            // 
            this.labelX11.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX11.ForeColor = System.Drawing.Color.Red;
            this.labelX11.Location = new System.Drawing.Point(145, 302);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(269, 47);
            this.labelX11.TabIndex = 0;
            this.labelX11.Text = "حسب المدة المحدد";
            this.labelX11.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(5, 3);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(88, 28);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.TabIndex = 2;
            this.buttonX1.Text = "طباعة";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // workers_accountBindingSource
            // 
            this.workers_accountBindingSource.DataMember = "fk_workers_account_workers1";
            this.workers_accountBindingSource.DataSource = this.workersBindingSource;
            // 
            // workers_absenceBindingSource
            // 
            this.workers_absenceBindingSource.DataMember = "FK_workers";
            this.workers_absenceBindingSource.DataSource = this.workersBindingSource;
            // 
            // workers_paymentsBindingSource
            // 
            this.workers_paymentsBindingSource.DataMember = "fk_workers_payments_workers1";
            this.workers_paymentsBindingSource.DataSource = this.workersBindingSource;
            // 
            // Caisse
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.PayBox1);
            this.Controls.Add(this.labelX11);
            this.Controls.Add(this.labelX10);
            this.Controls.Add(this.PayBox);
            this.Controls.Add(this.wPayBox1);
            this.Controls.Add(this.labelX7);
            this.Controls.Add(this.labelX9);
            this.Controls.Add(this.wPayBox);
            this.Controls.Add(this.ExpBox1);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.labelX8);
            this.Controls.Add(this.ExpBox);
            this.Controls.Add(this.ResteBox1);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.ResteBox);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Caisse";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Size = new System.Drawing.Size(868, 577);
            this.Load += new System.EventHandler(this.WorkersList_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateTimeInput2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimeInput1)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.workersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResteBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wPayBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PayBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResteBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wPayBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PayBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workers_accountBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workers_absenceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workers_paymentsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevComponents.DotNetBar.ButtonX btnExit;
        private DevComponents.DotNetBar.ButtonX btnFillByDate;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dateTimeInput2;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dateTimeInput1;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private gama_transportationdbDataSet gama_transportationdbDataSet;
        private gama_transportationdbDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingSource workersBindingSource;
        private gama_transportationdbDataSetTableAdapters.workersTableAdapter workersTableAdapter;
        private System.Windows.Forms.BindingSource workers_accountBindingSource;
        private System.Windows.Forms.BindingSource workers_paymentsBindingSource;
        private gama_transportationdbDataSetTableAdapters.workers_accountTableAdapter workers_accountTableAdapter;
        private gama_transportationdbDataSetTableAdapters.workers_paymentsTableAdapter workers_paymentsTableAdapter;
        private System.Windows.Forms.BindingSource workers_absenceBindingSource;
        private gama_transportationdbDataSetTableAdapters.workers_absenceTableAdapter workers_absenceTableAdapter;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.Editors.DoubleInput ResteBox;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.Editors.DoubleInput ExpBox;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.Editors.DoubleInput wPayBox;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.Editors.DoubleInput PayBox;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.Editors.DoubleInput ResteBox1;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.Editors.DoubleInput ExpBox1;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.Editors.DoubleInput wPayBox1;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.Editors.DoubleInput PayBox1;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.ButtonX buttonX1;
    }
}