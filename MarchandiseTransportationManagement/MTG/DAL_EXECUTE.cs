﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MTM.DAL
{
    public class DAL_EXECUTE
    {
        #region EXECUTE
        public static int ExecuteNonQuery(string textCommand, string connection)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(connection))
            {
                try
                {
                    con.Open();
                    //File.AppendAllText("./test.txt",con.ConnectionString+Environment.NewLine+textCommand);
                    MySql.Data.MySqlClient.MySqlCommand cm = new MySql.Data.MySqlClient.MySqlCommand(textCommand);
                    cm.Connection = con;
                    int result = cm.ExecuteNonQuery();
                    con.Close();
                    return result;
                }
                catch (Exception ex)
                {
                    return -1;
                }
            }
        }

        public static System.Data.DataTable ExecuteReader(string textCommand, string connection, string tablename = "TABLELHOM")
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(connection))
            {
                try
                {
                    con.Open();
                    MySql.Data.MySqlClient.MySqlCommand cm = new MySql.Data.MySqlClient.MySqlCommand();
                    cm.Connection = con;
                    cm.CommandText = textCommand;
             
                    MySql.Data.MySqlClient.MySqlDataReader reader = cm.ExecuteReader();

                    DataTable dt = new DataTable(tablename);
                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    dt.DataSet.EnforceConstraints = false;
                    dt.Load(reader);
                    con.Close();
                    return dt;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static T ExecuteScalar<T>(string textCommand, string connection)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(connection))
            {
                try
                {
                    con.Open();
                    MySql.Data.MySqlClient.MySqlCommand cm = new MySql.Data.MySqlClient.MySqlCommand(textCommand);
                    cm.Connection = con;
                    T result = (T)cm.ExecuteScalar();
                    con.Close();
                    return result;
                }
                catch (Exception)
                {
                    return default(T);
                }
            }
        }
        #endregion
    }
}
