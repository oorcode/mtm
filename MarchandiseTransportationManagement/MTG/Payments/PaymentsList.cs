using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Payments
{
    public partial class PaymentsList : UserControl,IIUserControl
    {
        public PaymentsList()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var select = new Partners.SelectPartner();
            select.IsFactory = false;
            select.IsMerchant = false;
            select.IsWarehouse = false;
            if (select.ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
            new Payments.AddEditPayment(0, select.SelectedPartnerID).ShowDialog(this);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (partnersPaymentsListBindingSource.Position < 0) return;
            int idpay  = Convert.ToInt32(((DataRowView)this.partnersPaymentsListBindingSource.Current)["ID"]);
            new Payments.AddEditPayment(idpay,0).ShowDialog(this);
        }

        private void PaymentsList_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'viewDataSet.PartnersPaymentsList' table. You can move, or remove it, as needed.
            this.partnersPaymentsListTableAdapter.Fill(this.viewDataSet.PartnersPaymentsList);

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (partnersPaymentsListBindingSource.Position < 0) return;
            if (MessageBox.Show("����� ��� ������ �����Ͽ", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            try
            {
                new PaymentRepository().DeletePayment(Convert.ToInt32(((DataRowView)this.partnersPaymentsListBindingSource.Current)["ID"]));
                //new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter().DeleteQuery(
                //     Convert.ToInt32(((DataRowView)this.partnersPaymentsListBindingSource.Current)["ID"]));
                partnersPaymentsListBindingSource.RemoveCurrent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

        }

        private void mainDataGridViewX_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void textBoxX1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                partnersPaymentsListBindingSource.Filter = "CONVERT(deliverer_Order_ID, 'System.String') = '" + textBoxX1.Text + "' OR CONVERT(sending_OrderID, 'System.String') = '" + textBoxX1.Text + "' OR Phones LIKE '%" + textBoxX1.Text + "%' OR Note LIKE '%" + textBoxX1.Text + "%' OR CustomerName LIKE '%" + textBoxX1.Text + "%' ";
            }
            catch (Exception)
            {
            }
        }

        public void Refill()
        {
            PaymentsList_Load(null, null);
        }
    }
}