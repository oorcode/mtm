using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Payments
{
    public partial class AddEditPayment : DevComponents.DotNetBar.Metro.MetroForm
    {
        int ID = 0;
        private int PartnerID;
        public double StartUpAmount { get; set; }
        public int? DelivererOrderYear { get; set; }
        public int? DelivererOrderID { get; set; }
        public int? SendingOrder { get; set; }
        public AddEditPayment(int Id_, int PartnersID)
        {
            InitializeComponent();
            ID = Id_;
            PartnerID = PartnersID;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void AddEditProduct_Load(object sender, EventArgs e)
        {
            if (ID == 0)
            {
                partners_paymentsBindingSource.AddNew();
              
                if (PartnerID > 0)
                {
                    ((DataRowView)partners_paymentsBindingSource.Current)["partners_ID"] = PartnerID;
                    ((DataRowView)partners_paymentsBindingSource.Current)["dat"] = DateTime.Now;
                    ((DataRowView)partners_paymentsBindingSource.Current)["Deleted"] = 0;              
                }
                if (StartUpAmount > 0) priceDoubleInput.Value = StartUpAmount;

            }
            else
            {
                this.partners_paymentsTableAdapter.FillByID(this.gama_transportationdbDataSet.partners_payments, ID);
                PartnerID = this.gama_transportationdbDataSet.partners_payments.FirstOrDefault().partners_ID;
                this.discountlinesTableAdapter.FillByPaymentID(this.gama_transportationdbDataSet.discountlines, ID);
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ID == 0)
                {
                    bool SendingLocal = DelivererOrderID.GetValueOrDefault(0) > 0 ? true : false;
                    if ((DelivererOrderID.GetValueOrDefault(0)+SendingOrder.GetValueOrDefault(0)) > 0)
                    {
                        new PaymentRepository().AddPaymentToOrder(SendingLocal,SendingLocal?DelivererOrderID.Value:SendingOrder.Value, DelivererOrderYear.GetValueOrDefault(0),
                            Convert.ToDecimal(priceDoubleInput.Value),
                            0,
                            datDateTimeInput.Value,
                            NoteBox.Text);
                    }
                    else
                    {
                        new PaymentRepository().AddPayment(
                            PartnerID,
                            Convert.ToDecimal(priceDoubleInput.Value),
                            0,
                            datDateTimeInput.Value,
                            NoteBox.Text);
                    }
                    //partners_paymentsTableAdapter.InsertQuery(datDateTimeInput.Value, Convert.ToDecimal(priceDoubleInput.Value), PartnerID, NoteBox.Text, DelivererOrderID, DelivererOrderYear,SendingOrder);
                }
                else
                {
                    new PaymentRepository().EditPayment(ID, Convert.ToDecimal(priceDoubleInput.Value), 0, datDateTimeInput.Value, NoteBox.Text);
                    //partners_paymentsTableAdapter.UpdateQuery(datDateTimeInput.Value, Convert.ToDecimal(priceDoubleInput.Value),NoteBox.Text, ID);
                }
             
                if (discountBox.Value > 0 && discountlinesBindingSource.Count==0)
                {
                    discountlinesTableAdapter.Insert(Convert.ToDecimal(discountBox.Value), null, null, PartnerID, 0, partners_paymentsTableAdapter.MaxID(), datDateTimeInput.Value);
                }
                else
                {
                    discountlinesTableAdapter.UpdateQuery(Convert.ToDecimal(discountBox.Value), datDateTimeInput.Value, ID);
                }

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }

    }
}