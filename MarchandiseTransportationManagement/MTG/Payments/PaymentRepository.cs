﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTM.Payments
{
    public class PaymentRepository
    {
        public PaymentRepository()
        {

        }



        public bool AddPaymentToOrder(bool SendingLocal,int OrderID,int YearID, decimal amount, decimal discount, DateTime date, string note = null)
        {
            gama_transportationdbDataSet.package_delivrer_orderRow lorder;
            gama_transportationdbDataSet.packages_orderRow sorder;
            if (OrderID <= 0)
            {
                return false;
            }

            if (amount <= 0)
            {
                return false;
            }
            decimal fullAmount = amount;
            gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter payAdapter = new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter();
            gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter orderAdapter = new gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter();
            gama_transportationdbDataSetTableAdapters.packages_order1TableAdapter sorderAdapter = new gama_transportationdbDataSetTableAdapters.packages_order1TableAdapter();

            int partnerId = 0;
            if (SendingLocal)
            {
                lorder = orderAdapter.GetDataByID(OrderID, YearID).FirstOrDefault(); partnerId = lorder.partners_ID;

                if (amount > lorder.ResteToPay)
                {
                    payAdapter.InsertQuery(date, lorder.ResteToPay, partnerId, note + "," + fullAmount, lorder.ID, lorder.SerialYear, null);
                    orderAdapter.UpdateResteToPay(0, lorder.ID, lorder.SerialYear);
                    amount -= lorder.ResteToPay;
                }
                else if (amount <= lorder.ResteToPay)
                {
                    // insert payment
                    payAdapter.InsertQuery(date, amount, partnerId, note + "," + fullAmount, lorder.ID, lorder.SerialYear, null);
                    // update order reste
                    orderAdapter.UpdateResteToPay(lorder.ResteToPay - amount, lorder.ID, lorder.SerialYear);
                    amount -= amount;
                }
            }
            else
            {
                sorder = sorderAdapter.GetDataByID(OrderID).FirstOrDefault(); partnerId = sorder.customers_ID;
                var reste = sorder.Total - sorder.TotalPayments;
                if (amount > reste)
                {
                    sorderAdapter.UpdateTotalPayment(sorder.Total, sorder.ID);
                    payAdapter.InsertQuery(date, reste, partnerId, note + "," + fullAmount, null, null, sorder.ID);
                    amount -= reste;
                }
                else 
                {
                    sorderAdapter.UpdateTotalPayment(sorder.TotalPayments+amount, sorder.ID);
                    payAdapter.InsertQuery(date, reste, partnerId, note + "," + fullAmount, null, null, sorder.ID);
                    amount -= amount;
                }
            }

            if (amount > 0)
            {
                AddPayment(partnerId, amount, 0, date, note + "," + fullAmount);
            }
            
            // Check if there is balance in delivery Order
            return true;

        }

        public bool AddPayment(int partnerId, decimal amount,decimal discount ,DateTime date,string note = null)
        {
            if (partnerId <= 0)
            {
                return false;
            }
            if (amount <= 0)
            {
                return false;
            }
            decimal fullAmount = amount;

            gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter payAdapter = new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter();
            gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter orderAdapter = new gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter();
                       
            // Check if there is balance in delivery Order
            var upayedOrders = new gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter() {}.GetUnpayedOrders(partnerId);
            
            foreach (var order in upayedOrders)
            {
                if (amount > order.ResteToPay)
                {
                    // insert payment
                    payAdapter.InsertQuery(date, order.ResteToPay, partnerId, note + "," + fullAmount, order.ID, order.SerialYear, null);
                    // update order reste
                    orderAdapter.UpdateResteToPay(0, order.ID, order.SerialYear);
                    amount -= order.ResteToPay; 
                }
                else if (amount <= order.ResteToPay )
                {
                    // insert payment
                    payAdapter.InsertQuery(date, amount, partnerId, note + "," + fullAmount, order.ID, order.SerialYear, null);
                    // update order reste
                    orderAdapter.UpdateResteToPay(order.ResteToPay-amount, order.ID, order.SerialYear);

                    amount -= amount;
                    break;
                }
            }
            
            // Check if there is Sending  Orders. later ******************************************************

            if (amount > 0)
            {
                // insert advance payment.
                payAdapter.InsertQuery(date, amount, partnerId, note + "," + fullAmount, null, null, null);
            }


            if (amount == 0) return true;
            else return false;
            
        }

        public bool EditPayment(int paymentID, decimal amount, decimal discount, DateTime date, string note = null)
        {
            if (paymentID <= 0) return false;

            gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter payAdapter = new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter();
            gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter orderAdapter = new gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter();

            gama_transportationdbDataSet.partners_paymentsRow Payment = payAdapter.GetDataByID(paymentID).FirstOrDefault();
            if (Payment != null)
            {
                decimal amountDiff = amount - Payment.Amount;

                if (amountDiff !=0 && !Payment.Isdeliverer_Order_IDNull())
                {
                    var order = orderAdapter.GetDataByID(Payment.deliverer_Order_ID, Payment.deliverer_Order_SerialYear).FirstOrDefault();

                    // 
                    if (amountDiff > order.ResteToPay)
                    {
                        orderAdapter.UpdateResteToPay(0, order.ID, order.SerialYear);
                        payAdapter.UpdateQuery(date,Payment.Amount+order.ResteToPay,note,paymentID);
                        amountDiff -= order.ResteToPay;
                    }
                    else if (amountDiff <= order.ResteToPay)
                    {
                        orderAdapter.UpdateResteToPay(order.ResteToPay - amountDiff, order.ID, order.SerialYear);
                        payAdapter.UpdateQuery(date, Payment.Amount + amountDiff, note, paymentID);
                    }

                    if (amountDiff > 0)
                    {
                        return AddPayment(Payment.partners_ID, amountDiff, 0, date, note + ", " + amount);
                    }
                }

                return true;
            }
            else return false;
        }

        public bool DeletePayment(int paymentID)
        {
            if (paymentID <= 0) return false;

            gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter payAdapter = new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter();
            gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter orderAdapter = new gama_transportationdbDataSetTableAdapters.package_delivrer_orderTableAdapter();
            gama_transportationdbDataSetTableAdapters.packages_orderTableAdapter sendOrderAdapter = new gama_transportationdbDataSetTableAdapters.packages_orderTableAdapter();

            gama_transportationdbDataSet.partners_paymentsRow Payment = payAdapter.GetDataByID(paymentID).FirstOrDefault();
            if (Payment != null)
            {
                if (payAdapter.DeleteQuery(paymentID) > 0)
                {
                    if (!Payment.Isdeliverer_Order_IDNull())
                    {
                        var order = orderAdapter.GetDataByID(Payment.deliverer_Order_ID, Payment.deliverer_Order_SerialYear).FirstOrDefault();
                        orderAdapter.UpdateResteToPay(order.ResteToPay + Payment.Amount, order.ID, order.SerialYear);
                    }
                    else if (!Payment.Issending_OrderIDNull())
                    {
                        var sorder = sendOrderAdapter.GetDataByID(Payment.sending_OrderID).FirstOrDefault();
                        sendOrderAdapter.UpdateTotalPayment(sorder.TotalPayments-Payment.Amount, sorder.ID);
                    }
                }
                return true;
            }
            else return false;
        }
    }
}
