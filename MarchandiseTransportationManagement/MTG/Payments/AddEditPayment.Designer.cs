﻿namespace MTM.Payments
{
    partial class AddEditPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.priceDoubleInput = new DevComponents.Editors.DoubleInput();
            this.partners_paymentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gama_transportationdbDataSet = new MTM.gama_transportationdbDataSet();
            this.partners_paymentsTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter();
            this.tableAdapterManager = new MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager();
            this.datDateTimeInput = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.discountBox = new DevComponents.Editors.DoubleInput();
            this.discountlinesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.discountlinesTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.discountlinesTableAdapter();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.NoteBox = new DevComponents.DotNetBar.Controls.TextBoxX();
            ((System.ComponentModel.ISupportInitialize)(this.priceDoubleInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.partners_paymentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datDateTimeInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountlinesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(335, 12);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(104, 27);
            this.labelX2.TabIndex = 4;
            this.labelX2.Text = "تاريخ الدفع:";
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(12, 144);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(111, 32);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.Symbol = "";
            this.btnCancel.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "إلغاء الأمر";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSave.Location = new System.Drawing.Point(129, 144);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(127, 32);
            this.btnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSave.Symbol = "";
            this.btnSave.SymbolColor = System.Drawing.Color.Green;
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "حفظ";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(335, 45);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(104, 27);
            this.labelX1.TabIndex = 4;
            this.labelX1.Text = "المبلغ المدفوع:";
            // 
            // priceDoubleInput
            // 
            this.priceDoubleInput.AllowEmptyState = false;
            this.priceDoubleInput.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.priceDoubleInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.priceDoubleInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.priceDoubleInput.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.priceDoubleInput.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.partners_paymentsBindingSource, "Amount", true));
            this.priceDoubleInput.FocusHighlightEnabled = true;
            this.priceDoubleInput.ForeColor = System.Drawing.Color.Black;
            this.priceDoubleInput.Increment = 1D;
            this.priceDoubleInput.Location = new System.Drawing.Point(195, 45);
            this.priceDoubleInput.MaxValue = 99999999D;
            this.priceDoubleInput.MinValue = -99999999D;
            this.priceDoubleInput.Name = "priceDoubleInput";
            this.priceDoubleInput.Size = new System.Drawing.Size(134, 27);
            this.priceDoubleInput.TabIndex = 1;
            // 
            // partners_paymentsBindingSource
            // 
            this.partners_paymentsBindingSource.DataMember = "partners_payments";
            this.partners_paymentsBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // gama_transportationdbDataSet
            // 
            this.gama_transportationdbDataSet.DataSetName = "gama_transportationdbDataSet";
            this.gama_transportationdbDataSet.EnforceConstraints = false;
            this.gama_transportationdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // partners_paymentsTableAdapter
            // 
            this.partners_paymentsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.deliverersTableAdapter = null;
            this.tableAdapterManager.discountlinesTableAdapter = null;
            this.tableAdapterManager.discountsTableAdapter = null;
            this.tableAdapterManager.expensesTableAdapter = null;
            this.tableAdapterManager.locationsTableAdapter = null;
            this.tableAdapterManager.operationsTableAdapter = null;
            this.tableAdapterManager.package_delivrer_orderTableAdapter = null;
            this.tableAdapterManager.package_order_expensesTableAdapter = null;
            this.tableAdapterManager.package_order_payments1TableAdapter = null;
            this.tableAdapterManager.package_order_paymentsTableAdapter = null;
            this.tableAdapterManager.packages_order_line_delivererTableAdapter = null;
            this.tableAdapterManager.packages_order_linesTableAdapter = null;
            this.tableAdapterManager.packages_orderTableAdapter = null;
            this.tableAdapterManager.partners_paymentsTableAdapter = this.partners_paymentsTableAdapter;
            this.tableAdapterManager.partners_rcTableAdapter = null;
            this.tableAdapterManager.partnersTableAdapter = null;
            this.tableAdapterManager.payment_methodsTableAdapter = null;
            this.tableAdapterManager.productsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.workers_absenceTableAdapter = null;
            this.tableAdapterManager.workers_accountTableAdapter = null;
            this.tableAdapterManager.workers_paymentsTableAdapter = null;
            this.tableAdapterManager.workersTableAdapter = null;
            // 
            // datDateTimeInput
            // 
            this.datDateTimeInput.AllowEmptyState = false;
            this.datDateTimeInput.AutoAdvance = true;
            this.datDateTimeInput.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.datDateTimeInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.datDateTimeInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.datDateTimeInput.ButtonDropDown.Visible = true;
            this.datDateTimeInput.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.partners_paymentsBindingSource, "dat", true));
            this.datDateTimeInput.FocusHighlightEnabled = true;
            this.datDateTimeInput.ForeColor = System.Drawing.Color.Black;
            this.datDateTimeInput.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center;
            this.datDateTimeInput.IsPopupCalendarOpen = false;
            this.datDateTimeInput.Location = new System.Drawing.Point(195, 12);
            // 
            // 
            // 
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.datDateTimeInput.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.DisplayMonth = new System.DateTime(2018, 12, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.TodayButtonVisible = true;
            this.datDateTimeInput.Name = "datDateTimeInput";
            this.datDateTimeInput.Size = new System.Drawing.Size(134, 27);
            this.datDateTimeInput.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.datDateTimeInput.TabIndex = 0;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(335, 78);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(104, 27);
            this.labelX3.TabIndex = 4;
            this.labelX3.Text = "التخفيض:";
            // 
            // discountBox
            // 
            this.discountBox.AllowEmptyState = false;
            this.discountBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.discountBox.BackgroundStyle.Class = "DateTimeInputBackground";
            this.discountBox.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.discountBox.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.discountBox.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.discountlinesBindingSource, "Amount", true));
            this.discountBox.FocusHighlightEnabled = true;
            this.discountBox.ForeColor = System.Drawing.Color.Black;
            this.discountBox.Increment = 1D;
            this.discountBox.Location = new System.Drawing.Point(195, 78);
            this.discountBox.MaxValue = 99999999D;
            this.discountBox.MinValue = 0D;
            this.discountBox.Name = "discountBox";
            this.discountBox.Size = new System.Drawing.Size(134, 27);
            this.discountBox.TabIndex = 2;
            // 
            // discountlinesBindingSource
            // 
            this.discountlinesBindingSource.DataMember = "discountlines";
            this.discountlinesBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // discountlinesTableAdapter
            // 
            this.discountlinesTableAdapter.ClearBeforeFill = true;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(335, 111);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(104, 27);
            this.labelX4.TabIndex = 4;
            this.labelX4.Text = "ملاحظة:";
            // 
            // NoteBox
            // 
            this.NoteBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.NoteBox.Border.Class = "TextBoxBorder";
            this.NoteBox.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.NoteBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.partners_paymentsBindingSource, "Note", true));
            this.NoteBox.DisabledBackColor = System.Drawing.Color.White;
            this.NoteBox.ForeColor = System.Drawing.Color.Black;
            this.NoteBox.Location = new System.Drawing.Point(12, 111);
            this.NoteBox.Name = "NoteBox";
            this.NoteBox.PreventEnterBeep = true;
            this.NoteBox.Size = new System.Drawing.Size(317, 27);
            this.NoteBox.TabIndex = 5;
            // 
            // AddEditPayment
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(472, 187);
            this.ControlBox = false;
            this.Controls.Add(this.NoteBox);
            this.Controls.Add(this.datDateTimeInput);
            this.Controls.Add(this.discountBox);
            this.Controls.Add(this.priceDoubleInput);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "AddEditPayment";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تسجيل دفعة";
            this.Load += new System.EventHandler(this.AddEditProduct_Load);
            ((System.ComponentModel.ISupportInitialize)(this.priceDoubleInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.partners_paymentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datDateTimeInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountlinesBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.Editors.DoubleInput priceDoubleInput;
        private gama_transportationdbDataSet gama_transportationdbDataSet;
        private System.Windows.Forms.BindingSource partners_paymentsBindingSource;
        private gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter partners_paymentsTableAdapter;
        private gama_transportationdbDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput datDateTimeInput;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.Editors.DoubleInput discountBox;
        private System.Windows.Forms.BindingSource discountlinesBindingSource;
        private gama_transportationdbDataSetTableAdapters.discountlinesTableAdapter discountlinesTableAdapter;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX NoteBox;
    }
}