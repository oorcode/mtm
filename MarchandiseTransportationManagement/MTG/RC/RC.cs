using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.RC
{
    public partial class RC : DevComponents.DotNetBar.Metro.MetroForm
    {
        public RC()
        {
            InitializeComponent();
        }

        private void partners_rcBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.partners_rcBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.gama_transportationdbDataSet);
                int pos = partners_rcBindingSource.Position;
                this.partners_rcTableAdapter.Fill(this.gama_transportationdbDataSet.partners_rc);
                partners_rcBindingSource.Position = pos;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void RC_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.partners_rc' table. You can move, or remove it, as needed.
            this.partners_rcTableAdapter.Fill(this.gama_transportationdbDataSet.partners_rc);

        }

        private void textBoxX1_TextChanged(object sender, EventArgs e)
        {
            partners_rcBindingSource.Filter = "RC LIKE '%" + textBoxX1.Text + "%' OR CompanyName LIKE '%" + textBoxX1.Text + "%' OR CompanyPhone LIKE '%" + textBoxX1.Text + "%' OR Note LIKE '%" + textBoxX1.Text + "%'";
        }
    }
}