using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Products
{
    public partial class ProductsList : UserControl,IIUserControl
    {
        public ProductsList()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (new Products.AddEditProduct().ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
            Refill1();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (productsBindingSource.Position < 0) return;
            if (new Products.AddEditProduct(Convert.ToInt32(((DataRowView)productsBindingSource.Current)["ID"])).ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
            Refill1();
        }

        private void ProductsList_Load(object sender, EventArgs e)
        {
            this.partnersTableAdapter.Fill(this.gama_transportationdbDataSet.partners);
            Refill1();
        }

        private void Refill1()
        {
            int pos = productsBindingSource.Position;
            this.productsTableAdapter.Fill(this.gama_transportationdbDataSet.products);
            productsBindingSource.Position = pos;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
        }

        private void mainDataGridViewX_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (productsBindingSource.Position < 0) return;
            if (MessageBox.Show("����� ��� ������ �����Ͽ", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            try
            {
                productsTableAdapter.DeleteQuery(Convert.ToInt32(((DataRowView)productsBindingSource.Current)["ID"]));
                productsBindingSource.RemoveCurrent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

       public  void Refill()
        {
            Refill1();
        }
    }
}