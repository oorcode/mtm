using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Products
{
    public partial class AddEditProduct : DevComponents.DotNetBar.Metro.MetroForm
    {
        int ID = 0;
        private int factoryID;
        public AddEditProduct(int Id_=0, int factorYID=0)
        {
            InitializeComponent();
            ID = Id_;
            factoryID = factorYID;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.productsBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(gama_transportationdbDataSet);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }

        private void AddEditProduct_Load(object sender, EventArgs e)
        {
            if (ID == 0)
            {
                productsBindingSource.AddNew();
                if (factoryID > 0)
                {
                    ((DataRowView)productsBindingSource.Current)["partners_ID"] = factoryID;
                }
            }
            else
            {
                this.productsTableAdapter.FillByID(this.gama_transportationdbDataSet.products,ID);
            }
        }
    }
}