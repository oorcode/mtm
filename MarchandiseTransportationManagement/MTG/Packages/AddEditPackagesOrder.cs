using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Packages
{
    public partial class AddEditPackagesOrder : DevComponents.DotNetBar.Metro.MetroForm
    {
        public int PackageOrderID { get; set; }
        public bool IsFactory { get; set; }
        public int PartnerID { get; set; }
        public int DestinationPartnerID { get; set; }
        
        gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter partners_paymentsTableAdapter;
        public AddEditPackagesOrder()
        {
            InitializeComponent();
        }

        private void AddEditPackagesOrder_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.payment_methods' table. You can move, or remove it, as needed.
            this.payment_methodsTableAdapter.Fill(this.gama_transportationdbDataSet.payment_methods);
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.partners_payments' table. You can move, or remove it, as needed.
            this.partners_paymentsTableAdapter1.FillBySendingOrderID(this.gama_transportationdbDataSet.partners_payments, PackageOrderID, PartnerID);
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.package_order_payments' table. You can move, or remove it, as needed.
            this.package_order_paymentsTableAdapter1.FillBySending_OrderID(this.gama_transportationdbDataSet.package_order_payments,PackageOrderID);
            
            partners_paymentsTableAdapter = new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter();

            this.packages_orderTableAdapter.FillByID(this.gama_transportationdbDataSet.packages_order,PackageOrderID);
            var partner = new gama_transportationdbDataSetTableAdapters.partnersTableAdapter().GetDataByID(PartnerID).FirstOrDefault();
            partnerNameLabel.Text = partner.CustomerName;

            if (IsFactory)
            {
                this.productsTableAdapter.FillByPartnerID(this.gama_transportationdbDataSet.products, PartnerID);
            }
            else
            {
                this.productsTableAdapter.FillByNullPartner(this.gama_transportationdbDataSet.products);
            }
           
            this.packages_order_linesTableAdapter.FillByOrderID(this.gama_transportationdbDataSet.packages_order_lines, PackageOrderID);

            DestinationPartnerID = new gama_transportationdbDataSetTableAdapters.operationsTableAdapter().GetDestinationPartnerID(PackageOrderID).GetValueOrDefault(0);
            
            Ok();
        }

        private void mainDataGridViewX_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void btnAddLine_Click(object sender, EventArgs e)
        {
            Ok();
        }

        private void Ok()
        {
            if (productsBindingSource.Position < 0) return;
            var add = new AddEditPackageOrderLine();
            add.PackageOrderID = PackageOrderID;
            add.ProductID = Convert.ToInt32(((DataRowView)productsBindingSource.Current)["ID"]);
            add.IsLeaving = IsLeaving;
            add.IsFactory = IsFactory;
            add.LineID = 0;
            if (add.ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
            this.packages_order_linesTableAdapter.FillByOrderID(this.gama_transportationdbDataSet.packages_order_lines, PackageOrderID);
            CalculateTotal();
        }

        private void CalculateTotal(bool delay = true)
        {
            Timer timer = new Timer();
            timer.Interval = delay?400:1;
            timer.Start();
            timer.Tick += (ee, rr) =>
            {
                timer.Stop();
                var totExpenses = gama_transportationdbDataSet.package_order_payments.Sum(x => x.Amount);
                var totPackages = gama_transportationdbDataSet.packages_order_lines.Sum(x => x.Quantity * x.Price);
                var expenses = gama_transportationdbDataSet.PackageOrderExpensesView.Sum(x => x.Amount);
                totalPackagesBox.Value = Convert.ToDouble(totPackages);
                ExpensesBox.Value = Convert.ToDouble(totExpenses);
                doubleInput2.Value = totalPackagesBox.Value - discountBox.Value+ ExpensesBox.Value;
                
            };
        }

        private void btnEditLine_Click(object sender, EventArgs e)
        {
            EditOrderLine();
            CalculateTotal();
        }

        private void EditOrderLine()
        {
            if (packages_order_linesBindingSource.Position < 0) return;
            var add = new AddEditPackageOrderLine();
            add.PackageOrderID = PackageOrderID;
            add.LineID = Convert.ToInt32(((DataRowView)packages_order_linesBindingSource.Current)["ID"]);
            add.ProductID = Convert.ToInt32(((DataRowView)packages_order_linesBindingSource.Current)["products_ID"]);
            add.IsLeaving = IsLeaving;
            add.IsFactory = IsFactory;
            if (add.ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
            this.packages_order_linesTableAdapter.FillByOrderID(this.gama_transportationdbDataSet.packages_order_lines, PackageOrderID);
        }

        private void ProductSearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyData == Keys.Down)
            {
                productsBindingSource.MoveNext();
            }
            else if (e.KeyData == Keys.Up)
            {
                productsBindingSource.MovePrevious();
            }
            else if (e.KeyData == Keys.Enter)
            {
                Ok();
            }
        }
       
        private void btnDeleteLine_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("����� ����� ��������", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
            packages_order_linesTableAdapter.DeleteQuery(Convert.ToInt32(((DataRowView)packages_order_linesBindingSource.Current)["ID"]));
            this.packages_order_linesTableAdapter.FillByOrderID(this.gama_transportationdbDataSet.packages_order_lines, PackageOrderID);
            CalculateTotal();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

        }       

        private void packages_order_linesBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            labelX12.Text = packages_order_linesBindingSource.Count.ToString();
            labelX13.Text = gama_transportationdbDataSet.packages_order_lines.Sum(x => x.Quantity).ToString();
            CalculateTotal();
        }

        private void ProductsdataGridViewX1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Ok();
        }

        private void mainDataGridViewX_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            EditOrderLine();
        }

        private void btnAddOrder_Click(object sender, EventArgs e)
        {

        }

        private void btnAddOrderFactory_Click(object sender, EventArgs e)
        {
            var select = new Partners.SelectPartner();
            select.IsFactory = true;
            select.IsMerchant = false;
            select.IsWarehouse = false;

            if (select.ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
            IsFactory = true;
            PartnerID = select.SelectedPartnerID;
            var packages_order = gama_transportationdbDataSet.packages_order.FirstOrDefault();
            var operation = new gama_transportationdbDataSetTableAdapters.operationsTableAdapter().GetDataByID(packages_order.Operations_ID).FirstOrDefault();
          
            var packageAdapter = new gama_transportationdbDataSetTableAdapters.packages_orderTableAdapter();
            packageAdapter.InsertQuery(packages_order.Operations_ID, select.SelectedPartnerID, 0, 0, 0, 0, 0, DateTime.Now);
            PackageOrderID = packageAdapter.MaxID().GetValueOrDefault(0);

            AddEditPackagesOrder_Load(null, null);
            CalculateTotal();
        }

        private void btnAddOrderMerchant_Click(object sender, EventArgs e)
        {
            var select = new Partners.SelectPartner();
            select.IsFactory = false;
            select.IsMerchant = true;
            select.IsWarehouse = false;
            if (select.ShowDialog(this) == System.Windows.Forms.DialogResult.Cancel) return;
            IsFactory = false;
            PartnerID = select.SelectedPartnerID;
            var packages_order = gama_transportationdbDataSet.packages_order.FirstOrDefault();
            var operation = new gama_transportationdbDataSetTableAdapters.operationsTableAdapter().GetDataByID(packages_order.Operations_ID).FirstOrDefault();

            var packageAdapter = new gama_transportationdbDataSetTableAdapters.packages_orderTableAdapter();
            packageAdapter.InsertQuery(packages_order.Operations_ID, select.SelectedPartnerID, 0, 0, 0, 0, operation.IsLeaving ? 0 : 1, DateTime.Now);
            PackageOrderID = packageAdapter.MaxID().GetValueOrDefault(0);

            AddEditPackagesOrder_Load(null, null);
            CalculateTotal();
        }

        public bool IsLeaving { get; set; }

        private void discountBox_Leave(object sender, EventArgs e)
        {
            CalculateTotal();
        }

        private void discountBox_Enter(object sender, EventArgs e)
        {
            CalculateTotal();
        }
      
        private void btnExit_Click(object sender, EventArgs e)
        {
            CalculateTotal(false);
            try
            {
                this.Validate();
                this.packages_order_linesBindingSource.EndEdit();
                this.packages_orderBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(gama_transportationdbDataSet);
                this.Close();
            }
            catch (Exception ex)
            {
                this.packages_orderTableAdapter.FillByID(this.gama_transportationdbDataSet.packages_order, PackageOrderID);
                var partner = new gama_transportationdbDataSetTableAdapters.partnersTableAdapter().GetDataByID(PartnerID).FirstOrDefault();
                partnerNameLabel.Text = partner.CustomerName;

                if (IsFactory)
                {
                    this.productsTableAdapter.FillByPartnerID(this.gama_transportationdbDataSet.products, PartnerID);
                }
                else
                {
                    this.productsTableAdapter.FillByNullPartner(this.gama_transportationdbDataSet.products);
                }
                this.packages_order_linesTableAdapter.FillByOrderID(this.gama_transportationdbDataSet.packages_order_lines, PackageOrderID);
                
            }
        }

        private void AddEditPackagesOrder_FormClosing(object sender, FormClosingEventArgs e)
        {
            var totExpenses = gama_transportationdbDataSet.package_order_payments.Sum(x => x.Amount); 
            var totPackages = gama_transportationdbDataSet.packages_order_lines.Sum(x => x.Quantity * x.Price);
            var expenses = gama_transportationdbDataSet.PackageOrderExpensesView.Sum(x => x.Amount);
            totalPackagesBox.Value = Convert.ToDouble(totPackages);
            ExpensesBox.Value = Convert.ToDouble(totExpenses);
            doubleInput2.Value = totalPackagesBox.Value - discountBox.Value + ExpensesBox.Value;
            try
            {
                this.Validate();
                this.packages_orderBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(gama_transportationdbDataSet);
            }
            catch (Exception)
            {
            }
        }

        private void mainDataGridViewX_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.Validate();
            this.packages_order_linesBindingSource.EndEdit();
            this.packages_orderBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(gama_transportationdbDataSet);
            this.packages_orderTableAdapter.FillByID(this.gama_transportationdbDataSet.packages_order, PackageOrderID);
            this.packages_order_linesTableAdapter.FillByOrderID(this.gama_transportationdbDataSet.packages_order_lines, PackageOrderID);
        }

        private void mainDataGridViewX_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == PayedColumn.Index)
            {
                //this.Validate();
                this.packages_order_linesBindingSource.EndEdit();
                //this.packages_orderBindingSource.EndEdit();
                //this.tableAdapterManager.UpdateAll(gama_transportationdbDataSet);
                //this.packages_orderTableAdapter.FillByID(this.gama_transportationdbDataSet.packages_order, PackageOrderID);
                //this.packages_order_linesTableAdapter.FillByOrderID(this.gama_transportationdbDataSet.packages_order_lines, PackageOrderID);
            }
        }

        private void btnPrint_Click_1(object sender, EventArgs e)
        {

        }

        private void TotalPackAmountDepot_ValueChanged(object sender, EventArgs e)
        {
            CalculateTotal();
        }

        private void discountBox_ValueChanged(object sender, EventArgs e)
        {
            CalculateTotal(false);
        }

        private void btnAddPayment_Click(object sender, EventArgs e)
        {
            int id =  PartnerID;
            new Payments.AddEditPayment(0, id) { StartUpAmount = doubleInput2.Value, DelivererOrderID = null, DelivererOrderYear = null,SendingOrder=PackageOrderID }.ShowDialog();
            this.partners_paymentsTableAdapter1.FillBySendingOrderID(this.gama_transportationdbDataSet.partners_payments, PackageOrderID, PartnerID);
            CalculateTotal();
        }

        private void btnPrintt_Click(object sender, EventArgs e)
        {
            if (packages_orderBindingSource.Position < 0) return;
            new Packages.Reports.PackageOrderReportForm() { OrderID = PackageOrderID }.ShowDialog(this);
        }

        private void btnAddExpences_Click(object sender, EventArgs e)
        {
            new Expenses.AddEditPackageOrderExpense() { PackageOrderID = PackageOrderID, DepotID = DestinationPartnerID, OrderPaymentID = 0 }.ShowDialog();
            this.package_order_paymentsTableAdapter1.FillBySending_OrderID(this.gama_transportationdbDataSet.package_order_payments , PackageOrderID);
            CalculateTotal();
        }

        private void btnEditExpenses_Click(object sender, EventArgs e)
        {
            if (package_order_paymentsBindingSource.Position < 0) return;
            new Expenses.AddEditPackageOrderExpense() { PackageOrderID = PackageOrderID, DepotID = DestinationPartnerID, OrderPaymentID = (int)((DataRowView)package_order_paymentsBindingSource.Current)["ID"] }.ShowDialog();
            this.package_order_paymentsTableAdapter1.FillBySending_OrderID(this.gama_transportationdbDataSet.package_order_payments, PackageOrderID);
            CalculateTotal();

        }

        private void btnDeleteExpenses_Click(object sender, EventArgs e)
        {
            if (package_order_paymentsBindingSource.Position < 0) return;
            if (MessageBox.Show("����� ����� ��������", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
            var partnerPaymentID = (int)((DataRowView)package_order_paymentsBindingSource.Current)["partners_payments_ID"];

            package_order_paymentsTableAdapter1.DeleteQuery((int)((DataRowView)package_order_paymentsBindingSource.Current)["ID"]);
            partners_paymentsTableAdapter.DeleteQuery(partnerPaymentID);
            this.package_order_paymentsTableAdapter1.FillBySending_OrderID(this.gama_transportationdbDataSet.package_order_payments, PackageOrderID);
            CalculateTotal();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.packages_order_linesBindingSource.EndEdit();
                this.packages_orderBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(gama_transportationdbDataSet);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            this.packages_orderTableAdapter.FillByID(this.gama_transportationdbDataSet.packages_order, PackageOrderID);
            var partner = new gama_transportationdbDataSetTableAdapters.partnersTableAdapter().GetDataByID(PartnerID).FirstOrDefault();
            partnerNameLabel.Text = partner.CustomerName;

            if (IsFactory)
            {
                this.productsTableAdapter.FillByPartnerID(this.gama_transportationdbDataSet.products, PartnerID);
            }
            else
            {
                this.productsTableAdapter.FillByNullPartner(this.gama_transportationdbDataSet.products);
            }
            this.packages_order_linesTableAdapter.FillByOrderID(this.gama_transportationdbDataSet.packages_order_lines, PackageOrderID);
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            if (partners_paymentsBindingSource.Position < 0) return;
            int id = PartnerID;
            int idpp = (int)((DataRowView)partners_paymentsBindingSource.Current)["ID"];
            new Payments.AddEditPayment(idpp, id) { StartUpAmount = doubleInput2.Value, DelivererOrderID = null, DelivererOrderYear = null, SendingOrder = PackageOrderID }.ShowDialog();
            this.partners_paymentsTableAdapter1.FillBySendingOrderID(this.gama_transportationdbDataSet.partners_payments, PackageOrderID, PartnerID);
            CalculateTotal();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            if (partners_paymentsBindingSource.Position < 0) return;
            if (MessageBox.Show("����� ����� ��������", "�����", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No) return;
            int idpp = (int)((DataRowView)partners_paymentsBindingSource.Current)["ID"];
            partners_paymentsTableAdapter1.DeleteQuery(idpp);
            this.partners_paymentsTableAdapter1.FillBySendingOrderID(this.gama_transportationdbDataSet.partners_payments, PackageOrderID,PartnerID);
            CalculateTotal();
        }

        private void Payed_CheckedChanged(object sender, EventArgs e)
        {
           
        }
    }
}