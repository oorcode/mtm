 using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Packages.Expenses
{
    public partial class AddEditPackageOrderExpense : DevComponents.DotNetBar.Metro.MetroForm
    {
        public int OrderPaymentID { get; set; }
        public int? PackageOrderID { get; set; }
        public int DepotID { get; set; }
        public AddEditPackageOrderExpense()
        {
            InitializeComponent();
        }

        private void AddEditPackageOrderLine_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gama_transportationdbDataSet.package_order_payments' table. You can move, or remove it, as needed.
            this.package_order_paymentsTableAdapter.FillByID(this.gama_transportationdbDataSet.package_order_payments, OrderPaymentID);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var ppayAdapter = new gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter();
            if (OrderPaymentID == 0)
            {
                try
                {
                    var amount = Convert.ToDecimal(AmountDoubleInput.Value);
                 
                    ppayAdapter.InsertQuery(datDateTimeInput.Value, amount, DepotID, textBoxX1.Text, null, null, PackageOrderID);
                    int PaymentID = ppayAdapter.MaxID().GetValueOrDefault(0);
                    new gama_transportationdbDataSetTableAdapters.package_order_paymentsTableAdapter()
                        .InsertQuery1(datDateTimeInput.Value, amount, PaymentID, PackageOrderID, textBoxX1.Text);
                }
                catch (Exception ex)
                {
                    MessageBoxEx.Show(ex.ToString());
                }
            }
            else
            {
                var OrderPaymentLine = gama_transportationdbDataSet.package_order_payments.FirstOrDefault();
                var amount = Convert.ToDecimal(AmountDoubleInput.Value);
               
                ppayAdapter.UpdateQuery(datDateTimeInput.Value,amount,textBoxX1.Text, OrderPaymentLine.partners_payments_ID);
                new gama_transportationdbDataSetTableAdapters.package_order_paymentsTableAdapter().UpdateQuery(datDateTimeInput.Value, amount, textBoxX1.Text, OrderPaymentID);
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
    }
}