﻿namespace MTM.Packages
{
    partial class AddEditPackageOrderLine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.designationTextBoxX = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.packages_order_linesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gama_transportationdbDataSet = new MTM.gama_transportationdbDataSet();
            this.priceDoubleInput = new DevComponents.Editors.DoubleInput();
            this.recipientTextBoxX = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.packages_order_linesTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.packages_order_linesTableAdapter();
            this.tableAdapterManager = new MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager();
            this.productsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productsTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.productsTableAdapter();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.ProductNameLblx = new DevComponents.DotNetBar.LabelX();
            this.noteTextBoxX = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.phonesTextBoxX1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.checNonPayed = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.checPayed = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.checRegistred = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.quantityDoubleInput = new DevComponents.Editors.DoubleInput();
            ((System.ComponentModel.ISupportInitialize)(this.packages_order_linesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceDoubleInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityDoubleInput)).BeginInit();
            this.SuspendLayout();
            // 
            // designationTextBoxX
            // 
            this.designationTextBoxX.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.designationTextBoxX.Border.Class = "TextBoxBorder";
            this.designationTextBoxX.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.designationTextBoxX.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.packages_order_linesBindingSource, "Designation", true));
            this.designationTextBoxX.DisabledBackColor = System.Drawing.Color.White;
            this.designationTextBoxX.FocusHighlightEnabled = true;
            this.designationTextBoxX.ForeColor = System.Drawing.Color.Black;
            this.designationTextBoxX.Location = new System.Drawing.Point(12, 216);
            this.designationTextBoxX.Name = "designationTextBoxX";
            this.designationTextBoxX.PreventEnterBeep = true;
            this.designationTextBoxX.Size = new System.Drawing.Size(280, 27);
            this.designationTextBoxX.TabIndex = 5;
            // 
            // packages_order_linesBindingSource
            // 
            this.packages_order_linesBindingSource.DataMember = "packages_order_lines";
            this.packages_order_linesBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // gama_transportationdbDataSet
            // 
            this.gama_transportationdbDataSet.DataSetName = "gama_transportationdbDataSet";
            this.gama_transportationdbDataSet.EnforceConstraints = false;
            this.gama_transportationdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // priceDoubleInput
            // 
            this.priceDoubleInput.AllowEmptyState = false;
            this.priceDoubleInput.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.priceDoubleInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.priceDoubleInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.priceDoubleInput.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.priceDoubleInput.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.packages_order_linesBindingSource, "Price", true));
            this.priceDoubleInput.FocusHighlightEnabled = true;
            this.priceDoubleInput.ForeColor = System.Drawing.Color.Black;
            this.priceDoubleInput.Increment = 1D;
            this.priceDoubleInput.Location = new System.Drawing.Point(163, 183);
            this.priceDoubleInput.Name = "priceDoubleInput";
            this.priceDoubleInput.ShowCheckBox = true;
            this.priceDoubleInput.Size = new System.Drawing.Size(129, 27);
            this.priceDoubleInput.TabIndex = 4;
            // 
            // recipientTextBoxX
            // 
            this.recipientTextBoxX.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.recipientTextBoxX.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.recipientTextBoxX.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.recipientTextBoxX.Border.Class = "TextBoxBorder";
            this.recipientTextBoxX.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.recipientTextBoxX.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.packages_order_linesBindingSource, "Recipient", true));
            this.recipientTextBoxX.DisabledBackColor = System.Drawing.Color.White;
            this.recipientTextBoxX.FocusHighlightEnabled = true;
            this.recipientTextBoxX.ForeColor = System.Drawing.Color.Black;
            this.recipientTextBoxX.Location = new System.Drawing.Point(48, 51);
            this.recipientTextBoxX.MaxLength = 255;
            this.recipientTextBoxX.Name = "recipientTextBoxX";
            this.recipientTextBoxX.PreventEnterBeep = true;
            this.recipientTextBoxX.Size = new System.Drawing.Size(244, 27);
            this.recipientTextBoxX.TabIndex = 0;
            this.recipientTextBoxX.KeyUp += new System.Windows.Forms.KeyEventHandler(this.recipientTextBoxX_KeyUp);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(298, 182);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(83, 27);
            this.labelX1.TabIndex = 54;
            this.labelX1.Text = "سعر الوحدة:";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(298, 215);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(83, 27);
            this.labelX2.TabIndex = 53;
            this.labelX2.Text = "التعييــــــــــــن:";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(298, 115);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(83, 27);
            this.labelX3.TabIndex = 53;
            this.labelX3.Text = "الكمية:";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(298, 49);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(83, 29);
            this.labelX4.TabIndex = 54;
            this.labelX4.Text = "المستلم:";
            this.labelX4.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.labelX4.WordWrap = true;
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnCancel.Location = new System.Drawing.Point(12, 282);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(111, 32);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.Symbol = "";
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "خروج";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnSave.Location = new System.Drawing.Point(129, 282);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(111, 32);
            this.btnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSave.Symbol = "";
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "حفظ";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // packages_order_linesTableAdapter
            // 
            this.packages_order_linesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.deliverersTableAdapter = null;
            this.tableAdapterManager.discountlinesTableAdapter = null;
            this.tableAdapterManager.discountsTableAdapter = null;
            this.tableAdapterManager.expensesTableAdapter = null;
            this.tableAdapterManager.locationsTableAdapter = null;
            this.tableAdapterManager.operationsTableAdapter = null;
            this.tableAdapterManager.package_delivrer_orderTableAdapter = null;
            this.tableAdapterManager.package_order_expensesTableAdapter = null;
            this.tableAdapterManager.package_order_payments1TableAdapter = null;
            this.tableAdapterManager.package_order_paymentsTableAdapter = null;
            this.tableAdapterManager.packages_order_line_delivererTableAdapter = null;
            this.tableAdapterManager.packages_order_linesTableAdapter = this.packages_order_linesTableAdapter;
            this.tableAdapterManager.packages_orderTableAdapter = null;
            this.tableAdapterManager.partners_paymentsTableAdapter = null;
            this.tableAdapterManager.partners_rcTableAdapter = null;
            this.tableAdapterManager.partnersTableAdapter = null;
            this.tableAdapterManager.payment_methodsTableAdapter = null;
            this.tableAdapterManager.productsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.workers_absenceTableAdapter = null;
            this.tableAdapterManager.workers_accountTableAdapter = null;
            this.tableAdapterManager.workers_paymentsTableAdapter = null;
            this.tableAdapterManager.workersTableAdapter = null;
            // 
            // productsBindingSource
            // 
            this.productsBindingSource.DataMember = "products";
            this.productsBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // productsTableAdapter
            // 
            this.productsTableAdapter.ClearBeforeFill = true;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(298, 18);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(83, 27);
            this.labelX5.TabIndex = 54;
            this.labelX5.Text = "السلعة:";
            // 
            // ProductNameLblx
            // 
            this.ProductNameLblx.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ProductNameLblx.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ProductNameLblx.ForeColor = System.Drawing.Color.Black;
            this.ProductNameLblx.Location = new System.Drawing.Point(12, 18);
            this.ProductNameLblx.Name = "ProductNameLblx";
            this.ProductNameLblx.Size = new System.Drawing.Size(280, 27);
            this.ProductNameLblx.TabIndex = 54;
            this.ProductNameLblx.Text = "---";
            // 
            // noteTextBoxX
            // 
            this.noteTextBoxX.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.noteTextBoxX.Border.Class = "TextBoxBorder";
            this.noteTextBoxX.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.noteTextBoxX.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.packages_order_linesBindingSource, "Note", true));
            this.noteTextBoxX.DisabledBackColor = System.Drawing.Color.White;
            this.noteTextBoxX.ForeColor = System.Drawing.Color.Black;
            this.noteTextBoxX.Location = new System.Drawing.Point(12, 249);
            this.noteTextBoxX.Name = "noteTextBoxX";
            this.noteTextBoxX.PreventEnterBeep = true;
            this.noteTextBoxX.Size = new System.Drawing.Size(280, 27);
            this.noteTextBoxX.TabIndex = 6;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(298, 249);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(83, 27);
            this.labelX6.TabIndex = 53;
            this.labelX6.Text = "ملاحظـــــــــــة:";
            // 
            // phonesTextBoxX1
            // 
            this.phonesTextBoxX1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.phonesTextBoxX1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.phonesTextBoxX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.phonesTextBoxX1.Border.Class = "TextBoxBorder";
            this.phonesTextBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.phonesTextBoxX1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.packages_order_linesBindingSource, "Phones", true));
            this.phonesTextBoxX1.DisabledBackColor = System.Drawing.Color.White;
            this.phonesTextBoxX1.FocusHighlightEnabled = true;
            this.phonesTextBoxX1.ForeColor = System.Drawing.Color.Black;
            this.phonesTextBoxX1.Location = new System.Drawing.Point(48, 84);
            this.phonesTextBoxX1.MaxLength = 255;
            this.phonesTextBoxX1.Name = "phonesTextBoxX1";
            this.phonesTextBoxX1.PreventEnterBeep = true;
            this.phonesTextBoxX1.Size = new System.Drawing.Size(244, 27);
            this.phonesTextBoxX1.TabIndex = 1;
            this.phonesTextBoxX1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.phonesTextBoxX1_KeyUp);
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(298, 82);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(83, 29);
            this.labelX7.TabIndex = 54;
            this.labelX7.Text = "رقم الهاتف:";
            this.labelX7.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.labelX7.WordWrap = true;
            // 
            // checNonPayed
            // 
            this.checNonPayed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.checNonPayed.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.checNonPayed.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.checNonPayed.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.checNonPayed.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checNonPayed.ForeColor = System.Drawing.Color.Black;
            this.checNonPayed.Location = new System.Drawing.Point(205, 144);
            this.checNonPayed.Name = "checNonPayed";
            this.checNonPayed.Size = new System.Drawing.Size(87, 38);
            this.checNonPayed.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.checNonPayed.TabIndex = 71;
            this.checNonPayed.Text = "غير خالص";
            this.checNonPayed.CheckedChanged += new System.EventHandler(this.checNonPayed_CheckedChanged);
            // 
            // checPayed
            // 
            this.checPayed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.checPayed.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.checPayed.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.checPayed.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.checPayed.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checPayed.ForeColor = System.Drawing.Color.Black;
            this.checPayed.Location = new System.Drawing.Point(54, 144);
            this.checPayed.Name = "checPayed";
            this.checPayed.Size = new System.Drawing.Size(71, 38);
            this.checPayed.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.checPayed.TabIndex = 72;
            this.checPayed.Text = "خالص";
            this.checPayed.CheckedChanged += new System.EventHandler(this.checNonPayed_CheckedChanged);
            // 
            // checRegistred
            // 
            this.checRegistred.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.checRegistred.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.checRegistred.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.checRegistred.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.checRegistred.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checRegistred.ForeColor = System.Drawing.Color.Black;
            this.checRegistred.Location = new System.Drawing.Point(131, 144);
            this.checRegistred.Name = "checRegistred";
            this.checRegistred.Size = new System.Drawing.Size(68, 38);
            this.checRegistred.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.checRegistred.TabIndex = 73;
            this.checRegistred.Text = "مسجل";
            this.checRegistred.CheckedChanged += new System.EventHandler(this.checNonPayed_CheckedChanged);
            // 
            // quantityDoubleInput
            // 
            this.quantityDoubleInput.AllowEmptyState = false;
            // 
            // 
            // 
            this.quantityDoubleInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.quantityDoubleInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.quantityDoubleInput.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.quantityDoubleInput.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.packages_order_linesBindingSource, "Quantity", true));
            this.quantityDoubleInput.Increment = 1D;
            this.quantityDoubleInput.Location = new System.Drawing.Point(188, 115);
            this.quantityDoubleInput.MaxValue = 999999999D;
            this.quantityDoubleInput.MinValue = 0D;
            this.quantityDoubleInput.Name = "quantityDoubleInput";
            this.quantityDoubleInput.ShowUpDown = true;
            this.quantityDoubleInput.Size = new System.Drawing.Size(104, 27);
            this.quantityDoubleInput.TabIndex = 74;
            // 
            // AddEditPackageOrderLine
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(401, 334);
            this.ControlBox = false;
            this.Controls.Add(this.quantityDoubleInput);
            this.Controls.Add(this.checRegistred);
            this.Controls.Add(this.checPayed);
            this.Controls.Add(this.checNonPayed);
            this.Controls.Add(this.noteTextBoxX);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ProductNameLblx);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.labelX7);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.phonesTextBoxX1);
            this.Controls.Add(this.recipientTextBoxX);
            this.Controls.Add(this.priceDoubleInput);
            this.Controls.Add(this.designationTextBoxX);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "AddEditPackageOrderLine";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "إصافة سلعة";
            this.Load += new System.EventHandler(this.AddEditPackageOrderLine_Load);
            ((System.ComponentModel.ISupportInitialize)(this.packages_order_linesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceDoubleInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantityDoubleInput)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private gama_transportationdbDataSet gama_transportationdbDataSet;
        private System.Windows.Forms.BindingSource packages_order_linesBindingSource;
        private gama_transportationdbDataSetTableAdapters.packages_order_linesTableAdapter packages_order_linesTableAdapter;
        private gama_transportationdbDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private DevComponents.DotNetBar.Controls.TextBoxX designationTextBoxX;
        private DevComponents.Editors.DoubleInput priceDoubleInput;
        private DevComponents.DotNetBar.Controls.TextBoxX recipientTextBoxX;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private System.Windows.Forms.BindingSource productsBindingSource;
        private gama_transportationdbDataSetTableAdapters.productsTableAdapter productsTableAdapter;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX ProductNameLblx;
        private DevComponents.DotNetBar.Controls.TextBoxX noteTextBoxX;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX phonesTextBoxX1;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.CheckBoxX checNonPayed;
        private DevComponents.DotNetBar.Controls.CheckBoxX checPayed;
        private DevComponents.DotNetBar.Controls.CheckBoxX checRegistred;
        private DevComponents.Editors.DoubleInput quantityDoubleInput;
    }
}