using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Packages
{
    public partial class AddEditPackageOrderLine : DevComponents.DotNetBar.Metro.MetroForm
    {
        public int LineID { get; set; }
        public int PackageOrderID { get; set; }
        public int ProductID { get; set; }
        public bool IsLeaving { get; set; }
        public bool IsFactory { get; set; }
        bool RechargeRecipientAfterClosing;
        public AddEditPackageOrderLine()
        {
            InitializeComponent();
            RechargeRecipientAfterClosing = false;
        }

        private void packages_order_linesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.packages_order_linesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.gama_transportationdbDataSet);
        }

        private void AddEditPackageOrderLine_Load(object sender, EventArgs e)
        {
            try
            {
                var list = Helper.ReceiversSource.Source.Select(x => x.Recipient).ToArray();
                AutoCompleteStringCollection ac = new AutoCompleteStringCollection();
                ac.AddRange(list);
                recipientTextBoxX.AutoCompleteCustomSource = ac;
            }
            catch (Exception)
            {
            }

            try
            {
                var list = Helper.ReceiversSource.Source.Select(x => x.phones).ToArray();
                AutoCompleteStringCollection ac = new AutoCompleteStringCollection();
                ac.AddRange(list);
                phonesTextBoxX1.AutoCompleteCustomSource = ac;
            }
            catch (Exception)
            {
            }

            this.productsTableAdapter.FillByID(this.gama_transportationdbDataSet.products,ProductID);
            if (LineID == 0)
            {

                packages_order_linesBindingSource.AddNew();
                ((DataRowView)packages_order_linesBindingSource.Current)["PackagesOrder_ID"] = PackageOrderID;
                ((DataRowView)packages_order_linesBindingSource.Current)["products_ID"] = ProductID;
                ((DataRowView)packages_order_linesBindingSource.Current)["Payed"] = 0;
                checNonPayed.Checked = true;
                var product = gama_transportationdbDataSet.products.FirstOrDefault();
                ((DataRowView)packages_order_linesBindingSource.Current)["Designation"] = designationTextBoxX.Text = ProductNameLblx.Text = product.ProductName;
                priceDoubleInput.Value = Convert.ToDouble(product.Price);
                if (!IsLeaving)
                {
                    recipientTextBoxX.Visible = false;
                    labelX1.Visible =
                    labelX4.Visible = false;
                    if (!checPayed.Checked)
                    {
                        priceDoubleInput.Value = 0;
                    }
                }
            }
            else
            {
                this.packages_order_linesTableAdapter.FillByLineID(this.gama_transportationdbDataSet.packages_order_lines, LineID);
                int p = this.gama_transportationdbDataSet.packages_order_lines.FirstOrDefault().Payed;
                if (p == 0)
                {
                    checNonPayed.Checked = true;
                }
                else if (p == 1)
                {
                    checRegistred.Checked = true;
                }
                else if (p == 2)
                {
                    checPayed.Checked = true;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsLeaving && checPayed.Checked && priceDoubleInput.Value <= 0)
                {
                    ToastNotification.Show(this, "��� ����� ��� ����� ��� ����� ������ �����");
                    return;
                }
                this.Validate();
                this.packages_order_linesBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.gama_transportationdbDataSet);
                DialogResult = System.Windows.Forms.DialogResult.OK;
                if (Helper.ReceiversSource.RechargeRecipientAfterClosing)
                {

                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
        }

        private void payedSwitchButton_ValueChanged(object sender, EventArgs e)
        {
        }
        private void recipientTextBoxX_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                try
                {
                var dr = Helper.ReceiversSource.Source.First(x=>!x.IsRecipientNull() && x.Recipient == recipientTextBoxX.Text);
               
                    if (dr != null)
                    {
                        phonesTextBoxX1.Text = dr.phones;
                    }
                    else
                    {
                        RechargeRecipientAfterClosing = true;
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void phonesTextBoxX1_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                var dr = Helper.ReceiversPhonesSource.Source.First(x => x.phones == phonesTextBoxX1.Text);
                if (dr != null)
                {
                    recipientTextBoxX.Text = dr.Recipient;
                }
                else
                {
                    RechargeRecipientAfterClosing = true;
                }
            }
            catch (Exception)
            {
            }
        }

        private void checNonPayed_CheckedChanged(object sender, EventArgs e)
        {
            if(checNonPayed.Checked)
                ((DataRowView)packages_order_linesBindingSource.Current)["Payed"] = 0;
            else if (checRegistred.Checked)
                ((DataRowView)packages_order_linesBindingSource.Current)["Payed"] = 1;
            else if (checPayed.Checked)
                ((DataRowView)packages_order_linesBindingSource.Current)["Payed"] = 2;

        }
    }
}