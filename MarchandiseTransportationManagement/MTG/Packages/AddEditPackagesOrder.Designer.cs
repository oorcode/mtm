﻿namespace MTM.Packages
{
    partial class AddEditPackagesOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddEditPackagesOrder));
            this.totalPackagesBox = new DevComponents.Editors.DoubleInput();
            this.packages_orderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gama_transportationdbDataSet = new MTM.gama_transportationdbDataSet();
            this.discountBox = new DevComponents.Editors.DoubleInput();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.mainDataGridViewX = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.productsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.paymentmethodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.packages_order_linesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ProductsdataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.productNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partnersIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.collapsibleSplitContainer1 = new DevComponents.DotNetBar.Controls.CollapsibleSplitContainer();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.ExpensesCommandesTable = new System.Windows.Forms.TableLayoutPanel();
            this.btnDeleteExpenses = new DevComponents.DotNetBar.ButtonX();
            this.btnEditExpenses = new DevComponents.DotNetBar.ButtonX();
            this.btnAddExpences = new DevComponents.DotNetBar.ButtonX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.ExpensesDataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.noteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateStampDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packagesorderIDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partnerspaymentsIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packageorderexpensesIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packagesorderYearIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendingOrderIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.package_order_paymentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.btnAddPayment = new DevComponents.DotNetBar.ButtonX();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.datDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noteDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partnersIDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deletedDataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.iDDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.delivererOrderIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.delivererOrderSerialYearDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendingOrderIDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partners_paymentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.btnEditLine = new DevComponents.DotNetBar.ButtonX();
            this.btnDeleteLine = new DevComponents.DotNetBar.ButtonX();
            this.panelEx3 = new DevComponents.DotNetBar.PanelEx();
            this.btnAddLine = new DevComponents.DotNetBar.ButtonX();
            this.ProductSearchBox = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnExit = new DevComponents.DotNetBar.ButtonX();
            this.btnAddOrder = new DevComponents.DotNetBar.ButtonX();
            this.btnAddOrderFactory = new DevComponents.DotNetBar.ButtonItem();
            this.btnAddOrderMerchant = new DevComponents.DotNetBar.ButtonItem();
            this.btnPrintt = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.paymentOnRecipientSwitchButton = new DevComponents.DotNetBar.Controls.SwitchButton();
            this.datDateTimeInput = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.partnerNameLabel = new DevComponents.DotNetBar.LabelX();
            this.textBoxX1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.doubleInput2 = new DevComponents.Editors.DoubleInput();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.ExpensesBox = new DevComponents.Editors.DoubleInput();
            this.dataGridViewButtonXColumn1 = new DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn();
            this.packages_orderTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.packages_orderTableAdapter();
            this.packages_order_linesTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.packages_order_linesTableAdapter();
            this.productsTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.productsTableAdapter();
            this.tableAdapterManager = new MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager();
            this.package_order_paymentsTableAdapter1 = new MTM.gama_transportationdbDataSetTableAdapters.package_order_paymentsTableAdapter();
            this.partners_paymentsTableAdapter1 = new MTM.gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter();
            this.payment_methodsTableAdapter = new MTM.gama_transportationdbDataSetTableAdapters.payment_methodsTableAdapter();
            this.recipientDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhonesColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productsIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.designationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayedColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.iDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packagesOrderIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deletedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DeleteLineColumn = new DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn();
            ((System.ComponentModel.ISupportInitialize)(this.totalPackagesBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packages_orderBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGridViewX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentmethodsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packages_order_linesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProductsdataGridViewX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.collapsibleSplitContainer1)).BeginInit();
            this.collapsibleSplitContainer1.Panel1.SuspendLayout();
            this.collapsibleSplitContainer1.Panel2.SuspendLayout();
            this.collapsibleSplitContainer1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.ExpensesCommandesTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExpensesDataGridViewX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.package_order_paymentsBindingSource)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.partners_paymentsBindingSource)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            this.panelEx3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datDateTimeInput)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.doubleInput2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpensesBox)).BeginInit();
            this.SuspendLayout();
            // 
            // totalPackagesBox
            // 
            this.totalPackagesBox.AllowEmptyState = false;
            this.totalPackagesBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.totalPackagesBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.totalPackagesBox.BackgroundStyle.Class = "DateTimeInputBackground";
            this.totalPackagesBox.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.totalPackagesBox.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.totalPackagesBox.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.packages_orderBindingSource, "TotalPackages", true));
            this.totalPackagesBox.FocusHighlightEnabled = true;
            this.totalPackagesBox.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalPackagesBox.ForeColor = System.Drawing.Color.Black;
            this.totalPackagesBox.Increment = 0D;
            this.totalPackagesBox.Location = new System.Drawing.Point(318, 4);
            this.totalPackagesBox.MinValue = 0D;
            this.totalPackagesBox.Name = "totalPackagesBox";
            this.totalPackagesBox.Size = new System.Drawing.Size(132, 27);
            this.totalPackagesBox.TabIndex = 2;
            // 
            // packages_orderBindingSource
            // 
            this.packages_orderBindingSource.DataMember = "packages_order";
            this.packages_orderBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // gama_transportationdbDataSet
            // 
            this.gama_transportationdbDataSet.DataSetName = "gama_transportationdbDataSet";
            this.gama_transportationdbDataSet.EnforceConstraints = false;
            this.gama_transportationdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // discountBox
            // 
            this.discountBox.AllowEmptyState = false;
            this.discountBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.discountBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.discountBox.BackgroundStyle.Class = "DateTimeInputBackground";
            this.discountBox.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.discountBox.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.discountBox.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.packages_orderBindingSource, "Discount", true));
            this.discountBox.FocusHighlightEnabled = true;
            this.discountBox.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.discountBox.ForeColor = System.Drawing.Color.Black;
            this.discountBox.Increment = 0D;
            this.discountBox.Location = new System.Drawing.Point(727, 4);
            this.discountBox.MinValue = 0D;
            this.discountBox.Name = "discountBox";
            this.discountBox.Size = new System.Drawing.Size(125, 27);
            this.discountBox.TabIndex = 4;
            this.discountBox.ValueChanged += new System.EventHandler(this.discountBox_ValueChanged);
            this.discountBox.Enter += new System.EventHandler(this.discountBox_Enter);
            this.discountBox.Leave += new System.EventHandler(this.discountBox_Leave);
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(453, 4);
            this.labelX1.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(66, 29);
            this.labelX1.TabIndex = 5;
            this.labelX1.Text = "المجموع:";
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(855, 3);
            this.labelX2.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(81, 30);
            this.labelX2.TabIndex = 5;
            this.labelX2.Text = "التخفيض:";
            // 
            // mainDataGridViewX
            // 
            this.mainDataGridViewX.AllowUserToAddRows = false;
            this.mainDataGridViewX.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.mainDataGridViewX.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.mainDataGridViewX.AutoGenerateColumns = false;
            this.mainDataGridViewX.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mainDataGridViewX.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.mainDataGridViewX.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mainDataGridViewX.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.recipientDataGridViewTextBoxColumn,
            this.PhonesColumn,
            this.productsIDDataGridViewTextBoxColumn,
            this.designationDataGridViewTextBoxColumn,
            this.quantityDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn1,
            this.PayedColumn,
            this.iDDataGridViewTextBoxColumn1,
            this.packagesOrderIDDataGridViewTextBoxColumn,
            this.deletedDataGridViewCheckBoxColumn,
            this.DeleteLineColumn});
            this.mainDataGridViewX.DataSource = this.packages_order_linesBindingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.mainDataGridViewX.DefaultCellStyle = dataGridViewCellStyle5;
            this.mainDataGridViewX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDataGridViewX.EnableHeadersVisualStyles = false;
            this.mainDataGridViewX.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.mainDataGridViewX.Location = new System.Drawing.Point(0, 0);
            this.mainDataGridViewX.Name = "mainDataGridViewX";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mainDataGridViewX.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.mainDataGridViewX.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.mainDataGridViewX.RowTemplate.Height = 32;
            this.mainDataGridViewX.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mainDataGridViewX.Size = new System.Drawing.Size(690, 272);
            this.mainDataGridViewX.TabIndex = 60;
            this.mainDataGridViewX.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.mainDataGridViewX_CellClick);
            this.mainDataGridViewX.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.mainDataGridViewX_CellEndEdit);
            this.mainDataGridViewX.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.mainDataGridViewX_DataError);
            this.mainDataGridViewX.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.mainDataGridViewX_MouseDoubleClick);
            // 
            // productsBindingSource1
            // 
            this.productsBindingSource1.DataMember = "products";
            this.productsBindingSource1.DataSource = this.gama_transportationdbDataSet;
            // 
            // paymentmethodsBindingSource
            // 
            this.paymentmethodsBindingSource.DataMember = "payment_methods";
            this.paymentmethodsBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // packages_order_linesBindingSource
            // 
            this.packages_order_linesBindingSource.DataMember = "fk_Packages_PackagesOrder1";
            this.packages_order_linesBindingSource.DataSource = this.packages_orderBindingSource;
            this.packages_order_linesBindingSource.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.packages_order_linesBindingSource_ListChanged);
            // 
            // ProductsdataGridViewX1
            // 
            this.ProductsdataGridViewX1.AllowUserToAddRows = false;
            this.ProductsdataGridViewX1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            this.ProductsdataGridViewX1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.ProductsdataGridViewX1.AutoGenerateColumns = false;
            this.ProductsdataGridViewX1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ProductsdataGridViewX1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.ProductsdataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProductsdataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.productNameDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.iDDataGridViewTextBoxColumn,
            this.partnersIDDataGridViewTextBoxColumn});
            this.ProductsdataGridViewX1.DataSource = this.productsBindingSource;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ProductsdataGridViewX1.DefaultCellStyle = dataGridViewCellStyle11;
            this.ProductsdataGridViewX1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProductsdataGridViewX1.EnableHeadersVisualStyles = false;
            this.ProductsdataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.ProductsdataGridViewX1.Location = new System.Drawing.Point(0, 74);
            this.ProductsdataGridViewX1.Name = "ProductsdataGridViewX1";
            this.ProductsdataGridViewX1.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ProductsdataGridViewX1.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ProductsdataGridViewX1.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this.ProductsdataGridViewX1.RowTemplate.Height = 32;
            this.ProductsdataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ProductsdataGridViewX1.Size = new System.Drawing.Size(348, 510);
            this.ProductsdataGridViewX1.TabIndex = 61;
            this.ProductsdataGridViewX1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.mainDataGridViewX_DataError);
            this.ProductsdataGridViewX1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ProductsdataGridViewX1_MouseDoubleClick);
            // 
            // productNameDataGridViewTextBoxColumn
            // 
            this.productNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.productNameDataGridViewTextBoxColumn.DataPropertyName = "ProductName";
            this.productNameDataGridViewTextBoxColumn.HeaderText = "تعيين المنتج";
            this.productNameDataGridViewTextBoxColumn.MinimumWidth = 130;
            this.productNameDataGridViewTextBoxColumn.Name = "productNameDataGridViewTextBoxColumn";
            this.productNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.Format = "N2";
            this.priceDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle10;
            this.priceDataGridViewTextBoxColumn.HeaderText = "سعر النقل";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // partnersIDDataGridViewTextBoxColumn
            // 
            this.partnersIDDataGridViewTextBoxColumn.DataPropertyName = "partners_ID";
            this.partnersIDDataGridViewTextBoxColumn.HeaderText = "partners_ID";
            this.partnersIDDataGridViewTextBoxColumn.Name = "partnersIDDataGridViewTextBoxColumn";
            this.partnersIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.partnersIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // productsBindingSource
            // 
            this.productsBindingSource.DataMember = "products";
            this.productsBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // collapsibleSplitContainer1
            // 
            this.collapsibleSplitContainer1.BackColor = System.Drawing.Color.White;
            this.collapsibleSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.collapsibleSplitContainer1.ForeColor = System.Drawing.Color.Black;
            this.collapsibleSplitContainer1.Location = new System.Drawing.Point(0, 73);
            this.collapsibleSplitContainer1.Name = "collapsibleSplitContainer1";
            // 
            // collapsibleSplitContainer1.Panel1
            // 
            this.collapsibleSplitContainer1.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.mainDataGridViewX);
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.tableLayoutPanel4);
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.ExpensesCommandesTable);
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.ExpensesDataGridViewX1);
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.collapsibleSplitContainer1.Panel1.Controls.Add(this.dataGridViewX1);
            this.collapsibleSplitContainer1.Panel1.ForeColor = System.Drawing.Color.Black;
            this.collapsibleSplitContainer1.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            // 
            // collapsibleSplitContainer1.Panel2
            // 
            this.collapsibleSplitContainer1.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.collapsibleSplitContainer1.Panel2.Controls.Add(this.ProductsdataGridViewX1);
            this.collapsibleSplitContainer1.Panel2.Controls.Add(this.tableLayoutPanel6);
            this.collapsibleSplitContainer1.Panel2.Controls.Add(this.panelEx3);
            this.collapsibleSplitContainer1.Panel2.ForeColor = System.Drawing.Color.Black;
            this.collapsibleSplitContainer1.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.collapsibleSplitContainer1.Size = new System.Drawing.Size(1058, 584);
            this.collapsibleSplitContainer1.SplitterDistance = 690;
            this.collapsibleSplitContainer1.SplitterWidth = 20;
            this.collapsibleSplitContainer1.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.tableLayoutPanel4.ColumnCount = 7;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 168F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 117F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 101F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel4.Controls.Add(this.labelX13, 6, 0);
            this.tableLayoutPanel4.Controls.Add(this.labelX11, 5, 0);
            this.tableLayoutPanel4.Controls.Add(this.labelX12, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.labelX10, 3, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel4.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 272);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(690, 38);
            this.tableLayoutPanel4.TabIndex = 69;
            // 
            // labelX13
            // 
            this.labelX13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX13.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(0, 3);
            this.labelX13.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(50, 32);
            this.labelX13.TabIndex = 5;
            this.labelX13.Text = "0";
            this.labelX13.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX11
            // 
            this.labelX11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX11.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(50, 3);
            this.labelX11.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(115, 32);
            this.labelX11.TabIndex = 5;
            this.labelX11.Text = "عدد الطرود:";
            // 
            // labelX12
            // 
            this.labelX12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX12.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(165, 3);
            this.labelX12.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(50, 32);
            this.labelX12.TabIndex = 5;
            this.labelX12.Text = "0";
            this.labelX12.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX10
            // 
            this.labelX10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX10.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(215, 3);
            this.labelX10.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(101, 32);
            this.labelX10.TabIndex = 5;
            this.labelX10.Text = "عدد الأسطر:";
            // 
            // ExpensesCommandesTable
            // 
            this.ExpensesCommandesTable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.ExpensesCommandesTable.ColumnCount = 5;
            this.ExpensesCommandesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ExpensesCommandesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.ExpensesCommandesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.ExpensesCommandesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.ExpensesCommandesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.ExpensesCommandesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.ExpensesCommandesTable.Controls.Add(this.btnDeleteExpenses, 4, 0);
            this.ExpensesCommandesTable.Controls.Add(this.btnEditExpenses, 3, 0);
            this.ExpensesCommandesTable.Controls.Add(this.btnAddExpences, 2, 0);
            this.ExpensesCommandesTable.Controls.Add(this.labelX3, 0, 0);
            this.ExpensesCommandesTable.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ExpensesCommandesTable.ForeColor = System.Drawing.Color.Black;
            this.ExpensesCommandesTable.Location = new System.Drawing.Point(0, 310);
            this.ExpensesCommandesTable.Name = "ExpensesCommandesTable";
            this.ExpensesCommandesTable.RowCount = 1;
            this.ExpensesCommandesTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ExpensesCommandesTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.ExpensesCommandesTable.Size = new System.Drawing.Size(690, 38);
            this.ExpensesCommandesTable.TabIndex = 88;
            // 
            // btnDeleteExpenses
            // 
            this.btnDeleteExpenses.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDeleteExpenses.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteExpenses.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDeleteExpenses.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteExpenses.Location = new System.Drawing.Point(3, 3);
            this.btnDeleteExpenses.Name = "btnDeleteExpenses";
            this.btnDeleteExpenses.Size = new System.Drawing.Size(109, 32);
            this.btnDeleteExpenses.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnDeleteExpenses.Symbol = "";
            this.btnDeleteExpenses.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDeleteExpenses.TabIndex = 2;
            this.btnDeleteExpenses.Text = "إلغاء";
            this.btnDeleteExpenses.Click += new System.EventHandler(this.btnDeleteExpenses_Click);
            // 
            // btnEditExpenses
            // 
            this.btnEditExpenses.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEditExpenses.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditExpenses.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnEditExpenses.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditExpenses.Location = new System.Drawing.Point(118, 3);
            this.btnEditExpenses.Name = "btnEditExpenses";
            this.btnEditExpenses.Size = new System.Drawing.Size(109, 32);
            this.btnEditExpenses.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnEditExpenses.Symbol = "";
            this.btnEditExpenses.SymbolColor = System.Drawing.Color.OrangeRed;
            this.btnEditExpenses.TabIndex = 1;
            this.btnEditExpenses.Text = "تعديل";
            this.btnEditExpenses.Click += new System.EventHandler(this.btnEditExpenses_Click);
            // 
            // btnAddExpences
            // 
            this.btnAddExpences.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddExpences.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddExpences.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddExpences.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddExpences.Location = new System.Drawing.Point(233, 3);
            this.btnAddExpences.Name = "btnAddExpences";
            this.btnAddExpences.Size = new System.Drawing.Size(109, 32);
            this.btnAddExpences.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddExpences.Symbol = "";
            this.btnAddExpences.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnAddExpences.TabIndex = 0;
            this.btnAddExpences.Text = "مصاريف";
            this.btnAddExpences.Click += new System.EventHandler(this.btnAddExpences_Click);
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ExpensesCommandesTable.SetColumnSpan(this.labelX3, 2);
            this.labelX3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(348, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(339, 32);
            this.labelX3.TabIndex = 67;
            this.labelX3.Text = "مصاريف الوصل";
            // 
            // ExpensesDataGridViewX1
            // 
            this.ExpensesDataGridViewX1.AllowUserToAddRows = false;
            this.ExpensesDataGridViewX1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            this.ExpensesDataGridViewX1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle14;
            this.ExpensesDataGridViewX1.AutoGenerateColumns = false;
            this.ExpensesDataGridViewX1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ExpensesDataGridViewX1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.ExpensesDataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ExpensesDataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.noteDataGridViewTextBoxColumn,
            this.amountDataGridViewTextBoxColumn,
            this.datDataGridViewTextBoxColumn,
            this.dateStampDataGridViewTextBoxColumn,
            this.iDDataGridViewTextBoxColumn2,
            this.packagesorderIDDataGridViewTextBoxColumn1,
            this.partnerspaymentsIDDataGridViewTextBoxColumn,
            this.packageorderexpensesIDDataGridViewTextBoxColumn,
            this.packagesorderYearIDDataGridViewTextBoxColumn,
            this.sendingOrderIDDataGridViewTextBoxColumn});
            this.ExpensesDataGridViewX1.DataSource = this.package_order_paymentsBindingSource;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ExpensesDataGridViewX1.DefaultCellStyle = dataGridViewCellStyle18;
            this.ExpensesDataGridViewX1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ExpensesDataGridViewX1.EnableHeadersVisualStyles = false;
            this.ExpensesDataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.ExpensesDataGridViewX1.Location = new System.Drawing.Point(0, 348);
            this.ExpensesDataGridViewX1.Name = "ExpensesDataGridViewX1";
            this.ExpensesDataGridViewX1.ReadOnly = true;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ExpensesDataGridViewX1.RowHeadersDefaultCellStyle = dataGridViewCellStyle19;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ExpensesDataGridViewX1.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this.ExpensesDataGridViewX1.RowTemplate.Height = 32;
            this.ExpensesDataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ExpensesDataGridViewX1.Size = new System.Drawing.Size(690, 106);
            this.ExpensesDataGridViewX1.TabIndex = 87;
            // 
            // noteDataGridViewTextBoxColumn
            // 
            this.noteDataGridViewTextBoxColumn.DataPropertyName = "Note";
            this.noteDataGridViewTextBoxColumn.HeaderText = "ملاحظة";
            this.noteDataGridViewTextBoxColumn.Name = "noteDataGridViewTextBoxColumn";
            this.noteDataGridViewTextBoxColumn.ReadOnly = true;
            this.noteDataGridViewTextBoxColumn.Width = 300;
            // 
            // amountDataGridViewTextBoxColumn
            // 
            this.amountDataGridViewTextBoxColumn.DataPropertyName = "Amount";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.Format = "N2";
            this.amountDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle16;
            this.amountDataGridViewTextBoxColumn.HeaderText = "المبلغ";
            this.amountDataGridViewTextBoxColumn.Name = "amountDataGridViewTextBoxColumn";
            this.amountDataGridViewTextBoxColumn.ReadOnly = true;
            this.amountDataGridViewTextBoxColumn.Width = 130;
            // 
            // datDataGridViewTextBoxColumn
            // 
            this.datDataGridViewTextBoxColumn.DataPropertyName = "dat";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.Format = "d";
            this.datDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle17;
            this.datDataGridViewTextBoxColumn.HeaderText = "التاريخ";
            this.datDataGridViewTextBoxColumn.Name = "datDataGridViewTextBoxColumn";
            this.datDataGridViewTextBoxColumn.ReadOnly = true;
            this.datDataGridViewTextBoxColumn.Width = 140;
            // 
            // dateStampDataGridViewTextBoxColumn
            // 
            this.dateStampDataGridViewTextBoxColumn.DataPropertyName = "DateStamp";
            this.dateStampDataGridViewTextBoxColumn.HeaderText = "DateStamp";
            this.dateStampDataGridViewTextBoxColumn.Name = "dateStampDataGridViewTextBoxColumn";
            this.dateStampDataGridViewTextBoxColumn.ReadOnly = true;
            this.dateStampDataGridViewTextBoxColumn.Visible = false;
            // 
            // iDDataGridViewTextBoxColumn2
            // 
            this.iDDataGridViewTextBoxColumn2.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn2.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn2.Name = "iDDataGridViewTextBoxColumn2";
            this.iDDataGridViewTextBoxColumn2.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn2.Visible = false;
            // 
            // packagesorderIDDataGridViewTextBoxColumn1
            // 
            this.packagesorderIDDataGridViewTextBoxColumn1.DataPropertyName = "packages_order_ID";
            this.packagesorderIDDataGridViewTextBoxColumn1.HeaderText = "packages_order_ID";
            this.packagesorderIDDataGridViewTextBoxColumn1.Name = "packagesorderIDDataGridViewTextBoxColumn1";
            this.packagesorderIDDataGridViewTextBoxColumn1.ReadOnly = true;
            this.packagesorderIDDataGridViewTextBoxColumn1.Visible = false;
            // 
            // partnerspaymentsIDDataGridViewTextBoxColumn
            // 
            this.partnerspaymentsIDDataGridViewTextBoxColumn.DataPropertyName = "partners_payments_ID";
            this.partnerspaymentsIDDataGridViewTextBoxColumn.HeaderText = "partners_payments_ID";
            this.partnerspaymentsIDDataGridViewTextBoxColumn.Name = "partnerspaymentsIDDataGridViewTextBoxColumn";
            this.partnerspaymentsIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.partnerspaymentsIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // packageorderexpensesIDDataGridViewTextBoxColumn
            // 
            this.packageorderexpensesIDDataGridViewTextBoxColumn.DataPropertyName = "package_order_expenses_ID";
            this.packageorderexpensesIDDataGridViewTextBoxColumn.HeaderText = "package_order_expenses_ID";
            this.packageorderexpensesIDDataGridViewTextBoxColumn.Name = "packageorderexpensesIDDataGridViewTextBoxColumn";
            this.packageorderexpensesIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.packageorderexpensesIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // packagesorderYearIDDataGridViewTextBoxColumn
            // 
            this.packagesorderYearIDDataGridViewTextBoxColumn.DataPropertyName = "packages_order_YearID";
            this.packagesorderYearIDDataGridViewTextBoxColumn.HeaderText = "packages_order_YearID";
            this.packagesorderYearIDDataGridViewTextBoxColumn.Name = "packagesorderYearIDDataGridViewTextBoxColumn";
            this.packagesorderYearIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.packagesorderYearIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // sendingOrderIDDataGridViewTextBoxColumn
            // 
            this.sendingOrderIDDataGridViewTextBoxColumn.DataPropertyName = "sending_OrderID";
            this.sendingOrderIDDataGridViewTextBoxColumn.HeaderText = "sending_OrderID";
            this.sendingOrderIDDataGridViewTextBoxColumn.Name = "sendingOrderIDDataGridViewTextBoxColumn";
            this.sendingOrderIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.sendingOrderIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // package_order_paymentsBindingSource
            // 
            this.package_order_paymentsBindingSource.DataMember = "package_order_payments";
            this.package_order_paymentsBindingSource.DataSource = this.gama_transportationdbDataSet;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 113F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 117F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.buttonX2, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonX3, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelX9, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnAddPayment, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 454);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(690, 38);
            this.tableLayoutPanel1.TabIndex = 90;
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX2.Location = new System.Drawing.Point(3, 3);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(109, 32);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonX2.TabIndex = 2;
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX3.Location = new System.Drawing.Point(118, 3);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Size = new System.Drawing.Size(109, 32);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.OrangeRed;
            this.buttonX3.TabIndex = 1;
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // labelX9
            // 
            this.labelX9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX9.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel1.SetColumnSpan(this.labelX9, 2);
            this.labelX9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(350, 3);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(337, 32);
            this.labelX9.TabIndex = 67;
            this.labelX9.Text = "دفعات الوصل";
            // 
            // btnAddPayment
            // 
            this.btnAddPayment.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddPayment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddPayment.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddPayment.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPayment.Image = global::MTM.Properties.Resources.NewPayment;
            this.btnAddPayment.ImageFixedSize = new System.Drawing.Size(24, 24);
            this.btnAddPayment.Location = new System.Drawing.Point(233, 3);
            this.btnAddPayment.Name = "btnAddPayment";
            this.btnAddPayment.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F12);
            this.btnAddPayment.Size = new System.Drawing.Size(111, 32);
            this.btnAddPayment.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddPayment.TabIndex = 6;
            this.btnAddPayment.Text = "تسجيل دفع";
            this.btnAddPayment.Tooltip = "تسجيل دفع";
            this.btnAddPayment.Click += new System.EventHandler(this.btnAddPayment_Click);
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.AllowUserToAddRows = false;
            this.dataGridViewX1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            this.dataGridViewX1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewX1.AutoGenerateColumns = false;
            this.dataGridViewX1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.datDataGridViewTextBoxColumn1,
            this.amountDataGridViewTextBoxColumn1,
            this.noteDataGridViewTextBoxColumn1,
            this.partnersIDDataGridViewTextBoxColumn1,
            this.deletedDataGridViewCheckBoxColumn1,
            this.iDDataGridViewTextBoxColumn3,
            this.delivererOrderIDDataGridViewTextBoxColumn,
            this.delivererOrderSerialYearDataGridViewTextBoxColumn,
            this.sendingOrderIDDataGridViewTextBoxColumn1});
            this.dataGridViewX1.DataSource = this.partners_paymentsBindingSource;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewX1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewX1.EnableHeadersVisualStyles = false;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(0, 492);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.ReadOnly = true;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX1.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dataGridViewX1.RowsDefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridViewX1.RowTemplate.Height = 32;
            this.dataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewX1.Size = new System.Drawing.Size(690, 92);
            this.dataGridViewX1.TabIndex = 89;
            // 
            // datDataGridViewTextBoxColumn1
            // 
            this.datDataGridViewTextBoxColumn1.DataPropertyName = "dat";
            this.datDataGridViewTextBoxColumn1.HeaderText = "التاريخ";
            this.datDataGridViewTextBoxColumn1.Name = "datDataGridViewTextBoxColumn1";
            this.datDataGridViewTextBoxColumn1.ReadOnly = true;
            this.datDataGridViewTextBoxColumn1.Width = 150;
            // 
            // amountDataGridViewTextBoxColumn1
            // 
            this.amountDataGridViewTextBoxColumn1.DataPropertyName = "Amount";
            this.amountDataGridViewTextBoxColumn1.HeaderText = "المبلغ";
            this.amountDataGridViewTextBoxColumn1.Name = "amountDataGridViewTextBoxColumn1";
            this.amountDataGridViewTextBoxColumn1.ReadOnly = true;
            this.amountDataGridViewTextBoxColumn1.Width = 120;
            // 
            // noteDataGridViewTextBoxColumn1
            // 
            this.noteDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.noteDataGridViewTextBoxColumn1.DataPropertyName = "Note";
            this.noteDataGridViewTextBoxColumn1.HeaderText = "ملاحظة";
            this.noteDataGridViewTextBoxColumn1.Name = "noteDataGridViewTextBoxColumn1";
            this.noteDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // partnersIDDataGridViewTextBoxColumn1
            // 
            this.partnersIDDataGridViewTextBoxColumn1.DataPropertyName = "partners_ID";
            this.partnersIDDataGridViewTextBoxColumn1.HeaderText = "partners_ID";
            this.partnersIDDataGridViewTextBoxColumn1.Name = "partnersIDDataGridViewTextBoxColumn1";
            this.partnersIDDataGridViewTextBoxColumn1.ReadOnly = true;
            this.partnersIDDataGridViewTextBoxColumn1.Visible = false;
            // 
            // deletedDataGridViewCheckBoxColumn1
            // 
            this.deletedDataGridViewCheckBoxColumn1.DataPropertyName = "Deleted";
            this.deletedDataGridViewCheckBoxColumn1.HeaderText = "Deleted";
            this.deletedDataGridViewCheckBoxColumn1.Name = "deletedDataGridViewCheckBoxColumn1";
            this.deletedDataGridViewCheckBoxColumn1.ReadOnly = true;
            this.deletedDataGridViewCheckBoxColumn1.Visible = false;
            // 
            // iDDataGridViewTextBoxColumn3
            // 
            this.iDDataGridViewTextBoxColumn3.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn3.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn3.Name = "iDDataGridViewTextBoxColumn3";
            this.iDDataGridViewTextBoxColumn3.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn3.Visible = false;
            // 
            // delivererOrderIDDataGridViewTextBoxColumn
            // 
            this.delivererOrderIDDataGridViewTextBoxColumn.DataPropertyName = "deliverer_Order_ID";
            this.delivererOrderIDDataGridViewTextBoxColumn.HeaderText = "deliverer_Order_ID";
            this.delivererOrderIDDataGridViewTextBoxColumn.Name = "delivererOrderIDDataGridViewTextBoxColumn";
            this.delivererOrderIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.delivererOrderIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // delivererOrderSerialYearDataGridViewTextBoxColumn
            // 
            this.delivererOrderSerialYearDataGridViewTextBoxColumn.DataPropertyName = "deliverer_Order_SerialYear";
            this.delivererOrderSerialYearDataGridViewTextBoxColumn.HeaderText = "deliverer_Order_SerialYear";
            this.delivererOrderSerialYearDataGridViewTextBoxColumn.Name = "delivererOrderSerialYearDataGridViewTextBoxColumn";
            this.delivererOrderSerialYearDataGridViewTextBoxColumn.ReadOnly = true;
            this.delivererOrderSerialYearDataGridViewTextBoxColumn.Visible = false;
            // 
            // sendingOrderIDDataGridViewTextBoxColumn1
            // 
            this.sendingOrderIDDataGridViewTextBoxColumn1.DataPropertyName = "sending_OrderID";
            this.sendingOrderIDDataGridViewTextBoxColumn1.HeaderText = "sending_OrderID";
            this.sendingOrderIDDataGridViewTextBoxColumn1.Name = "sendingOrderIDDataGridViewTextBoxColumn1";
            this.sendingOrderIDDataGridViewTextBoxColumn1.ReadOnly = true;
            this.sendingOrderIDDataGridViewTextBoxColumn1.Visible = false;
            // 
            // partners_paymentsBindingSource
            // 
            this.partners_paymentsBindingSource.DataMember = "fk_sendingOrderID";
            this.partners_paymentsBindingSource.DataSource = this.packages_orderBindingSource;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Controls.Add(this.buttonX4, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnEditLine, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnDeleteLine, 3, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel6.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 39);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(348, 35);
            this.tableLayoutPanel6.TabIndex = 73;
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX4.Location = new System.Drawing.Point(233, 3);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Size = new System.Drawing.Size(109, 29);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonX4.TabIndex = 3;
            this.buttonX4.Text = "إصافة سلعة";
            this.buttonX4.Click += new System.EventHandler(this.btnAddLine_Click);
            // 
            // btnEditLine
            // 
            this.btnEditLine.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEditLine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditLine.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnEditLine.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditLine.Location = new System.Drawing.Point(118, 3);
            this.btnEditLine.Name = "btnEditLine";
            this.btnEditLine.Size = new System.Drawing.Size(109, 29);
            this.btnEditLine.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnEditLine.Symbol = "";
            this.btnEditLine.SymbolColor = System.Drawing.Color.OrangeRed;
            this.btnEditLine.TabIndex = 0;
            this.btnEditLine.Text = "تعديل سلعة";
            this.btnEditLine.Click += new System.EventHandler(this.btnEditLine_Click);
            // 
            // btnDeleteLine
            // 
            this.btnDeleteLine.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDeleteLine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteLine.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDeleteLine.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteLine.Location = new System.Drawing.Point(3, 3);
            this.btnDeleteLine.Name = "btnDeleteLine";
            this.btnDeleteLine.Size = new System.Drawing.Size(109, 29);
            this.btnDeleteLine.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnDeleteLine.Symbol = "";
            this.btnDeleteLine.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDeleteLine.TabIndex = 1;
            this.btnDeleteLine.Text = "إلغاء سلعة";
            this.btnDeleteLine.Click += new System.EventHandler(this.btnDeleteLine_Click);
            // 
            // panelEx3
            // 
            this.panelEx3.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx3.Controls.Add(this.btnAddLine);
            this.panelEx3.Controls.Add(this.ProductSearchBox);
            this.panelEx3.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEx3.Location = new System.Drawing.Point(0, 0);
            this.panelEx3.Name = "panelEx3";
            this.panelEx3.Size = new System.Drawing.Size(348, 39);
            this.panelEx3.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx3.Style.GradientAngle = 90;
            this.panelEx3.TabIndex = 69;
            this.panelEx3.Text = " ";
            // 
            // btnAddLine
            // 
            this.btnAddLine.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddLine.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddLine.Location = new System.Drawing.Point(3, 5);
            this.btnAddLine.Name = "btnAddLine";
            this.btnAddLine.Size = new System.Drawing.Size(120, 27);
            this.btnAddLine.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddLine.Symbol = "";
            this.btnAddLine.TabIndex = 1;
            this.btnAddLine.Text = "إضافة للوصل";
            this.btnAddLine.Click += new System.EventHandler(this.btnAddLine_Click);
            // 
            // ProductSearchBox
            // 
            this.ProductSearchBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ProductSearchBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ProductSearchBox.Border.Class = "TextBoxBorder";
            this.ProductSearchBox.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ProductSearchBox.DisabledBackColor = System.Drawing.Color.White;
            this.ProductSearchBox.ForeColor = System.Drawing.Color.Black;
            this.ProductSearchBox.Location = new System.Drawing.Point(128, 5);
            this.ProductSearchBox.Name = "ProductSearchBox";
            this.ProductSearchBox.PreventEnterBeep = true;
            this.ProductSearchBox.Size = new System.Drawing.Size(217, 27);
            this.ProductSearchBox.TabIndex = 0;
            this.ProductSearchBox.WatermarkText = "بحــــــــــث";
            this.ProductSearchBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ProductSearchBox_KeyDown);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.tableLayoutPanel2.ColumnCount = 11;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 145F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel2.Controls.Add(this.btnExit, 10, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnAddOrder, 8, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnPrintt, 9, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonX1, 7, 0);
            this.tableLayoutPanel2.Controls.Add(this.paymentOnRecipientSwitchButton, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.datDateTimeInput, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelX7, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelX6, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelX5, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.partnerNameLabel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.textBoxX1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelX14, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1058, 73);
            this.tableLayoutPanel2.TabIndex = 69;
            // 
            // btnExit
            // 
            this.btnExit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(3, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(109, 33);
            this.btnExit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnExit.Symbol = "";
            this.btnExit.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "خروج";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnAddOrder
            // 
            this.btnAddOrder.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddOrder.AutoExpandOnClick = true;
            this.btnAddOrder.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddOrder.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddOrder.Location = new System.Drawing.Point(233, 3);
            this.btnAddOrder.Name = "btnAddOrder";
            this.btnAddOrder.ShowSubItems = false;
            this.btnAddOrder.Size = new System.Drawing.Size(109, 33);
            this.btnAddOrder.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddOrder.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnAddOrderFactory,
            this.btnAddOrderMerchant});
            this.btnAddOrder.Symbol = "";
            this.btnAddOrder.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnAddOrder.TabIndex = 3;
            this.btnAddOrder.Text = "وصل جديد";
            this.btnAddOrder.Click += new System.EventHandler(this.btnAddOrder_Click);
            // 
            // btnAddOrderFactory
            // 
            this.btnAddOrderFactory.GlobalItem = false;
            this.btnAddOrderFactory.Name = "btnAddOrderFactory";
            this.btnAddOrderFactory.Text = "مصنع";
            this.btnAddOrderFactory.Click += new System.EventHandler(this.btnAddOrderFactory_Click);
            // 
            // btnAddOrderMerchant
            // 
            this.btnAddOrderMerchant.GlobalItem = false;
            this.btnAddOrderMerchant.Name = "btnAddOrderMerchant";
            this.btnAddOrderMerchant.Text = "تاجر";
            this.btnAddOrderMerchant.Click += new System.EventHandler(this.btnAddOrderMerchant_Click);
            // 
            // btnPrintt
            // 
            this.btnPrintt.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPrintt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintt.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnPrintt.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintt.Location = new System.Drawing.Point(118, 3);
            this.btnPrintt.Name = "btnPrintt";
            this.btnPrintt.Size = new System.Drawing.Size(109, 33);
            this.btnPrintt.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPrintt.Symbol = "";
            this.btnPrintt.TabIndex = 1;
            this.btnPrintt.Text = "طباعة";
            this.btnPrintt.Click += new System.EventHandler(this.btnPrintt_Click);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX1.Location = new System.Drawing.Point(348, 3);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(109, 33);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.TabIndex = 2;
            this.buttonX1.Text = "حفظ";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // paymentOnRecipientSwitchButton
            // 
            this.paymentOnRecipientSwitchButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.paymentOnRecipientSwitchButton.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.paymentOnRecipientSwitchButton.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel2.SetColumnSpan(this.paymentOnRecipientSwitchButton, 2);
            this.paymentOnRecipientSwitchButton.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.packages_orderBindingSource, "PaymentOnRecipient", true));
            this.paymentOnRecipientSwitchButton.ForeColor = System.Drawing.Color.Black;
            this.paymentOnRecipientSwitchButton.Location = new System.Drawing.Point(463, 0);
            this.paymentOnRecipientSwitchButton.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.paymentOnRecipientSwitchButton.Name = "paymentOnRecipientSwitchButton";
            this.paymentOnRecipientSwitchButton.OffText = "الدفع على الباعث";
            this.paymentOnRecipientSwitchButton.OnText = "الدفع على القابض";
            this.paymentOnRecipientSwitchButton.Size = new System.Drawing.Size(157, 39);
            this.paymentOnRecipientSwitchButton.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.paymentOnRecipientSwitchButton.TabIndex = 2;
            // 
            // datDateTimeInput
            // 
            this.datDateTimeInput.AllowEmptyState = false;
            this.datDateTimeInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.datDateTimeInput.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.datDateTimeInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.datDateTimeInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.datDateTimeInput.ButtonDropDown.Visible = true;
            this.datDateTimeInput.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.packages_orderBindingSource, "dat", true));
            this.datDateTimeInput.FocusHighlightEnabled = true;
            this.datDateTimeInput.ForeColor = System.Drawing.Color.Black;
            this.datDateTimeInput.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center;
            this.datDateTimeInput.IsPopupCalendarOpen = false;
            this.datDateTimeInput.Location = new System.Drawing.Point(626, 6);
            // 
            // 
            // 
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.datDateTimeInput.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.datDateTimeInput.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.DisplayMonth = new System.DateTime(2018, 12, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.datDateTimeInput.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.datDateTimeInput.MonthCalendar.TodayButtonVisible = true;
            this.datDateTimeInput.Name = "datDateTimeInput";
            this.datDateTimeInput.Size = new System.Drawing.Size(139, 27);
            this.datDateTimeInput.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.datDateTimeInput.TabIndex = 69;
            this.datDateTimeInput.Value = new System.DateTime(2019, 2, 17, 16, 53, 1, 955);
            // 
            // labelX7
            // 
            this.labelX7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX7.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(768, 3);
            this.labelX7.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(55, 33);
            this.labelX7.TabIndex = 68;
            this.labelX7.Text = "التاريخ:";
            // 
            // labelX6
            // 
            this.labelX6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX6.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.packages_orderBindingSource, "ID", true));
            this.labelX6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(823, 3);
            this.labelX6.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(70, 33);
            this.labelX6.TabIndex = 68;
            this.labelX6.Text = "---";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX5
            // 
            this.labelX5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX5.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(893, 3);
            this.labelX5.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(33, 33);
            this.labelX5.TabIndex = 68;
            this.labelX5.Text = "رقم:";
            // 
            // partnerNameLabel
            // 
            this.partnerNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.partnerNameLabel.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.partnerNameLabel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.partnerNameLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partnerNameLabel.ForeColor = System.Drawing.Color.Black;
            this.partnerNameLabel.Location = new System.Drawing.Point(926, 3);
            this.partnerNameLabel.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.partnerNameLabel.Name = "partnerNameLabel";
            this.partnerNameLabel.Size = new System.Drawing.Size(132, 33);
            this.partnerNameLabel.TabIndex = 67;
            this.partnerNameLabel.Text = "إسم الزبون";
            // 
            // textBoxX1
            // 
            this.textBoxX1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.textBoxX1.Border.Class = "TextBoxBorder";
            this.textBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel2.SetColumnSpan(this.textBoxX1, 7);
            this.textBoxX1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.packages_orderBindingSource, "Note", true));
            this.textBoxX1.DisabledBackColor = System.Drawing.Color.White;
            this.textBoxX1.FocusHighlightEnabled = true;
            this.textBoxX1.ForeColor = System.Drawing.Color.Black;
            this.textBoxX1.Location = new System.Drawing.Point(348, 42);
            this.textBoxX1.MaxLength = 999;
            this.textBoxX1.Name = "textBoxX1";
            this.textBoxX1.PreventEnterBeep = true;
            this.textBoxX1.Size = new System.Drawing.Size(575, 27);
            this.textBoxX1.TabIndex = 70;
            // 
            // labelX14
            // 
            this.labelX14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(926, 42);
            this.labelX14.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(132, 28);
            this.labelX14.TabIndex = 67;
            this.labelX14.Text = "ملاحظة:";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.tableLayoutPanel3.ColumnCount = 12;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 88F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 81F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 131F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 79F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 126F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 138F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 157F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 158F));
            this.tableLayoutPanel3.Controls.Add(this.totalPackagesBox, 9, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelX1, 8, 0);
            this.tableLayoutPanel3.Controls.Add(this.doubleInput2, 11, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelX4, 10, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelX2, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.discountBox, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelX8, 6, 0);
            this.tableLayoutPanel3.Controls.Add(this.ExpensesBox, 7, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel3.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 657);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1058, 36);
            this.tableLayoutPanel3.TabIndex = 70;
            // 
            // doubleInput2
            // 
            this.doubleInput2.AllowEmptyState = false;
            this.doubleInput2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.doubleInput2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.doubleInput2.BackgroundStyle.Class = "DateTimeInputBackground";
            this.doubleInput2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.doubleInput2.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.doubleInput2.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.packages_orderBindingSource, "Total", true));
            this.doubleInput2.FocusHighlightEnabled = true;
            this.doubleInput2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.doubleInput2.ForeColor = System.Drawing.Color.Black;
            this.doubleInput2.Increment = 0D;
            this.doubleInput2.Location = new System.Drawing.Point(3, 4);
            this.doubleInput2.MinValue = 0D;
            this.doubleInput2.Name = "doubleInput2";
            this.doubleInput2.Size = new System.Drawing.Size(152, 27);
            this.doubleInput2.TabIndex = 2;
            // 
            // labelX4
            // 
            this.labelX4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(158, 3);
            this.labelX4.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(157, 30);
            this.labelX4.TabIndex = 5;
            this.labelX4.Text = "المجموع مع المصاريف:";
            // 
            // labelX8
            // 
            this.labelX8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX8.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(645, 3);
            this.labelX8.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(79, 30);
            this.labelX8.TabIndex = 5;
            this.labelX8.Text = "المصاريف:";
            // 
            // ExpensesBox
            // 
            this.ExpensesBox.AllowEmptyState = false;
            this.ExpensesBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ExpensesBox.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ExpensesBox.BackgroundStyle.Class = "DateTimeInputBackground";
            this.ExpensesBox.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ExpensesBox.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.ExpensesBox.FocusHighlightEnabled = true;
            this.ExpensesBox.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExpensesBox.ForeColor = System.Drawing.Color.Black;
            this.ExpensesBox.Increment = 0D;
            this.ExpensesBox.Location = new System.Drawing.Point(522, 4);
            this.ExpensesBox.MinValue = 0D;
            this.ExpensesBox.Name = "ExpensesBox";
            this.ExpensesBox.Size = new System.Drawing.Size(120, 27);
            this.ExpensesBox.TabIndex = 4;
            this.ExpensesBox.ValueChanged += new System.EventHandler(this.discountBox_ValueChanged);
            this.ExpensesBox.Enter += new System.EventHandler(this.discountBox_Enter);
            this.ExpensesBox.Leave += new System.EventHandler(this.discountBox_Leave);
            // 
            // dataGridViewButtonXColumn1
            // 
            this.dataGridViewButtonXColumn1.HeaderText = "×";
            this.dataGridViewButtonXColumn1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.dataGridViewButtonXColumn1.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewButtonXColumn1.Image")));
            this.dataGridViewButtonXColumn1.Name = "dataGridViewButtonXColumn1";
            this.dataGridViewButtonXColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewButtonXColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewButtonXColumn1.Text = null;
            this.dataGridViewButtonXColumn1.Width = 32;
            // 
            // packages_orderTableAdapter
            // 
            this.packages_orderTableAdapter.ClearBeforeFill = true;
            // 
            // packages_order_linesTableAdapter
            // 
            this.packages_order_linesTableAdapter.ClearBeforeFill = true;
            // 
            // productsTableAdapter
            // 
            this.productsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.deliverersTableAdapter = null;
            this.tableAdapterManager.discountlinesTableAdapter = null;
            this.tableAdapterManager.discountsTableAdapter = null;
            this.tableAdapterManager.expensesTableAdapter = null;
            this.tableAdapterManager.locationsTableAdapter = null;
            this.tableAdapterManager.operationsTableAdapter = null;
            this.tableAdapterManager.package_delivrer_orderTableAdapter = null;
            this.tableAdapterManager.package_order_expensesTableAdapter = null;
            this.tableAdapterManager.package_order_payments1TableAdapter = null;
            this.tableAdapterManager.package_order_paymentsTableAdapter = null;
            this.tableAdapterManager.packages_order_line_delivererTableAdapter = null;
            this.tableAdapterManager.packages_order_linesTableAdapter = this.packages_order_linesTableAdapter;
            this.tableAdapterManager.packages_orderTableAdapter = this.packages_orderTableAdapter;
            this.tableAdapterManager.partners_paymentsTableAdapter = null;
            this.tableAdapterManager.partners_rcTableAdapter = null;
            this.tableAdapterManager.partnersTableAdapter = null;
            this.tableAdapterManager.payment_methodsTableAdapter = null;
            this.tableAdapterManager.productsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MTM.gama_transportationdbDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.workers_absenceTableAdapter = null;
            this.tableAdapterManager.workers_accountTableAdapter = null;
            this.tableAdapterManager.workers_paymentsTableAdapter = null;
            this.tableAdapterManager.workersTableAdapter = null;
            // 
            // package_order_paymentsTableAdapter1
            // 
            this.package_order_paymentsTableAdapter1.ClearBeforeFill = true;
            // 
            // partners_paymentsTableAdapter1
            // 
            this.partners_paymentsTableAdapter1.ClearBeforeFill = true;
            // 
            // payment_methodsTableAdapter
            // 
            this.payment_methodsTableAdapter.ClearBeforeFill = true;
            // 
            // recipientDataGridViewTextBoxColumn
            // 
            this.recipientDataGridViewTextBoxColumn.DataPropertyName = "Recipient";
            this.recipientDataGridViewTextBoxColumn.HeaderText = "المستلم";
            this.recipientDataGridViewTextBoxColumn.MinimumWidth = 150;
            this.recipientDataGridViewTextBoxColumn.Name = "recipientDataGridViewTextBoxColumn";
            this.recipientDataGridViewTextBoxColumn.Width = 150;
            // 
            // PhonesColumn
            // 
            this.PhonesColumn.DataPropertyName = "Phones";
            this.PhonesColumn.HeaderText = "الهاتف";
            this.PhonesColumn.Name = "PhonesColumn";
            this.PhonesColumn.Width = 150;
            // 
            // productsIDDataGridViewTextBoxColumn
            // 
            this.productsIDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.productsIDDataGridViewTextBoxColumn.DataPropertyName = "products_ID";
            this.productsIDDataGridViewTextBoxColumn.DataSource = this.productsBindingSource1;
            this.productsIDDataGridViewTextBoxColumn.DisplayMember = "ProductName";
            this.productsIDDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.productsIDDataGridViewTextBoxColumn.HeaderText = "تعيين السلعة";
            this.productsIDDataGridViewTextBoxColumn.MinimumWidth = 150;
            this.productsIDDataGridViewTextBoxColumn.Name = "productsIDDataGridViewTextBoxColumn";
            this.productsIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.productsIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.productsIDDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // designationDataGridViewTextBoxColumn
            // 
            this.designationDataGridViewTextBoxColumn.DataPropertyName = "Designation";
            this.designationDataGridViewTextBoxColumn.HeaderText = "الوصف";
            this.designationDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.designationDataGridViewTextBoxColumn.Name = "designationDataGridViewTextBoxColumn";
            this.designationDataGridViewTextBoxColumn.Width = 150;
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "Quantity";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Format = "N2";
            this.quantityDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.quantityDataGridViewTextBoxColumn.HeaderText = "الكمية";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            this.quantityDataGridViewTextBoxColumn.Width = 65;
            // 
            // priceDataGridViewTextBoxColumn1
            // 
            this.priceDataGridViewTextBoxColumn1.DataPropertyName = "Price";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.Format = "N2";
            this.priceDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle4;
            this.priceDataGridViewTextBoxColumn1.HeaderText = "سعر النقل";
            this.priceDataGridViewTextBoxColumn1.Name = "priceDataGridViewTextBoxColumn1";
            // 
            // PayedColumn
            // 
            this.PayedColumn.DataPropertyName = "Payed";
            this.PayedColumn.DataSource = this.paymentmethodsBindingSource;
            this.PayedColumn.DisplayMember = "Name";
            this.PayedColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.PayedColumn.HeaderText = "الحالة";
            this.PayedColumn.Name = "PayedColumn";
            this.PayedColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PayedColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.PayedColumn.ValueMember = "id";
            // 
            // iDDataGridViewTextBoxColumn1
            // 
            this.iDDataGridViewTextBoxColumn1.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn1.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn1.Name = "iDDataGridViewTextBoxColumn1";
            this.iDDataGridViewTextBoxColumn1.Visible = false;
            // 
            // packagesOrderIDDataGridViewTextBoxColumn
            // 
            this.packagesOrderIDDataGridViewTextBoxColumn.DataPropertyName = "PackagesOrder_ID";
            this.packagesOrderIDDataGridViewTextBoxColumn.HeaderText = "PackagesOrder_ID";
            this.packagesOrderIDDataGridViewTextBoxColumn.Name = "packagesOrderIDDataGridViewTextBoxColumn";
            this.packagesOrderIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // deletedDataGridViewCheckBoxColumn
            // 
            this.deletedDataGridViewCheckBoxColumn.DataPropertyName = "Deleted";
            this.deletedDataGridViewCheckBoxColumn.HeaderText = "Deleted";
            this.deletedDataGridViewCheckBoxColumn.Name = "deletedDataGridViewCheckBoxColumn";
            this.deletedDataGridViewCheckBoxColumn.Visible = false;
            // 
            // DeleteLineColumn
            // 
            this.DeleteLineColumn.HeaderText = "×";
            this.DeleteLineColumn.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Color;
            this.DeleteLineColumn.Image = ((System.Drawing.Image)(resources.GetObject("DeleteLineColumn.Image")));
            this.DeleteLineColumn.Name = "DeleteLineColumn";
            this.DeleteLineColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DeleteLineColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DeleteLineColumn.Text = null;
            this.DeleteLineColumn.Width = 32;
            // 
            // AddEditPackagesOrder
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1058, 693);
            this.ControlBox = false;
            this.Controls.Add(this.collapsibleSplitContainer1);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "AddEditPackagesOrder";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "وصل";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddEditPackagesOrder_FormClosing);
            this.Load += new System.EventHandler(this.AddEditPackagesOrder_Load);
            ((System.ComponentModel.ISupportInitialize)(this.totalPackagesBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packages_orderBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gama_transportationdbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGridViewX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentmethodsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packages_order_linesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProductsdataGridViewX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).EndInit();
            this.collapsibleSplitContainer1.Panel1.ResumeLayout(false);
            this.collapsibleSplitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.collapsibleSplitContainer1)).EndInit();
            this.collapsibleSplitContainer1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ExpensesCommandesTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExpensesDataGridViewX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.package_order_paymentsBindingSource)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.partners_paymentsBindingSource)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.panelEx3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.datDateTimeInput)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.doubleInput2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpensesBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private gama_transportationdbDataSet gama_transportationdbDataSet;
        private System.Windows.Forms.BindingSource packages_orderBindingSource;
        private gama_transportationdbDataSetTableAdapters.packages_orderTableAdapter packages_orderTableAdapter;
        private gama_transportationdbDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private DevComponents.Editors.DoubleInput totalPackagesBox;
        private DevComponents.Editors.DoubleInput discountBox;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.DataGridViewX mainDataGridViewX;
        private DevComponents.DotNetBar.Controls.DataGridViewX ProductsdataGridViewX1;
        private DevComponents.DotNetBar.Controls.CollapsibleSplitContainer collapsibleSplitContainer1;
        private DevComponents.DotNetBar.PanelEx panelEx3;
        private DevComponents.DotNetBar.Controls.TextBoxX ProductSearchBox;
        private DevComponents.DotNetBar.ButtonX btnAddLine;
        private gama_transportationdbDataSetTableAdapters.productsTableAdapter productsTableAdapter;
        private System.Windows.Forms.BindingSource productsBindingSource;
        private gama_transportationdbDataSetTableAdapters.packages_order_linesTableAdapter packages_order_linesTableAdapter;
        private System.Windows.Forms.BindingSource packages_order_linesBindingSource;
        private System.Windows.Forms.BindingSource productsBindingSource1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.ButtonX btnEditLine;
        private DevComponents.DotNetBar.ButtonX btnDeleteLine;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput datDateTimeInput;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.SwitchButton paymentOnRecipientSwitchButton;
        private DevComponents.DotNetBar.ButtonX btnExit;
        private DevComponents.DotNetBar.ButtonX btnAddOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn productNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn partnersIDDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.LabelX partnerNameLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private DevComponents.DotNetBar.ButtonItem btnAddOrderFactory;
        private DevComponents.DotNetBar.ButtonItem btnAddOrderMerchant;
        private DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn dataGridViewButtonXColumn1;
        private DevComponents.DotNetBar.ButtonX btnAddPayment;
        private DevComponents.Editors.DoubleInput doubleInput2;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.ButtonX btnPrintt;
        private DevComponents.DotNetBar.Controls.DataGridViewX ExpensesDataGridViewX1;
        private System.Windows.Forms.TableLayoutPanel ExpensesCommandesTable;
        private DevComponents.DotNetBar.ButtonX btnDeleteExpenses;
        private DevComponents.DotNetBar.ButtonX btnEditExpenses;
        private DevComponents.DotNetBar.ButtonX btnAddExpences;
        private DevComponents.DotNetBar.LabelX labelX3;
        private System.Windows.Forms.BindingSource package_order_paymentsBindingSource;
        private gama_transportationdbDataSetTableAdapters.package_order_paymentsTableAdapter package_order_paymentsTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn noteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateStampDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn packagesorderIDDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn partnerspaymentsIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn packageorderexpensesIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn packagesorderYearIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendingOrderIDDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.Editors.DoubleInput ExpensesBox;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private System.Windows.Forms.BindingSource partners_paymentsBindingSource;
        private gama_transportationdbDataSetTableAdapters.partners_paymentsTableAdapter partners_paymentsTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn datDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn noteDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn partnersIDDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn deletedDataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn delivererOrderIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn delivererOrderSerialYearDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendingOrderIDDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource paymentmethodsBindingSource;
        private gama_transportationdbDataSetTableAdapters.payment_methodsTableAdapter payment_methodsTableAdapter;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX1;
        private DevComponents.DotNetBar.LabelX labelX14;
        private System.Windows.Forms.DataGridViewTextBoxColumn recipientDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhonesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn productsIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn designationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn PayedColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn packagesOrderIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn deletedDataGridViewCheckBoxColumn;
        private DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn DeleteLineColumn;
    }
}