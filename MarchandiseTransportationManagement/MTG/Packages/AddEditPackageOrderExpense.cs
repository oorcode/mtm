 using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace MTM.Packages
{
    public partial class AddEditPackageOrderExpense : DevComponents.DotNetBar.Metro.MetroForm
    {
        public int ExpenseID { get; set; }
        public int PackageOrderID { get; set; }
        public int PackageOrderYearID { get; set; }
        public int DepotID { get; set; }
        public AddEditPackageOrderExpense()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}