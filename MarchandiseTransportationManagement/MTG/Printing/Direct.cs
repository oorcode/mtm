﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Reporting.WinForms;
using System.Drawing.Printing;
using DevComponents.DotNetBar;
using System.Drawing.Imaging;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;

namespace MTM.Report
{
    public  class Direct
    {
        #region Direct Printing Get Ready
        private int m_currentPageIndex;
        private IList<Stream> m_streams;
        Random d = new Random();


        private Stream CreateStream(string name, string fileNameExtension, Encoding encoding, string mimeType, bool willSeek)
        {
            int i = 0;
            Stream stream = null;
        w:
            i++;
            try
            {
                Directory.CreateDirectory("BonLiv");
                stream = new FileStream("BonLiv\\" + name +
                     (int)d.Next(0, 10) + "." + fileNameExtension, FileMode.Create);
            }
            catch (Exception)
            {
                if (i < 10) goto w;
            }

            m_streams.Add(stream);

            return stream;
        }

        private void ExportA4(LocalReport report)
        {
            string deviceInfo =
              @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                <PaperType>A4</PaperType>
                <PageWidth>21cm</PageWidth>
                <PageHeight>29.7cm</PageHeight>
                <MarginTop>0.5cm</MarginTop>
                <MarginLeft>0.5cm</MarginLeft>
                <MarginRight>0cm</MarginRight>
                <MarginBottom>0.5cm</MarginBottom>
                </DeviceInfo>";
            Warning[] warnings;
            m_streams = new List<Stream>();
            report.Render("Image", deviceInfo, CreateStream,
               out warnings);
            foreach (Stream stream in m_streams)
                stream.Position = 0;
        }

        private  void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new
               Metafile(m_streams[m_currentPageIndex]);
            ev.Graphics.DrawImage(pageImage, ev.PageBounds);
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

        public void Print(string PrinterName, bool istlanding)
        {
            if (m_streams == null || m_streams.Count == 0)
                return;
            PrintDocument printDoc = new PrintDocument();

            printDoc.PrinterSettings.PrinterName = PrinterName;

            if (istlanding) printDoc.DefaultPageSettings.Landscape = true;

            if (!printDoc.PrinterSettings.IsValid)
            {
                string msg = String.Format(
                   "Imprimante n'existe pas \"{0}\".", PrinterName);
                MessageBoxEx.Show(msg, "Imprimante n'existe pas.");
                return;
            }

            printDoc.DocumentName = "Imprimer";
            printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
            try
            {
                printDoc.Print();
            }
            catch (Win32Exception) { }
            catch (Exception) { }
        }

        public void exportToExcelPDF(string ExportPath, string reportNamePath, bool isLanding, List<ReportDataSource> dataSources, List<ReportDataSource> subDataSources, string NoneExcelPDF = "PDF", params ReportParameter[] parametre)
        {
                
           
            LocalReport localReport = new LocalReport();
            localReport.ReportEmbeddedResource = reportNamePath;

            if (dataSources != null)
                foreach (ReportDataSource data in dataSources)
                {
                    localReport.DataSources.Add(data);
                }
           // Needs.SSConfigNeeds.SetParameters(localReport, false);
            if (parametre != null)
            {
                localReport.SetParameters(parametre);
                localReport.EnableExternalImages = true;
            }

            ExportA4(localReport);
            m_currentPageIndex = 0;

            if (ExportPath == null)
            {
                FolderBrowserDialog fBDialog = new FolderBrowserDialog();
                fBDialog.ShowNewFolderButton = true;
                if (fBDialog.ShowDialog() == DialogResult.OK)
                {
                    ExportPath = fBDialog.SelectedPath;
                }
            }
            var filename = DateTime.Now.ToString("yyyy-MM-dd HH-mm")+"."+NoneExcelPDF;
            ExportPath = Path.Combine(ExportPath, filename);
            System.IO.FileInfo fi = new System.IO.FileInfo(ExportPath);
            
            if (fi.Exists) fi.Delete();

            Warning[] warnings;

            string[] streamids;

            string mimeType, encoding, filenameExtension;

            byte[] bytes = localReport.Render(NoneExcelPDF, null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);

            using (System.IO.FileStream fs = System.IO.File.Create(ExportPath))
            {
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
            }
        }

        public void DirectPrinting(string PrinterName, string reportNamePath, bool isLanding, List<ReportDataSource> dataSources, List<ReportDataSource> subDataSources, string NoneExcelPDF = "PDF", params ReportParameter[] parametre)
        {
            LocalReport localReport = new LocalReport();
            localReport.ReportEmbeddedResource = reportNamePath;

            if (dataSources != null)
                foreach (ReportDataSource data in dataSources)
                {
                    localReport.DataSources.Add(data);
                }
           // Needs.SSConfigNeeds.SetParameters(localReport, false);
            if (parametre != null)
            {
                localReport.SetParameters(parametre);
                localReport.EnableExternalImages = true;
            }

            ExportA4(localReport);
            m_currentPageIndex = 0;

            Print(PrinterName, isLanding);
        }
        #endregion
    }
}
