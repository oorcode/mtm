﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using gamadev.gpf.utilities;
using System.ComponentModel;

namespace SSUtilities
{

    public enum MachineState
    {
        Slave = 0,     
        Master = 1
    }

    public enum PrintingLanguage
    {
        Arabic = 0,
        French = 1
    }
  
    public enum PaperSizes
    {
        A4 = 0,
        A5 = 1,
        A6 = 2,
        Ticket = 3
    }

    public static class CONFIG_SECTIONS
    {
        public struct MTM
        {
            public const string NAME = "MTM";
            public const string IS_FIRST_RUN = "IS_FIRST_RUN";
            public const string IS_FIRST_MIGRATION = "IS_FIRST_MIGRATION";
            public const string MACHINE_STATE = "MACHINE_STATE";
            public const string SERVER_ADDRESS = "SERVER_ADDRESS";
            public const string SERVER_PORT = "SERVER_PORT";
            public const string DB_NAME = "DB_NAME";
            public const string BACKUP_PATH = "BACKUP_PATH";
            public const string BACKUP_PATH_DOC = "BACKUP_PATH_DOC";
            public const string DefaultPrinter = "DefaultPrinter";
        }

        public struct CompanyInfo
        {
            public const string CompanyName = "CompanyName";
            public const string CompanyActivity = "CompanyActivity";
            public const string Mobile1 = "Mobile1";
            public const string Mobile2 = "Mobile2";
            
        }

      
    }

    public class SSConfigFile : IniFile
    {
        public SSConfigFile() : base(@".\cfg.ini")
        {

        }

        public SSConfigFile(string cfgFile) : base(cfgFile)
        {
        }

        private void InitializeValue<T>(IniFile cfg,string section, string entry, T @default) 
        {
            if (!cfg.HasEntry(section, entry))
            {
                cfg.SetValue(section,entry,@default);               
            }
        }

        public T GetValue<T>(string section_name, string entry, T @default = default(T)) 
        {
            var value = GetValue(section_name, entry);
            if (value != null)
            {
                TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
                if (converter != null)
                {
                    return (T)converter.ConvertFrom(value);
                }
            }
            return @default;
        }

        public static void Initialize()
        {
            var cfg = new SSConfigFile();

            if (cfg.HasEntry(CONFIG_SECTIONS.MTM.NAME, CONFIG_SECTIONS.MTM.IS_FIRST_RUN) &&  Convert.ToBoolean(cfg.GetValue(CONFIG_SECTIONS.MTM.NAME, CONFIG_SECTIONS.MTM.IS_FIRST_RUN))) 
            {
                return;
            }

            cfg.InitializeValue<bool>(cfg, CONFIG_SECTIONS.MTM.NAME, CONFIG_SECTIONS.MTM.IS_FIRST_RUN, false);
            cfg.InitializeValue<int>(cfg, CONFIG_SECTIONS.MTM.NAME, CONFIG_SECTIONS.MTM.SERVER_PORT, 3306);
            cfg.InitializeValue<string>(cfg, CONFIG_SECTIONS.MTM.NAME, CONFIG_SECTIONS.MTM.SERVER_ADDRESS, "localhost");
            cfg.InitializeValue<MachineState>(cfg, CONFIG_SECTIONS.MTM.NAME, CONFIG_SECTIONS.MTM.MACHINE_STATE, MachineState.Master);
        }
    }
}
